﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bopper.Data
{
    public class DirectMessages
    {
        [Key]
        public System.Guid Id { get; set; }
        public System.Guid RecipientId { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public string dm_Type { get; set; }
        public System.Guid SenderId { get; set; }
    }
}
