﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bopper.Data.Model
{
    public class FbClientPics
    {
        [Key]
        public System.Guid Id { get; set; }
        public System.Guid ClientId { get; set; }
        public string PhotoUrl { get; set; }
        public string PhotoType { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
