//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bopper.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Client
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Client()
        {
            this.Events = new HashSet<Event>();
            this.InviteRequests = new HashSet<InviteRequest>();
        }
        [Key]
        public System.Guid Id { get; set; }
        public string UserName { get; set; }
        public string FullNames { get; set; }
        public string Email { get; set; }
        public string ProfPic { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }       
        public string State { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Description { get; set; }
        public string DOB { get; set; }
        public string Bio { get; set; }
        public string Gender { get; set; }
        public string CoverPhoto { get; set; }
        public System.DateTime TimeStamp { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Event> Events { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InviteRequest> InviteRequests { get; set; }
    }
}
