﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bopper.Data
{
    public class Contact
    {
        [Key]
        public System.Guid Id { get; set; }
        public Guid ContactId { get; set; }
        public Guid UserId { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
