﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bopper.Data.Model
{
    public class Review
    {
        [Key]
        public Guid Id { get; set; }
        public Guid EventId { get; set; }
        public string Comment { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
