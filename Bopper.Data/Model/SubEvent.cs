﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bopper.Data.Model
{
    public class SubEvent
    {
        [Key]
        public Guid Id { get; set; }
        public Guid EventId { get; set; }
        public Guid ClientId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DbGeography Location { get; set; }
        public bool PrivacyFlag { get; set; }
        public double Cost { get; set; }
        public string Photo1 { get; set; }
        public string Photo2 { get; set; }
        public string Photo3 { get; set; }
        public string Photo4 { get; set; }
        public string Photo5 { get; set; }
        public System.DateTime TimeStamp { get; set; }
    }
}
