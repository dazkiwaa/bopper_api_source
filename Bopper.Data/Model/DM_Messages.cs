﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bopper.Data
{
    public class DM_Messages
    {
        [Key]
        public System.Guid Id { get; set; }
        public System.Guid DirectMessageId { get; set; }
        public System.Guid ClientId { get; set; }
        public string Message { get; set; }
        public string Image { get; set; }
        public string Status { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public string timeZone { get; set; }
    }
}
