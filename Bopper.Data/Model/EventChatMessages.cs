﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bopper.Data
{
    public class EventChatMessages
    {
        [Key]
        public System.Guid Id { get; set; }
        public System.Guid EventId { get; set; }
        public System.Guid ClientId { get; set; }
        public string Message { get; set; }
        public string Image1 { get; set; }
        public string Image2 { get; set; }
        public string Image3 { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public string Status { get; set; }
        public string TimeZone { get; set; }

    }
}
