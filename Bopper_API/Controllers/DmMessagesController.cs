﻿using Bopper.Models;
using Bopper.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Bopper.Controllers
{
    public class DmMessagesController : ApiController
    {
        DM_MessagesRepo dmRepo = new DM_MessagesRepo();

        [HttpPost]
        [Route("api/SendDmMessage")]
        [ResponseType(typeof(DM_MessagesItems))]
        public async Task<IHttpActionResult> SendDmMessage(DM_MessagesItems dm)
        {
            var data = await dmRepo.createMessage(dm);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpPost]
        [Route("api/EditDmMessage")]
        [ResponseType(typeof(DM_MessagesItems))]
        public async Task<IHttpActionResult> EditDmMessage(DM_MessagesItems dm)
        {
            var data = await dmRepo.editMessage(dm);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [Route("api/DeleteDmMessage")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> DeleteDmMessage(Guid dmId)
        {
            var data = await dmRepo.deleteMessage(dmId);

            return data ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetMyDmMessages")]
        [ResponseType(typeof(List<DM_MessagesItems>))]
        public async Task<IHttpActionResult> GetMyDmMessages(Guid directMessageId)
        {
            var data = dmRepo.getChatRoomMessages(directMessageId);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        //    [HttpGet]
        //    [Route("api/GetTimeZoneTime")]
        //    [ResponseType(typeof(string))]
        //    public IHttpActionResult GetTimeZoneTime(string timeZone)
        //    {                  
        //        var dateUtc = DateTime.UtcNow;

        //        string offsetType = timeZone.Substring(0, 1);

        //        if (offsetType.Equals("+"))
        //        {
        //            string offsetTime = timeZone.Substring(1);

        //            var offsetHour = TimeSpan.Parse(offsetTime);

        //            dateUtc += offsetHour;

        //            return Ok(dateUtc);
        //        }
        //        else if (offsetType.Equals("-"))
        //        {
        //            string offsetTime = timeZone.Substring(1);

        //            var offsetHour = TimeSpan.Parse(offsetTime);

        //            dateUtc -= offsetHour;

        //            return Ok(dateUtc);
        //        }
        //        else
        //            return Ok(dateUtc);
        //    }
        //}
    }
}
