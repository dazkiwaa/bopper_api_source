﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bopper.ViewModels;
using Bopper.Models;
using System.Web.Http.Description;
using System.Threading.Tasks;

namespace Bopper.Controllers
{
    public class EventController : ApiController
    {

        private readonly EventRepo _eventRepo = new EventRepo();

        [HttpPost]
        [Route("api/AddEvent")]
        [ResponseType(typeof(EventItems))]
        public async Task<IHttpActionResult> AddEvent(EventItems et)
        {
            var data = await _eventRepo.addEvent(et);

            return (data != null) ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpPost]
        [Route("api/UpdateEvent")]
        [ResponseType(typeof(EventItems))]
        public async Task<IHttpActionResult> UpdateEvent(EventItems et)
        {
            var data = await _eventRepo.updateEvent(et);

            return (data != null) ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [Route("api/DeleteEvent")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> DeleteEvent(Guid eId)
        {
            var data = await _eventRepo.deleteEvent(eId);

            return data ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpPost]
        [Route("api/GetLatestEvents")]
        [ResponseType(typeof(List<EventItems>))]
        public async Task<IHttpActionResult> GetLatestEvents(LocationItems lcs)
        {
            var data = _eventRepo.getLatestEvents(lcs);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpPost]
        [Route("api/GetLatestEventsWithoutMine")]
        [ResponseType(typeof(List<EventItems>))]
        public async Task<IHttpActionResult> GetLatestEventsWithoutMine(LocationItems lcs, Guid clientId)
        {
            var data = _eventRepo.getLatestEventsWithoutMine(lcs, clientId);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetNumberOfEventsIPosted")]
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> GetNumberOfEventsIPosted(Guid clientId)
        {
            return Ok(_eventRepo.getNumberOfPostsIPost(clientId));
        }

        [HttpGet]
        [Route("api/GetEventsIPosted")]
        [ResponseType(typeof(List<EventItems>))]
        public async Task<IHttpActionResult> GetEventsIPosted(Guid clientId)
        {
            var data = _eventRepo.getEventsIPosted(clientId);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetEventsByLocationString")]
        [ResponseType(typeof(List<EventItems>))]
        public async Task<IHttpActionResult> GetEventsByLocationString(string location)
        {
            var data = _eventRepo.getEventsByCity(location);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetEventById")]
        [ResponseType(typeof(EventItems))]
        public async Task<IHttpActionResult> GetEventById(Guid eventId)
        {
            var data = _eventRepo.getEventById(eventId);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetAllEvents")]
        [ResponseType(typeof(List<EventItems>))]
        public async Task<IHttpActionResult> GetAllEvents()
        {
            var data = _eventRepo.getAllEvents();

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        //[HttpPost]
        //[Route("api/GetLatestEventsWithoutMine")]
        //[ResponseType(typeof(List<EventItems>))]
        //public async Task<IHttpActionResult> GetLatestEventsWithoutMine(LocationItems locs, Guid clientId)
        //{
        //    var data = _eventRepo.getLatestEventsWithoutMine(locs, clientId);

        //    return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        //}

        [HttpGet]
        [Route("api/GetEventsByCityWithoutMine")]
        [ResponseType(typeof(List<EventItems>))]
        public async Task<IHttpActionResult> GetEventsByCityWithoutMine(string city, Guid clientId)
        {
            var data = _eventRepo.getEventsByCityWithoutMine(city, clientId);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/CancelEvent")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> CancelEvent(Guid eventId)
        {
            var data = await _eventRepo.cancelEvent(eventId);

            return data ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetEventSpaces")]
        [ResponseType(typeof(EventSpaceItems))]
        public IHttpActionResult GetEventSpaces(Guid eventId)
        {
            var data = _eventRepo.getEventSpaces(eventId);

            return (data != null) ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpPost]
        [Route("api/FilterEvents")]
        [ResponseType(typeof(List<EventItems>))]
        public IHttpActionResult FilterEvents(FilterItems evt)
        {
            var data = _eventRepo.searchForEvent(evt);

            return (data != null) ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpPost]
        [Route("api/FilterEventsWithDate")]
        [ResponseType(typeof(List<EventItems>))]
        public IHttpActionResult FilterEventsWithDates(FilterItems evt)
        {
            var data = _eventRepo.searchForEventDate(evt);

            return (data != null) ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpPost]
        [Route("api/FilterEventsWithDateDistance")]
        [ResponseType(typeof(List<EventItems>))]
        public IHttpActionResult FilterEventsWithDateDistance(FilterItems evt)
        {
            var data = _eventRepo.searchForEventsDateDist(evt);

            return (data != null) ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/UpdateTypeEventX")]
        public async Task<IHttpActionResult> UpdateTypeEventX(){
            await _eventRepo.updateTypeEvent();

            return Ok();
        }

        [HttpPost]
        [Route("api/AddTestRecords")]
        [ResponseType(typeof(EventGenItem))]
        public async Task<IHttpActionResult> AddTestRecords(EventGenItem evtG)
        {
            var data = await _eventRepo.addTestEvents(evtG);

            return (data != null) ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpPost]
        [Route("api/EventEditImages")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> EventEditImage(Guid eventId, List<string> imageLinks)
        {
            var data = await _eventRepo.editEventImageLinks(eventId, imageLinks);

            return (data) ? (IHttpActionResult)Ok(data) : BadRequest();
        }
    }
}
