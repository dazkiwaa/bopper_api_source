﻿using Bopper.Models;
using Bopper.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Bopper.Controllers
{
    public class EventChatController : ApiController
    {

        EventChatRepo cRepo = new EventChatRepo();

        [HttpPost]
        [Route("api/ChatOnArticle")]
        [ResponseType(typeof(EventChatItems))]
        public async Task<IHttpActionResult> ChatOnArticle(EventChatItems cm)
        {
            var data = await cRepo.commentOnArticle(cm);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpPost]
        [Route("api/EditChatMessage")]
        [ResponseType(typeof(EventChatItems))]
        public async Task<IHttpActionResult> EditChatMessage(EventChatItems cm)
        {
            var data = await cRepo.editComment(cm);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }


        [Route("api/DeleteEventChatMessage")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> DeleteEventChatMessage(Guid commentId)
        {
            var data = await cRepo.deleteComment(commentId);

            return data ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetEventChatMessage")]
        [ResponseType(typeof(List<EventChatReturnItems>))]
        public async Task<IHttpActionResult> GetEventChatMessage(Guid articleId)
        {
            var data = cRepo.getArticleComments(articleId);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }
    }
}
