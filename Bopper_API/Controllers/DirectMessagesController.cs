﻿using Bopper.Models;
using Bopper.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace Bopper.Controllers
{
    public class DirectMessagesController : ApiController
    {
        DirectMessageRepo dmRepo = new DirectMessageRepo();

        [HttpPost]
        [Route("api/CreateDm")]
        [ResponseType(typeof(DirectMessageItems))]
        public async Task<IHttpActionResult> CreateDm(DirectMessageItems dm)
        {
            var data = await dmRepo.createDm(dm);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }


        [Route("api/DeleteDm")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> DeleteDm(Guid directMessageId)
        {
            var data = await dmRepo.deleteDm(directMessageId);

            return data ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetMyDms")]
        [ResponseType(typeof(DirectMessageReturnItems))]
        public async Task<IHttpActionResult> GetMyDms(Guid senderId)
        {
            var data = dmRepo.getMyDms(senderId);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetDmId")]
        [ResponseType(typeof(DirectMessageReturnItems))]
        public async Task<IHttpActionResult> GetDmId(Guid senderId, Guid receiverId)
        {
            var data = dmRepo.getMyDmId(senderId, receiverId);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetByDmId")]
        [ResponseType(typeof(DirectMessageReturnItems))]
        public async Task<IHttpActionResult> GetDmId(Guid dmId)
        {
            var data = dmRepo.getMyDmId(dmId);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetDmMessagesNumber")]
        [ResponseType(typeof(int))]
        public IHttpActionResult GetDmMessagesNumber(Guid clientId)
        {
            return (IHttpActionResult)Ok(dmRepo.getNumberOfMessages(clientId));
        }
    }
}