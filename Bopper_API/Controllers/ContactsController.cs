﻿using Bopper.Models;
using Bopper.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Bopper.Controllers
{
    public class ContactsController : ApiController
    {
        readonly ContactsRepo cRepo = new ContactsRepo();

        [HttpPost]
        [Route("api/AddContact")]
        [ResponseType(typeof(ContactItems))]
        public async Task<IHttpActionResult> AddContact(ContactItems contact)
        {
            var isAdded = cRepo.checkIfAddedContact(contact.userId, contact.contactId);

            if (isAdded)
            {
                var data = await cRepo.addContact(contact);

                return (data != null) ? (IHttpActionResult)Ok(data) : BadRequest();
            } else
                return new System.Web.Http.Results.ResponseMessageResult(Request.CreateErrorResponse((HttpStatusCode)420, new HttpError("Already added to contacts.")));
        }

        [HttpPost]
        [Route("api/AddContactByMail")]
        [ResponseType(typeof(bool?))]
        public async Task<IHttpActionResult> AddContactByMail(ContactItems contact, string email)
        {          
            var data = await cRepo.addContactByMail(contact, email);

            // return (data != null) ? (IHttpActionResult)Ok(data) : BadRequest();
            try
            {
                if (!data.Value)
                    return new System.Web.Http.Results.ResponseMessageResult(Request.CreateErrorResponse((HttpStatusCode)420, new HttpError("Already added to contacts.")));
                else
                    return Ok(data);
            }catch(Exception r)
            {
                return BadRequest();
            }
            //else
                
        }

        [Route("api/DeleteContact")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> DeleteContact(Guid contactId)
        {
            var data = await cRepo.deleteContact(contactId);

            return data ? (IHttpActionResult)Ok(data) : BadRequest();           
        }

        [HttpGet]
        [Route("api/GetMyContacts")]
        [ResponseType(typeof(List<ContactItems>))]
        public IHttpActionResult GetMyContacts(Guid userId)
        {
            var data = cRepo.getMyContacts(userId);

            return (data != null) ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetNumberOfContacts")]
        [ResponseType(typeof(int))]
        public IHttpActionResult GetNumberOfContacts(Guid clientId)
        {
            return Ok(cRepo.getNumberOfContacts(clientId));
        }
    }
}
