﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bopper.Models;
using Bopper.ViewModels;
using System.Web.Http.Description;
using System.Threading.Tasks;

namespace Bopper.Controllers
{
    public class ClientsController : ApiController
    {
       private readonly ClientRepo _clientRepo = new ClientRepo();

        [HttpPost]
        [Route("api/LoginClient")]
        [ResponseType(typeof(ClientItems))]
        public async Task<IHttpActionResult> LoginClient(LoginItems lgs)
        {
            var data = await _clientRepo.loginClient(lgs);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpPost]
        [Route("api/CreateClient")]
        [ResponseType(typeof(ClientItems))]
        public async Task<IHttpActionResult> CreateClient(ClientItems cls)
        {
            //if (!_clientRepo.checkUserName(cls.userName))           
            //{
                var data = await _clientRepo.createClient(cls);
                return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
            //}else
            //    return Conflict();
        }

        [HttpPost]
        [Route("api/LoginRegistration")]
        [ResponseType(typeof(ClientItems))]
        public async Task<IHttpActionResult> LoginRegistration(ClientItems cls)
        {
            var data = await _clientRepo.loginRegistration(cls);            

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpPost]
        [Route("api/UpdateInfo")]
        [ResponseType(typeof(ClientItems))]
        public async Task<IHttpActionResult> UpdateInfo(ClientItems cls)
        {
            var data = await _clientRepo.updateInfo(cls);
                  
            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/ChangeProfPic")]
        [ResponseType(typeof(ClientItems))]
        public async Task<IHttpActionResult> ChangeProfPic(Guid clientId, string profPic)
        {
            bool data = await _clientRepo.changeProfPic(clientId, profPic);

            return (data) ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/CheckUserName")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> CheckUserName(string userName)
        {
            bool data = _clientRepo.checkUserName(userName);

            return data ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetAllClients")]
        [ResponseType(typeof(List<ClientItems>))]
        public async Task<IHttpActionResult> GetAllClients()
        {
            var data = _clientRepo.getAllClients();

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [Route("api/DeleteInfo")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> DeleteInfo(Guid clientId)
        {
            bool data = await _clientRepo.deleteInfo(clientId);

            return (data) ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/ForgotPassword")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> ForgotPassword(string userName)
        {
            var data = await _clientRepo.forgotPassword(userName);

            return data ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpPost]
        [Route("api/ChangePassword")]
        [ResponseType(typeof(NewPasswordItems))]
        public async Task<IHttpActionResult> ChangeRequest(NewPasswordItems np)
        {
            var data = await _clientRepo.changePassword(np);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetClientById")]
        [ResponseType(typeof(ClientItems))]
        public IHttpActionResult GetClientById(Guid clientId)
        {
            var data = _clientRepo.getClientAccountById(clientId);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

    }
}
