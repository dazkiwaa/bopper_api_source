﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bopper.Models;
using Bopper.ViewModels;
using System.Web.Http.Description;
using System.Threading.Tasks;

namespace Bopper.Controllers
{
    public class SubEventsController : ApiController
    {
        readonly SubEventRepo sRepo = new SubEventRepo();


        [HttpPost]
        [Route("api/CreateSubEvent")]
        [ResponseType(typeof(SubEventItems))]
        public async Task<IHttpActionResult> CreateSubEvent(SubEventItems sb)
        {
            var data = await sRepo.addSubEvent(sb);

            return (data != null) ? (IHttpActionResult)Ok(data) : BadRequest();
        }


        [HttpGet]
        [Route("api/GetEventSubEvents")]
        [ResponseType(typeof(List<SubEventItems>))]
        public IHttpActionResult GetEventSubEvents(Guid eventId)
        {
            return Ok(sRepo.getEventSubEvents(eventId));
        }

        [HttpGet]
        [Route("api/GetMySubEvents")]
        [ResponseType(typeof(List<SubEventItems>))]
        public IHttpActionResult GetMySubEvents(Guid clientId)
        {
            return Ok(sRepo.getMySubEvents(clientId));
        }
    }
}
