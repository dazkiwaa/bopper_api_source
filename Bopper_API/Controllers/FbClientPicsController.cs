﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bopper.ViewModels;
using Bopper.Models;
using Bopper.Data;
using System.Web.Http.Description;
using System.Threading.Tasks;

namespace Bopper.Controllers
{
    public class FbClientPicsController : ApiController
    {
        private readonly FbClientPicRepo fRepo = new FbClientPicRepo();
        
        [HttpPost]
        [Route("api/AddPic")]
        [ResponseType(typeof(FbClientPicItems))]
        public async Task<IHttpActionResult> AddPic(FbClientPicItems pic)
        {
            var data = await fRepo.addPic(pic);
            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }


        [HttpPost]
        [Route("api/AddPics")]
        [ResponseType(typeof(List<FbClientPicItems>))]
        public async Task<IHttpActionResult> AddPics(List<FbClientPicItems> pic)
        {
            var data = await fRepo.addPics(pic);
            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpPost]
        [Route("api/UpdatePic")]
        [ResponseType(typeof(FbClientPicItems))]
        public async Task<IHttpActionResult> UpdatePic(FbClientPicItems pic)
        {
            var data = await fRepo.updatePic(pic);
            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }
     
        [Route("api/DeletePic")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> DeletePic(Guid photoId)
        {
            var data = await fRepo.deletePic(photoId);
            return data ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetClientPics")]
        [ResponseType(typeof(FbClientPicItems))]
        public  IHttpActionResult GetClientPics(Guid clientId)
        {
            var data = fRepo.getClientPics(clientId);
            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetNumberOfPics")]
        [ResponseType(typeof(int))]
        public IHttpActionResult GetNumberOfPics(Guid clientId)
        {
            return Ok(fRepo.getNumberOfPics(clientId));
        }
    }
}
