﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bopper.Models;
using Bopper.ViewModels;
using System.Web.Http.Description;
using System.Threading.Tasks;

namespace Bopper.Controllers
{
    public class CheckInController : ApiController
    {

        private readonly CheckInRepo _checkInRepo = new CheckInRepo();

        [HttpPost]
        [Route("api/CheckIn")]
        [ResponseType(typeof(CheckInItems))]
        public async Task<IHttpActionResult> CheckIn(CheckInItems chk)
        {
            var data = await _checkInRepo.checkIn(chk);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }


        [HttpPost]
        [Route("api/GetClientsInVicinity")]
        [ResponseType(typeof(List<ClientItems>))]
        public async Task<IHttpActionResult> GetClientsInVicinity(LocationItems lcs)
        {
            var data = _checkInRepo.getClientsInVicinity(lcs);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetLatestCheckIn")]
        [ResponseType(typeof(CheckInItems))]
        public async Task<IHttpActionResult> GetLatestCheckIn(Guid clientId)
        {
            var data = _checkInRepo.getLatestsCheckIn(clientId);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }
    }
}
