﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bopper.Models;
using Bopper.ViewModels;
using System.Web.Http.Description;
using System.Threading.Tasks;

namespace Bopper.Controllers
{
    public class NotificationController : ApiController
    {
        private readonly NotificationRepo _notificationRepo = new NotificationRepo();    
  
        [HttpGet]
        [Route("api/GetMyNotifications")]
        [ResponseType(typeof(List<NotificationReturnItems>))]
        public IHttpActionResult GetMyNotifications(Guid clientId)
        {
            var data = _notificationRepo.getMyNotifications(clientId);

            return (data != null) ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/SendGCMStringNotification")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> SendGCMStringNotification(string message)
        {
            return await _notificationRepo.sendGCMStringNotification(message) ? (IHttpActionResult)Ok(true) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetMyNotificationNumber")]
        [ResponseType(typeof(int))]
        public IHttpActionResult GetMyNotificationNumber(Guid clientId)
        {
            return (IHttpActionResult)Ok(_notificationRepo.getMyNotificationNumbers(clientId));
        }

        [HttpPost]
        [Route("api/SendTestNofication")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> SendTestNotificaiton(NotificationItems nt)
        {
            var data = await _notificationRepo.sendTestNotification(nt);
            return (IHttpActionResult)Ok(data);
        }

        [HttpGet]
        [Route("api/GetNotification")]
        [ResponseType(typeof(List<NotificationItems>))]
        public IHttpActionResult SendTextNotification(NotificationItems notif)
        {
            return (IHttpActionResult)Ok(_notificationRepo.getAllNotifications());
        }

        [Route("api/DeleteNotification")]
        [ResponseType(typeof(bool))]
        async public Task<IHttpActionResult> DeleteNotification(Guid notif)
        {
            var data = await _notificationRepo.deleteNotification(notif);

            return (data) ? (IHttpActionResult)Ok(data) : BadRequest();
        }
    }
}
