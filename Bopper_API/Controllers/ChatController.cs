﻿using Bopper.Models;
using Bopper.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Bopper.Controllers
{
    public class ChatController : ApiController
    {
        private readonly ChatRepo _chatRepo = new ChatRepo();

        [HttpPost]
        [Route("api/CreateMessage")]
        [ResponseType(typeof(ChatItems))]
        public async Task<IHttpActionResult> CreateMessage(ChatItems msg)
        {
            var data = await _chatRepo.createMessage(msg);
            if (data != null)
                return Ok(data);
            return BadRequest();
        }

        [HttpPost]
        [Route("api/ReplyMessage")]
        [ResponseType(typeof(ChatItems))]
        public async Task<IHttpActionResult> ReplyMessage(ChatItems msg)
        {
            var data = await _chatRepo.replyMessage(msg);
            if (data != null)
                return Ok(data);
            else
                return BadRequest();
        }

        [HttpPost]
        [Route("api/EditMessage")]
        [ResponseType(typeof(ChatItems))]
        public async Task<IHttpActionResult> EditMessage(ChatItems msg)
        {
            var data = await _chatRepo.editMessage(msg);
            if (data != null)
                return Ok(data);
            else
                return BadRequest();
        }

        [Route("api/DeleteMessage")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> DeleteMessage(Guid messageId)
        {
            var data = await _chatRepo.deleteMessage(messageId);
            if (data)
                return Ok(data);
            else
                return BadRequest();
        }

        [HttpGet]
        [Route("api/GetTrotMessage")]
        [ResponseType(typeof(List<ChatReturnItems>))]
        public async Task<IHttpActionResult> GetTrotMessage(Guid trotId)
        {
            var data = _chatRepo.getTrotMessages(trotId);
            if (data != null)
                return Ok(data);
            else
                return BadRequest();
        }


        [HttpGet]
        [Route("api/GetMyTrotMessages")]
        [ResponseType(typeof(List<ChatReturnItems>))]
        public async Task<IHttpActionResult> GetMyTrotMessages(Guid trotId, Guid senderId)
        {
            var data = _chatRepo.getMyTrotMessages(trotId, senderId);
            if (data != null)
                return Ok(data);
            else
                return BadRequest();
        }

        [HttpGet]
        [Route("api/GetNumberOfChatMessages")]
        [ResponseType(typeof(int))]
        public async Task<IHttpActionResult> GetNumberofChatMessages(Guid trotId)
        {
            var data = _chatRepo.getNumberOfMessages(trotId);
            return Ok(data);
        }
    }
}
