﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bopper.Data;
using Bopper.ViewModels;
using Bopper.Models;
using System.Web.Http.Description;
using System.Threading.Tasks;

namespace Bopper.Controllers
{
    public class RSVPController : ApiController
    {
        RSVPRepo rRepo = new RSVPRepo();

        [HttpPost]
        [Route("api/CreateRSVP")]
        [ResponseType(typeof(RSVPItems))]
        public async Task<IHttpActionResult> CreateRSVP(RSVPItems rsvp)
        {
            var rsvps = rRepo.getNumberOfRSVPS(rsvp.eventId);
            var maxGuests = rRepo.getMaxInvites(rsvp.eventId);

                if (rsvp != null && maxGuests != null)
                {
                    if (rsvps <= maxGuests)
                    {
                        var data = await rRepo.createRSVP(rsvp);
                        return (data != null) ? (IHttpActionResult)Ok(data) : BadRequest();
                    }else
                        return new System.Web.Http.Results.ResponseMessageResult(Request.CreateErrorResponse( (HttpStatusCode)420, new HttpError("Max Invites"))
                );

            }else
                return BadRequest();
        }


        [HttpGet]
        [Route("api/AcceptRSVP")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> AcceptRSVP(Guid rsvpId)
        {
            //var rsvpStatus = rRepo.checkIfAcceptedDeclined(rsvpId, "accepted");

            //if (rsvpStatus != null)
            //{
            //    if ((bool)rsvpStatus)
            //    {
                    var data = await rRepo.acceptRSVP(rsvpId);
                    return (data) ? (IHttpActionResult)Ok(data) : BadRequest();
            //    }
            //    else
            //        return new System.Web.Http.Results.ResponseMessageResult(Request.CreateErrorResponse((HttpStatusCode)420, new HttpError("This RSVP was already declined")));
            //}
            //else
            //    return BadRequest();
        }

        [HttpGet]
        [Route("api/DeclineRSVP")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> DeclineRSVP(Guid rsvpId)
        {
            //var rsvpStatus = rRepo.checkIfAcceptedDeclined(rsvpId, "accepted");

            //if (rsvpStatus != null)
            //{
            //    if ((bool)rsvpStatus)
            //    {
                    var data = await rRepo.declineRSVP(rsvpId);

                    return (data) ? (IHttpActionResult)Ok(data) : BadRequest();
            //    }
            //    else
            //        return new System.Web.Http.Results.ResponseMessageResult(Request.CreateErrorResponse((HttpStatusCode)420, new HttpError("This RSVP was already accepted")));
            //}
            //else
            //    return BadRequest();
        }

        [HttpGet]
        [Route("api/CancelMyRSVP")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> CancelMyRSVP(Guid eventId, Guid clientId)
        {
            var data = await rRepo.cancelMyRSVP(eventId, clientId);

            return (data) ? (IHttpActionResult)Ok(data) : BadRequest();
        }


        [Route("api/DeleteRSVP")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> DeleteRSVP(Guid rsvpId)
        {
            var data = await rRepo.deleteRSVP(rsvpId);

            return (data) ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/CheckIfHasRSVP")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult CheckIfHasRSVP(Guid eventId, Guid clientId)
        {
            var data =  rRepo.checkIfHasRSVP(eventId, clientId);

            return (data) ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetMyRSVP")]
        [ResponseType(typeof(RSVPItems))]
        public IHttpActionResult GetMyRSVP(Guid eventId, Guid clientId)
        {
            var data = rRepo.getMyRSVP(eventId, clientId);

            return (data != null) ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/getEventRSVPs")]
        [ResponseType(typeof(List<EventItems>))]
        public IHttpActionResult getEventRSVPs(Guid eventId)
        {
            var data = rRepo.getEventRSVPs(eventId);

            return (data != null) ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/getWhosComing")]
        [ResponseType(typeof(List<EventItems>))]
        public IHttpActionResult GetWhosComing(Guid eventId)
        {
            var data = rRepo.getWhosRSVPComing(eventId);

            return (data != null) ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/getWhosInterested")]
        [ResponseType(typeof(List<EventItems>))]
        public IHttpActionResult getWhosInterested(Guid eventId)
        {
            var data = rRepo.GetWhosRSVPInterested(eventId);

            return (data != null) ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetEventsIRSVP")]
        [ResponseType(typeof(List<EventItems>))]
        public IHttpActionResult GetEventIRSVP(Guid clientId)
        {
            var data = rRepo.getEventsIRSVP(clientId);

            return (data != null) ? (IHttpActionResult)Ok(data) : BadRequest();
        }
    }
}
