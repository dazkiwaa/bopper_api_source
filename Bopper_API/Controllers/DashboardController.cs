﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Bopper.Data.Model;
using Bopper.Models;
using Bopper.ViewModels;

namespace Bopper.Controllers
{
    public class DashboardController : ApiController
    {
        private readonly EventRepo eventRepo = new EventRepo();
        private readonly RSVPRepo rsvpRepo = new RSVPRepo();
        private readonly ContactsRepo contactsRepo = new ContactsRepo();

        [HttpGet]
        [Route("api/GetDashboardStat")]
        [ResponseType(typeof(Dashboard))]
        public async Task<IHttpActionResult> GetDashboardStat(Guid clientId)
        {

            var eventsRSVPed = rsvpRepo.getEventsIRSVP(clientId).Count;
            var amountSpent = rsvpRepo.getEventsIRSVP(clientId).Sum(x => x.cost);
            var allContacts = contactsRepo.getNumberOfContacts(clientId);
            var myEventsCreated = eventRepo.getNumberOfPostsIPost(clientId);
            var dashboardItems = new Dashboard
            {
                TotalAmountPaid = amountSpent,
                TotalRSVP = eventsRSVPed,
                TotalUsers = allContacts,
                TotalEventsCreated = myEventsCreated
            };
            //var data = await _chatRepo.createMessage(msg);
            if (dashboardItems != null)
                return Ok(dashboardItems);
            return BadRequest();
        }
        [HttpGet]
        [Route("api/GetEventStat")]
        [ResponseType(typeof(EventStat))]
        public async Task<IHttpActionResult> GetEventStat(Guid clientId)
        {
            var eventStatList = new List<EventStat>();
            var lastSixMonths = Enumerable
                .Range(0, 5)
                .Select(i => DateTime.Now.AddMonths(i - 5));
            foreach (var lastSixMonth in lastSixMonths)
            {
                var eventsData = eventRepo.getEventsIPosted(clientId).Where(x => x.timeStamp.Month == lastSixMonth.Month);
                var countConcert = 0;
                var countTravel = 0;
                var countNightLife = 0;
                var countSporting = 0;
                var countFood = 0;
                foreach (var eventData in eventsData)
                {
                    if (eventData.eventType.Contains("Concert"))
                    {
                        countConcert++;
                    }
                    if (eventData.eventType.Contains("Travel"))
                    {
                        countTravel++;
                    }
                    if (eventData.eventType.Contains("NightLife"))
                    {
                        countNightLife++;
                    }
                    if (eventData.eventType.Contains("Sporting"))
                    {
                        countSporting++;
                    }
                    if (eventData.eventType.Contains("Food"))
                    {
                        countFood++;
                    }
                }

                var eventStatData = new EventStat
                {
                    Month = lastSixMonth.ToString("MMMM"),
                    Concert = countConcert,
                    Food = countFood,
                    NightLife = countNightLife,
                    Sporting = countSporting,
                    Travel = countTravel
                };

                eventStatList.Add(eventStatData);
            }
            if (eventStatList != null)
                return Ok(eventStatList);
            return BadRequest();
        }
    }
}
