﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bopper.Models;
using Bopper.ViewModels;
using System.Web.Http.Description;
using System.Threading.Tasks;

namespace Bopper.Controllers
{
    public class InviteController : ApiController
    {

        private readonly InviteRepo _inviteRepo = new InviteRepo();

        [HttpPost]
        [Route("api/SendInvite")]
        [ResponseType(typeof(InviteItems))]
        public async Task<IHttpActionResult> SendInvite(InviteItems inv)
        {
            var data = await _inviteRepo.sendInvite(inv);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpPost]
        [Route("api/SendMultipleInvites")]
        [ResponseType(typeof(List<InviteItems>))]
        public async Task<IHttpActionResult> SendMultipleInvites(List<InviteItems> multiInvites)
        {
            var data = await _inviteRepo.sendMultipleInvites(multiInvites);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }


        [HttpGet]
        [Route("api/AcceptInvite")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> AcceptInvite(Guid inviteId)
        {
            var data = await _inviteRepo.acceptInvite(inviteId);

            return data ? (IHttpActionResult)Ok(data) : BadRequest();
        }


        [HttpGet]
        [Route("api/RejectInvite")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> RejectInvite(Guid inviteId)
        {
            var data = await _inviteRepo.declineInvite(inviteId);

            return data ? (IHttpActionResult)Ok(data) : BadRequest();
        }


        [Route("api/DeleteInvite")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> DeleteInvite(Guid inviteId)
        {
            var data = await _inviteRepo.deleteInvite(inviteId);

            return data ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetInvitesISent")]
        [ResponseType(typeof(List<InviteClientItems>))]
        public async Task<IHttpActionResult> GetInvitesISent(Guid inviteId)
        {
            var data =  _inviteRepo.getInvitesISent(inviteId);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetInvitesIReceived")]
        [ResponseType(typeof(List<InviteClientItems>))]
        public async Task<IHttpActionResult> GetInvitesIReceived(Guid inviteId)
        {
            var data = _inviteRepo.getInvitesIReceived(inviteId);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpPost]
        [Route("api/RequestInvite")]
        [ResponseType(typeof(InviteRequestItems))]
        public async Task<IHttpActionResult> RequestInvite(InviteRequestItems rqi)
        {
            var data = _inviteRepo.requestInvite(rqi);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetInviteById")]
        [ResponseType(typeof(InviteItems))]
        public IHttpActionResult GetInviteById(Guid inviteId)
        {
            var data = _inviteRepo.getInviteById(inviteId);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }


        //[HttpGet]
        //[Route("api/GetWhosComing")]
        //[ResponseType(typeof(List<InviteClientItems>))]
        //public async Task<IHttpActionResult> GetWhosComing(Guid eventId)
        //{
        //    var data = _inviteRepo.getWhosComing(eventId);

        //    return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        //}

        //[HttpGet]
        //[Route("api/GetWhoseInterested")]
        //[ResponseType(typeof(List<InviteClientItems>))]
        //public async Task<IHttpActionResult> GetWhosInterested(Guid eventId)
        //{
        //    var data = _inviteRepo.getWhosInterest(eventId);

        //    return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        //}

        [HttpGet]
        [Route("api/CheckIfHasInvite")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> CheckIfHasInvite(Guid eventId, Guid userId)
        {
            var data = _inviteRepo.checkIfHasInvite(eventId, userId);

            return data ? (IHttpActionResult)Ok(data) : BadRequest();
        }


        [HttpGet]
        [Route("api/CheckIfHasInviteRequest")]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> CheckIfHasInviteRequest(Guid eventId, Guid userId)
        {
            var data = _inviteRepo.checkIfHasRequestedInvite(eventId, userId);

            return data ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetInviteByEventIdClientId")]
        [ResponseType(typeof(InviteItems))]
        public async Task<IHttpActionResult> GetInvitesByEventIdClientId(Guid eventId, Guid clientId)
        {
            var data = _inviteRepo.getInvite(eventId, clientId);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }

        [HttpGet]
        [Route("api/GetMyEventInvitees")]
        [ResponseType(typeof(ContactItems))]
        public IHttpActionResult GetMyEventInvitees(Guid eventId)
        {
            var data = _inviteRepo.getEventInvites(eventId);

            return data != null ? (IHttpActionResult)Ok(data) : BadRequest();
        }
    }
}
