﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Bopper.Data;

namespace Bopper.Models
{
    public class BopperContext : DbContext
    {
        public BopperContext(): base("name=BopperContext")
        {

        }

        public DbSet<Chat> Chats { get; set; }
        public DbSet<CheckIn> CheckIns { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Invite> Invites { get; set; }
        public DbSet<InviteRequest> InviteRequests { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<RSVP> RSVPs { get; set; }

        public DbSet<Data.Model.Review> Reviews { get; set; }

        public DbSet<Data.Model.FbClientPics> FbClientPics { get; set; }
        public DbSet<EventChatMessages> EventChatMessages { get; set; }
        public DbSet<DM_Messages> DM_Messages { get; set; }
        public DbSet<DirectMessages> DirectMessages { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Data.Model.SubEvent> SubEvents { get; set; }
    }
}