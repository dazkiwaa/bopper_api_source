﻿using Bopper.Data;
using Bopper.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bopper.Models
{
    #region Interface
    interface ICommentonArticle
    { Task<EventChatItems> commentOnArticle(EventChatItems cm); }

    interface ICommentOnArticleMentor
    { Task<EventChatItems> commentOnArticleMentor(EventChatItems cm); }


    interface IEditComment
    { Task<EventChatItems> editComment(EventChatItems cm); }
    interface IDeleteComment
    { Task<bool> deleteComment(Guid commentId); }
    interface IGetArticleComments
    { List<EventChatReturnItems> getArticleComments(Guid articleId); }
    #endregion

    public class EventChatRepo : ICommentonArticle, IEditComment, IDeleteComment, IGetArticleComments, ICommentOnArticleMentor
    {

        BopperContext db = new BopperContext();

        public EventChatRepo()
        { db.Configuration.LazyLoadingEnabled = false; }

        public async Task<EventChatItems> commentOnArticle(EventChatItems cm)
        {

            cm.commentId = Guid.NewGuid();

            cm.timeStamp = DateTime.UtcNow;//CommonMethods.getKenyaTime();

            var comment = new EventChatMessages()
            {
                EventId = cm.eventId,
                ClientId = cm.clientId,
                Message = cm.comment,
                Id = cm.commentId,
                Image1 = cm.image1,
                Image2 = cm.image2,
                Image3 = cm.image3,
                TimeStamp = cm.timeStamp,
                TimeZone = cm.timeZone
            };

            try
            {
                db.EventChatMessages.Add(comment);

                await db.SaveChangesAsync();

                //var eventClient = (from e in db.Events
                //              join c in db.Clients on e.PosterId equals c.Id
                //              where e.Id == comment.EventId
                //              select new { e.PosterId, e.Id, e.Title, c.FullNames }).FirstOrDefault();

                var eventClient = (from e in db.Events
                                      join ec in db.EventChatMessages on e.Id equals ec.EventId
                                      join c in db.Clients on ec.ClientId equals c.Id
                                      where e.Id == comment.EventId
                                      select new { c.FullNames, c.ProfPic, e.Id, e.Title, e.PosterId }).FirstOrDefault();

                //Notify the merchant here
                //var mentor = (from a in db.Articles
                //              join m in db.Mentors on a.AuthorId equals m.MentorId
                //              select new { m.MentorId }).FirstOrDefault();

                if (eventClient != null)
                {
                    if (eventClient.PosterId != comment.ClientId)
                    {
                        var notification = new NotificationItems()
                        {
                            clientId = eventClient.PosterId,
                            notificationType = NotificationType.MENTION.ToString(),
                            notificationMessage = eventClient.FullNames + " commented on your event.",
                            timeStamp = comment.TimeStamp,
                            otherId = eventClient.Id
                        };

                        await new NotificationRepo().createNotificationHub(notification);
                    }
                }

                //if (mentor != null)
                //{
                //    var notification = new NotificationItems()
                //    {
                //        clientId = mentor.MentorId,
                //        notificationType = NotificationType.DIRECT_MSG.ToString(),
                //        otherId = comment.CommentId,
                //        timeStamp = comment.TimeStamp,
                //        notificationMessage = db.Clients.Where(c => c.ClientId == comment.ClientId).FirstOrDefault().Full_Names + " commented on your post."
                //    };

                //    await new NotificationsRepo().createNotification(notification);
                //}

                return cm;
            }
            catch (Exception r)
            {
                Debug.WriteLine(r.Message);
                Debug.WriteLine(r.InnerException.Message);

                return null;
            }
        }

        public Task<EventChatItems> commentOnArticleMentor(EventChatItems cm)
        {

            throw new NotImplementedException();
        }

        public async Task<bool> deleteComment(Guid commentId)
        {
            var comment = db.EventChatMessages.Where(c => c.EventId == commentId).FirstOrDefault();

            if (comment != null)
            {
                try
                {
                    db.EventChatMessages.Remove(comment);

                    await db.SaveChangesAsync();

                    return true;
                }
                catch (Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);

                    return false;
                }
            }
            else
                return false;
        }

        public async Task<EventChatItems> editComment(EventChatItems cm)
        {
            var comment = db.EventChatMessages.Where(c => c.EventId == cm.commentId).FirstOrDefault();

            if (comment != null)
            {
                comment.Message = cm.comment;
                comment.Image1 = cm.image1;
                comment.Image2 = cm.image2;
                comment.Image3 = cm.image3;

                try
                {
                    db.Entry(comment).State = System.Data.Entity.EntityState.Modified;

                    await db.SaveChangesAsync();

                    return cm;
                }
                catch (Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);

                    return null;
                }
            }
            else
                return null;
        }

        public List<EventChatReturnItems> getArticleComments(Guid articleId)
        {
            var articleComments = new List<EventChatReturnItems>();

            //var comments = db.Comments.Where(c => c.ArticleId == articleId);

            var comments = (from c in db.EventChatMessages
                            join cl in db.Clients on c.ClientId equals cl.Id
                            where c.EventId == articleId
                            select new
                            {
                                c.Id,
                                c.ClientId,
                                c.Message,
                                c.EventId,
                                c.Image1,
                                c.Image2,
                                c.Image3,
                                c.TimeStamp,
                                cl.FullNames,
                                cl.ProfPic,
                                c.TimeZone
                            });

            if (comments != null)
            {
                foreach (var c in comments)
                {
                    var comment = new EventChatReturnItems()
                    {
                        eventId = c.Id,
                        clientId = c.ClientId,
                        comment = c.Message,
                        commentId = c.EventId,
                        image1 = c.Image1,
                        image2 = c.Image2,
                        image3 = c.Image3,
                        timeStamp = c.TimeStamp,
                        clientName = c.FullNames,
                        clientPic = c.ProfPic
                    };

                    try
                    {
                        comment.timeStamp = CommonMethods.getTimeZonedTime(comment.timeStamp, comment.timeZone);
                    }
                    catch (Exception r)
                    {
                        Debug.WriteLine(r.Message);
                        Debug.WriteLine(r.InnerException.Message);
                    }


                    articleComments.Add(comment);
                }

                return articleComments.OrderByDescending(a => a.timeStamp).ToList();
            }
            else
                return null;
        }

    }
}