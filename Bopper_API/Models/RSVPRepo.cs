﻿using Bopper.Data;
using Bopper.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bopper.Models
{
    #region Interfaces
    interface ICreateRSVP { Task<RSVPItems> createRSVP(RSVPItems rsvp); }
    interface ICheckIfHasRSVP { bool checkIfHasRSVP(Guid eventId, Guid clientId); }
    interface IGetEventRSVPS { List<RSVPItems> getEventRSVPs(Guid eventId); }
    interface IGetMyRSVP { RSVPItems getMyRSVP(Guid eventId, Guid clientIs); }
    interface IAcceptRSVP { Task<bool> acceptRSVP(Guid rsvpId); }
    interface IDeclineRSVP { Task<bool> declineRSVP(Guid rsvpId); }
    interface IGetWhosRSVPComing { List<RSVPItems> getWhosRSVPComing(Guid eventId); }
    interface IGetWhosRSVPInterested { List<RSVPItems> GetWhosRSVPInterested(Guid eventId); }
    interface IGetEventSIRSVP { List<EventItems> getEventsIRSVP(Guid clientId); }
    interface IDeleteRSVP { Task<bool> deleteRSVP(Guid rsvpId); }

    interface ICheckIfAcceptedDeclined
    { bool? checkIfAcceptedDeclined(Guid rsvpId, string status);}

    interface ICancelMyRSVP
    { Task<bool> cancelMyRSVP(Guid eventId, Guid clientId); }
    #endregion

    public class RSVPRepo : ICreateRSVP, ICheckIfHasRSVP, IGetEventRSVPS,
        IGetMyRSVP, IAcceptRSVP, IDeclineRSVP, IGetWhosRSVPComing, IGetWhosRSVPInterested,
        IGetEventSIRSVP, IDeleteRSVP, ICheckIfAcceptedDeclined, ICancelMyRSVP
    {
        BopperContext db = new BopperContext();

        public RSVPRepo() { db.Configuration.LazyLoadingEnabled = false; }


        public async Task<bool> acceptRSVP(Guid rsvpId)
        {
            var rsvp = db.RSVPs.Where(r => r.Id == rsvpId).FirstOrDefault();
            if (rsvp != null)
            {
                rsvp.Status = "ACCEPTED";

                try
                {
                    db.Entry(rsvp).State = System.Data.Entity.EntityState.Modified;
                    await db.SaveChangesAsync();

                    //Create the contact here

                    var recipient = db.Clients.Where(x => x.Id == rsvp.ClientId).First();
                    var host = (from c in db.Clients
                                join e in db.Events on c.Id equals e.PosterId
                                where e.PosterId == rsvp.EventId
                                select new { c }).FirstOrDefault();

                    if(recipient != null && host != null)
                    {
                        var cRepo = new ContactsRepo();
                        //Check if added as cotnact then add

                        if (cRepo.checkIfAddedContactX(recipient.Id, host.c.Id))
                        {
                            await cRepo.addContact(new ContactItems()
                            {
                                contactId = host.c.Id,
                                userId = recipient.Id
                            });
                        }
                    }

                    return true;
                }
                catch (Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);
                    return false;
                }
            }
            else
                return false;
        }

        public bool checkIfHasRSVP(Guid eventId, Guid clientId)
        {
            return ((db.RSVPs.Where(r => r.EventId == eventId && r.ClientId == clientId).FirstOrDefault()) 
                == null) ? true : false;
        }

        public async Task<RSVPItems> createRSVPfromInvite(RSVPItems rsvp)
        {
            rsvp.rsvpId = Guid.NewGuid();
            rsvp.timeStamp = DateTime.Now;

            try
            {
                var rsvpx = new RSVP()
                {
                    ClientId = rsvp.userId,
                    EventId = rsvp.eventId,
                    Id = rsvp.rsvpId,
                    InviteId = rsvp.inviteId,
                    Status = "PENDING",
                    Timestamp = rsvp.timeStamp
                };

                db.RSVPs.Add(rsvpx);
                await db.SaveChangesAsync();              

                return rsvp;
            }
            catch (Exception r)
            {
                Debug.WriteLine(r.Message);
                Debug.WriteLine(r.InnerException.Message);
                return null;
            }
        }

        public async Task<RSVPItems> createRSVP(RSVPItems rsvp)
        {          
            rsvp.rsvpId = Guid.NewGuid();
            rsvp.timeStamp = DateTime.Now;

            try
            {
                var rsvpx = new RSVP() {
                    ClientId = rsvp.userId,
                    EventId = rsvp.eventId,
                    Id = rsvp.rsvpId,
                    InviteId = rsvp.inviteId,
                    Status = "PENDING",
                    Timestamp = rsvp.timeStamp
                };

                db.RSVPs.Add(rsvpx);
                await db.SaveChangesAsync();

                //Create notification
                //Send notification here           
                var eventPoster = (from e in db.Events
                                      join c in db.Clients on e.PosterId equals c.Id
                                      where e.Id == rsvp.eventId
                                      select new { c.FullNames, e.PosterId, e.Title }).FirstOrDefault();

                var eventRequester = (from c in db.Clients
                                      where c.Id == rsvp.userId
                                      select new { c.FullNames }).FirstOrDefault();

                var nR = new NotificationItems()
                {
                    notificationMessage = eventRequester.FullNames + " has requested for an RSVP for your " + eventPoster.Title + " event.",
                    notificationId = Guid.NewGuid(),
                    clientId = eventPoster.PosterId,
                    notificationType = NotificationType.REQUEST.ToString(),
                    otherId = rsvp.rsvpId,
                    timeStamp = rsvp.timeStamp
                };

                await new NotificationRepo().createNotificationHub(nR);

                return rsvp;
            }catch(Exception r)
            {
                Debug.WriteLine(r.Message);
                Debug.WriteLine(r.InnerException.Message);
                return null;
            }
        }

        public int? getNumberOfRSVPS(Guid eventId)
        {
            var eventX = (from e in db.Events
                          join r in db.RSVPs on e.Id equals r.EventId
                          where e.Id == eventId && r.Status.ToLower() != ("DECLINED").ToLower()
                          select new { });

            return (eventX != null) ? (int?)eventX.Count() : null;
        }

        public int? getMaxInvites(Guid eventId)
        {
            var eventX = db.Events.Where(e => e.Id == eventId).FirstOrDefault();

            return (eventX != null) ? (int?)eventX.MaxGuests : null;
        }

        public async Task<bool> declineRSVP(Guid rsvpId)
        {
            var rsvp = db.RSVPs.Where(r => r.Id == rsvpId).FirstOrDefault();
            if (rsvp != null)
            {
                rsvp.Status = "DECLINED";

                try
                {
                    db.Entry(rsvp).State = System.Data.Entity.EntityState.Modified;
                    await db.SaveChangesAsync();

                    return true;
                }
                catch (Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);
                    return false;
                }
            }
            else
                return false;
        }

        public async Task<bool> deleteRSVP(Guid rsvpId)
        {
            var rsvp = db.RSVPs.Where(r => r.Id == rsvpId).FirstOrDefault();

            try
            {
                db.RSVPs.Remove(rsvp);
                await db.SaveChangesAsync();

                return true;
            }catch(Exception r)
            {
                Debug.WriteLine(r.Message);
                Debug.Write(r.InnerException.Message);
                return false;
            }
        }

        public List<RSVPItems> getEventRSVPs(Guid eventId)
        {
            var lstRspvx = new List<RSVPItems>();

            var rsvpsE = (from r in db.RSVPs
                          join c in db.Clients on r.ClientId equals c.Id
                          where r.EventId == eventId
                          select new {
                              c.FullNames,
                              c.ProfPic,
                              c.Email,
                              r.ClientId,
                              r.EventId,
                              r.Id,
                              r.Status,
                              r.InviteId,
                              r.Timestamp,
                          });

            foreach(var r in rsvpsE)
            {
                lstRspvx.Add(new RSVPItems() {
                    eventId = r.EventId,
                    inviteId = r.InviteId,
                    rsvpId = r.Id,
                    status = r.Status,
                    timeStamp = r.Timestamp,
                    userId = r.ClientId,
                    clientEmail = r.Email,
                    clientName = r.FullNames,
                    clientPic = r.ProfPic
                });
            }

            return lstRspvx.OrderByDescending(l => l.timeStamp).ToList();
        }

        public List<EventItems> getEventsIRSVP(Guid clientId)
        {
            var myRSVPs = new List<EventItems>();

            var eventIRSVP = (from e in db.Events
                              join r in db.RSVPs on e.Id equals r.EventId
                              where r.ClientId == clientId && r.Status != "DECLINED"
                              select new {
                                  e.Id,
                                  e.Title,
                                  e.Date,
                                  e.StartTime,
                                  e.EndTime,
                                  e.Location,
                                  e.Description,
                                  e.Cost,
                                  e.MaxGuests,
                                  e.PlaceName,
                                  e.Photo1,
                                  e.Photo2,
                                  e.Photo3,
                                  e.Photo4,
                                  e.Photo5,
                                  e.EventType,
                                  e.PosterId,
                                  e.TimeStamp,
                                  e.State,
                                  e.Country,
                                  e.City,
                                  e.Classification,
                                  e.PrivacyFlag,
                                  e.EventAccess
                              }).ToList();

            foreach (var e in eventIRSVP)
            {
                LocationItems loc = CommonMethods.convertDbGeoToLocItems(e.Location);
                loc.placeName = e.PlaceName;

                myRSVPs.Add(new EventItems()
                {
                    eventId = e.Id,
                    title = e.Title,
                    date = e.Date,
                    startTime = e.StartTime,
                    endTime = e.EndTime,
                    location = loc,
                    description = e.Description,
                    cost = (float)e.Cost,
                    maxGuests = e.MaxGuests,
                    photo1 = e.Photo1,
                    photo2 = e.Photo2,
                    photo3 = e.Photo3,
                    photo4 = e.Photo4,
                    photo5 = e.Photo5,
                    City = e.City,
                    Classification = e.Classification,
                    Country = e.Country,
                    PrivacyFlag = e.PrivacyFlag,
                    eventType = e.EventType,
                    posterId = e.PosterId,
                    eventAccess = e.EventAccess,
                    timeStamp = (DateTime)e.TimeStamp
                });
            }

            return myRSVPs;
        }

        public RSVPItems getMyRSVP(Guid eventId, Guid clientIs)
        {
            var rsvp = db.RSVPs.Where(r => r.EventId == eventId && r.ClientId == clientIs).FirstOrDefault();

            var rsvpX = (rsvp != null) ? new RSVPItems() {
                eventId = rsvp.EventId,
                inviteId = rsvp.InviteId,
                rsvpId = rsvp.Id,
                status = rsvp.Status,
                timeStamp = rsvp.Timestamp,
                userId = rsvp.ClientId
            } : null;

            return rsvpX;
        }

        public List<RSVPItems> getWhosRSVPComing(Guid eventId)
        {
            var lstRspvx = new List<RSVPItems>();

            var rsvpsE = (from r in db.RSVPs
                          join c in db.Clients on r.ClientId equals c.Id
                          where r.EventId == eventId && r.Status.ToLower().Equals("accepted".ToLower())
                          select new
                          {
                              c.FullNames,
                              c.ProfPic,
                              c.Email,
                              r.ClientId,
                              r.EventId,
                              r.Id,
                              r.Status,
                              r.InviteId,
                              r.Timestamp,
                          });

            foreach (var r in rsvpsE)
            {
                lstRspvx.Add(new RSVPItems()
                {
                    eventId = r.EventId,
                    inviteId = r.InviteId,
                    rsvpId = r.Id,
                    status = r.Status,
                    timeStamp = r.Timestamp,
                    userId = r.ClientId,
                    clientEmail = r.Email,
                    clientName = r.FullNames,
                    clientPic = r.ProfPic
                });
            }

            return lstRspvx.OrderByDescending(l => l.timeStamp).ToList();
        }

        public List<RSVPItems> GetWhosRSVPInterested(Guid eventId)
        {
            var lstRspvx = new List<RSVPItems>();

            var rsvpsE = (from r in db.RSVPs
                          join c in db.Clients on r.ClientId equals c.Id
                          where r.EventId == eventId && r.Status.ToLower().Equals("pending".ToLower())
                          select new
                          {
                              c.FullNames,
                              c.ProfPic,
                              c.Email,
                              r.ClientId,
                              r.EventId,
                              r.Id,
                              r.Status,
                              r.InviteId,
                              r.Timestamp,
                          });

            foreach (var r in rsvpsE)
            {
                lstRspvx.Add(new RSVPItems()
                {
                    eventId = r.EventId,
                    inviteId = r.InviteId,
                    rsvpId = r.Id,
                    status = r.Status,
                    timeStamp = r.Timestamp,
                    userId = r.ClientId,
                    clientEmail = r.Email,
                    clientName = r.FullNames,
                    clientPic = r.ProfPic
                });
            }

            return lstRspvx.OrderByDescending(l => l.timeStamp).ToList();
        }

        public bool? checkIfAcceptedDeclined(Guid rsvpId, string status)
        {
            var rsvpx = db.RSVPs.Where(r => r.Id == rsvpId).FirstOrDefault();

            if (rsvpx != null)
            {
                if (rsvpx.Status.ToLower().Equals(("pending").ToLower()))
                    return true;
                else if (rsvpx.Status.ToLower().Equals(status.ToLower()))
                    return false;
                else
                    return null;
            }
            else
                return null;
        }

        public async Task<bool> cancelMyRSVP(Guid eventId, Guid clientId)
        {
            var myRSVP = db.RSVPs.Where(r => r.EventId == eventId && r.ClientId == clientId).FirstOrDefault();

            if (myRSVP != null)
            {
                myRSVP.Status = "DECLINED";
                try
                {
                    db.Entry(myRSVP).State = System.Data.Entity.EntityState.Modified;

                    await db.SaveChangesAsync();

                    return true;
                }
                catch (Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);

                    return false;
                }
            }
            else
                return false;
        }
    }
}