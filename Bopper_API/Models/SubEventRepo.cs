﻿using Bopper.Data.Model;
using Bopper.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bopper.Models
{

    #region Interfaces
    interface IAddSubEvent
    { Task<SubEventItems> addSubEvent(SubEventItems sb); }
    interface IGetMySubEvents
    { List<SubEventItems> getMySubEvents(Guid clientId); }
    interface IGetEventSubEvents
    { List<SubEventItems> getEventSubEvents(Guid eventId); }
    #endregion

    public class SubEventRepo : IAddSubEvent, IGetMySubEvents, IGetEventSubEvents
    {

        BopperContext db = new BopperContext();

        async public Task<SubEventItems> addSubEvent(SubEventItems sb)
        {
            sb.id = Guid.NewGuid();
            sb.timeStamp = DateTime.UtcNow;

            try
            {
                var subEv = new SubEvent();
                subEv.Id = sb.id;
                subEv.EventId = sb.eventId;
                subEv.ClientId = sb.clientId;
                subEv.Title = sb.title;
                subEv.Description = sb.description;
                subEv.Location = CommonMethods.convertLocItemsToDBGeo(sb.location);
                subEv.PrivacyFlag = sb.privacyFlag;
                subEv.Cost = sb.cost;
                subEv.Photo1 = sb.photo1;
                subEv.Photo2 = sb.photo2;
                subEv.Photo3 = sb.photo3;
                subEv.Photo4 = sb.photo4;
                subEv.Photo5 = sb.photo5;
                subEv.TimeStamp = sb.timeStamp;

                db.SubEvents.Add(subEv);

                await db.SaveChangesAsync();

                return sb;
            }
            catch(Exception r){
                Debug.WriteLine(r.Message);
                Debug.WriteLine(r.InnerException.Message);
                return null;
            }
        }

        public List<SubEventItems> getEventSubEvents(Guid eventId)
        {
            var lstSubEvents = new List<SubEventItems>();

            var subEvents = (from s in db.SubEvents
                             join e in db.Events on s.EventId equals e.Id
                             join c in db.Clients on s.ClientId equals c.Id
                             where s.EventId == eventId
                             select new {
                                 s.Id,
                                 s.EventId,
                                 s.ClientId,
                                 s.Title,
                                 s.Description,
                                 s.Location,
                                 s.PrivacyFlag,
                                 s.Cost,
                                 s.Photo1,
                                 s.Photo2,
                                 s.Photo3,
                                 s.Photo4,
                                 s.Photo5,
                                 s.TimeStamp,
                                 c.FullNames,
                                 c.PhoneNumber,
                                 c.ProfPic,
                                 c.Email
                             });


            foreach (var s in subEvents)
                lstSubEvents.Add(new SubEventItems() {
                    clientId = s.ClientId,
                    cost = s.Cost,
                    description = s.Description,
                     email = s.Email,
                     eventId = s.EventId,
                     eventTitle = s.Title,
                     fullNames = s.FullNames,
                     id = s.Id,
                     location = CommonMethods.convertDbGeoToLocItems(s.Location),
                     phoneNumber = s.PhoneNumber,
                     photo1 = s.Photo1,
                     photo2 = s.Photo2,
                     photo3 = s.Photo3,
                     photo4 = s.Photo4,
                     photo5 = s.Photo5,
                     privacyFlag = s.PrivacyFlag,
                     timeStamp = s.TimeStamp,
                     title = s.Title
                });


            return lstSubEvents.OrderByDescending(x => x.timeStamp).ToList();
        }

        public List<SubEventItems> getMySubEvents(Guid clientId)
        {
            var lstSubEvents = new List<SubEventItems>();

            var subEvents = (from s in db.SubEvents
                             join e in db.Events on s.EventId equals e.Id
                             join c in db.Clients on s.ClientId equals c.Id
                             where s.ClientId == clientId
                             select new
                             {
                                 s.Id,
                                 s.EventId,
                                 s.ClientId,
                                 s.Title,
                                 s.Description,
                                 s.Location,
                                 s.PrivacyFlag,
                                 s.Cost,
                                 s.Photo1,
                                 s.Photo2,
                                 s.Photo3,
                                 s.Photo4,
                                 s.Photo5,
                                 s.TimeStamp,
                                 c.FullNames,
                                 c.PhoneNumber,
                                 c.ProfPic,
                                 c.Email
                             });


            foreach (var s in subEvents)
                lstSubEvents.Add(new SubEventItems()
                {
                    clientId = s.ClientId,
                    cost = s.Cost,
                    description = s.Description,
                    email = s.Email,
                    eventId = s.EventId,
                    eventTitle = s.Title,
                    fullNames = s.FullNames,
                    id = s.Id,
                    location = CommonMethods.convertDbGeoToLocItems(s.Location),
                    phoneNumber = s.PhoneNumber,
                    photo1 = s.Photo1,
                    photo2 = s.Photo2,
                    photo3 = s.Photo3,
                    photo4 = s.Photo4,
                    photo5 = s.Photo5,
                    privacyFlag = s.PrivacyFlag,
                    timeStamp = s.TimeStamp,
                    title = s.Title
                });


            return lstSubEvents.OrderByDescending(x => x.timeStamp).ToList();
        }
    }
}