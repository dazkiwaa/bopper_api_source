﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bopper.ViewModels;
using System.Diagnostics;
using System.Threading.Tasks;
using Bopper.Data;

namespace Bopper.Models
{
    #region Interfaces
    interface ICheckIn
    { Task<CheckInItems> checkIn(CheckInItems chk); }

    interface IGetClientsInVicinity
    { List<ClientItems> getClientsInVicinity(LocationItems lcs); }

    interface IGetLatestCheckIn
    { CheckInItems getLatestsCheckIn(Guid clientId); }


    public class ClientComparer : IEqualityComparer<ClientItems>
    {
        public bool Equals(ClientItems x, ClientItems y)
        {
            return x.userId == y.userId;// && x.userId == y.userId;
        }

        public int GetHashCode(ClientItems obj)
        {
            return obj.userId.GetHashCode();
        }
    }

    #endregion

    public class CheckInRepo : ICheckIn, IGetClientsInVicinity, IGetLatestCheckIn
    {

        BopperContext db = new BopperContext();

        public CheckInRepo()
        { db.Configuration.LazyLoadingEnabled = false; }

        public async Task<CheckInItems> checkIn(CheckInItems chk)
        {
            chk.checkInId = Guid.NewGuid();
            chk.timeStamp = DateTime.Now;

            CheckIn ch = new CheckIn()
            {
                Id = chk.checkInId,
                ClientId = chk.clientId,
                Location = CommonMethods.convertLocItemsToDBGeo(chk.location),
                TimeStamp = chk.timeStamp
            };

            try
            {
                db.CheckIns.Add(ch);

                db.SaveChanges();

                return chk;

            }catch(Exception r)
            {
                Debug.WriteLine(r.Message);
                Debug.WriteLine(r.InnerException.Message);

                return null;
            }
        }

        public List<ClientItems> getClientsInVicinity(LocationItems lcs)
        {
            var allClients = new List<ClientItems>();

            var locs = CommonMethods.convertLocItemsToDBGeo(lcs);

            var clients = (from c in db.CheckIns
                           join cl in db.Clients on c.ClientId equals cl.Id
                           where c.Location.Distance(locs) <= 12000
                           select new {
                               cl.Id,
                               cl.FullNames,
                               cl.Email,
                               cl.ProfPic,
                               cl.PhoneNumber,
                               cl.DOB,
                               cl.Bio,
                               c.TimeStamp
                           }).OrderByDescending(t => t.TimeStamp).ToList();

            if (clients != null)
            {
                foreach(var c in clients)
                {
                    var clientItem = new ClientItems()
                    {
                        userId = c.Id,
                        fullNames = c.FullNames,
                        email = c.Email,
                        profPic = c.ProfPic,
                        phoneNumber = c.PhoneNumber,
                        dob = c.DOB,
                        bio = c.Bio
                    };

                    allClients.Add(clientItem);
                }

                return allClients.Distinct(new ClientComparer()).OrderBy(o => o.fullNames).ToList();
            }
            else
                return null;
        }

        public CheckInItems getLatestsCheckIn(Guid clientId)
        {
            throw new NotImplementedException();
        }
    }
}