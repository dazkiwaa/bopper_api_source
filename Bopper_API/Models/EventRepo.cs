﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bopper.ViewModels;
using System.Diagnostics;
using System.Threading.Tasks;
using Bopper.Data;
using System.Data.Entity;

namespace Bopper.Models
{
    #region Interfaces
    interface IAddEvent
    { //EventItems addEvent(EventItems evt);
        Task<EventItems> addEvent(EventItems evt);
    }

    interface IUpdateEvent
    { //EventItems updateEvent(EventItems evt);
        Task<EventItems> updateEvent(EventItems evt);
    }

    interface IDeleteEvent
    { //bool deleteEvent(Guid eId);
        Task<bool> deleteEvent(Guid eId);
    }

    interface IGetLatestEvent
    { //List<EventItems> getLatestEvents(LocationItems lcs);
        List<EventItems> getLatestEvents(LocationItems lcs);
    }

    interface IGetActiveEvents
    { //List<EventItems> getActiveEvents(LocationItems lcs, DateTime dte);
        List<EventItems> getActiveEvents(LocationItems lcs, DateTime dte);
    }

    interface IGetEventsIPosted
    { List<EventItems> getEventsIPosted(Guid clientId); }

    interface IGetNumberOfPostsIPost
    { int getNumberOfPostsIPost(Guid clientId); }

    interface IGetEvent
    { EventItems getEventById(Guid eventId); }

    interface IGetEventsByCity
    {
        List<EventItems> getEventsByCity(string city);
    }

    interface IGetAllEvents
    {
        List<EventItems> getAllEvents();
    }

    interface IGetLatestEventWithoutMine
    { //List<EventItems> getLatestEvents(LocationItems lcs);
        List<EventItems> getLatestEventsWithoutMine(LocationItems lcs, Guid clientId);
    }

    interface IGetEventsByCityWithoutMine
    {
        List<EventItems> getEventsByCityWithoutMine(string city, Guid clientId);
    }

    interface ICancelEvent
    { Task<bool> cancelEvent(Guid eventId); }

    interface IGetEventSpaces
    { EventSpaceItems getEventSpaces(Guid eventId); }

    interface ISearchForEvent
    { List<EventItems> searchForEvent(FilterItems evt); }
    #endregion

    interface IEditEventImageLinks
    { Task<bool> editEventImageLinks(Guid eventId, List<string> imageLinks); }

    

    public class EventRepo : IAddEvent, IUpdateEvent, IDeleteEvent, IGetLatestEvent, IGetNumberOfPostsIPost,
        IGetEventsIPosted, IGetEvent, IGetEventsByCity, IGetAllEvents, IGetLatestEventWithoutMine, IGetEventsByCityWithoutMine,
        ICancelEvent, IGetEventSpaces, ISearchForEvent, IEditEventImageLinks
    {
        BopperContext db = new BopperContext();

        public EventRepo()
        { db.Configuration.LazyLoadingEnabled = false; }

        public async Task<EventItems> addEvent(EventItems evt)
        {
            evt.timeStamp = DateTime.Now;
            evt.eventId = Guid.NewGuid();


            var evnte = new Event()
            {
                Id = evt.eventId,
                Title = evt.title,
                Date = evt.date,
                EndDate = evt.endDate,
                StartTime = evt.startTime,
                EndTime = evt.endTime,
                Location = CommonMethods.convertLocItemsToDBGeo(evt.location),
                PlaceName = evt.location.placeName,
                Description = evt.description,
                Cost = evt.cost,
                MaxGuests = evt.maxGuests,
                Photo1 = evt.photo1,
                Photo2 = evt.photo2,
                Photo3 = evt.photo3,
                Photo4 = evt.photo4,
                Photo5 = evt.photo5,
                City = evt.City,
                Classification = evt.Classification,
                Country = evt.Country,
                PrivacyFlag = evt.PrivacyFlag,
                EventAccess = evt.eventAccess,
                EventType = evt.eventType,
                PosterId = evt.posterId,
                State = evt.State,
                TimeStamp = DateTime.Now
            };


            //Set the event type
            if (evt.eventType.ToLower().Contains("night"))
                evnte.nightLife = true;

            if (evt.eventType.ToLower().Contains("active"))
                evnte.active = true;

            if (evt.eventType.ToLower().Contains("concert"))
                evnte.concert = true;

            if (evt.eventType.ToLower().Contains("travel"))
                evnte.travel = true;

            if (evt.eventType.ToLower().Contains("sport"))
                evnte.sporting = true;

            if (evt.eventType.ToLower().Contains("food"))
                evnte.food = true;

            db.Events.Add(evnte);

            try
            {

                await db.SaveChangesAsync();

                return evt;

            }catch(Exception r)
            {
                Debug.WriteLine(r.Message);
                Debug.WriteLine(r.InnerException.Message);

                return null; 
            }
        }

        public async Task<bool> cancelEvent(Guid eventId)
        {
            var ev = db.Events.Where(e => e.Id == eventId).FirstOrDefault();
            if (ev != null)
            {            
                try
                {
                    ev.EventAccess = "cancelled";
                    db.Entry(ev).State = System.Data.Entity.EntityState.Modified;
                    //await db.SaveChangesAsync();

                    //Get the rsvps for this event
                    var rsvps = db.RSVPs.Where(r => r.EventId == ev.Id);

                    //Send a cancel message to all
                    foreach(var r in rsvps)
                    {
                        var client = db.Clients.Where(c => c.Id == r.ClientId).FirstOrDefault();

                        if(client != null)
                        {
                            var nR = new NotificationItems()
                            {
                                notificationMessage = "Sorry to inform you that " + ev.Title + " has been cancelled.",
                                notificationId = Guid.NewGuid(),
                                clientId = client.Id,
                                notificationType = NotificationType.CANCEL.ToString(),
                                otherId = r.Id,
                                timeStamp = DateTime.Now
                            };

                            await new NotificationRepo().createNotificationHub(nR);
                        }
                    }

                    return true;
                }catch(Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);
                    return false;
                }
            }
            else
                return false;
            throw new NotImplementedException();
        }

        public async Task<bool> deleteEvent(Guid eId)
        {
            var evnte = db.Events.Where(e => e.Id == eId).FirstOrDefault();

            if (evnte != null)
            {

                db.Events.Remove(evnte);

                try {
                    await db.SaveChangesAsync();

                    return true;
                } catch(Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);

                    return false;
                }                
            }
            else
                return false;
        }

        public List<EventItems> getAllEvents()
        {
            var events = (from e in db.Events
                          join c in db.Clients on e.PosterId equals c.Id
                          where e.PrivacyFlag == false           
                          select new
                          {
                              e.Id,
                              e.Title,
                              e.Date,
                              e.EndDate,
                              e.StartTime,
                              e.EndTime,
                              e.Location,
                              e.PlaceName,
                              e.Description,
                              e.Cost,
                              e.Photo1,
                              e.Photo2,
                              e.Photo3,
                              e.Photo4,
                              e.Photo5,
                              e.City,
                              e.Classification,
                              e.Country,
                              e.PrivacyFlag,
                              e.EventType,
                              e.PosterId,
                              e.EventAccess,
                              e.MaxGuests,
                              e.State,
                              e.TimeStamp,
                              c.FullNames,
                              c.ProfPic,
                              c.Email
                          }).ToList();


            if (events != null)
            {
                events = events.OrderByDescending(e => e.Date).ToList();

                var latestEvents = new List<EventItems>();

                foreach (var e in events)
                {
                    LocationItems loc = CommonMethods.convertDbGeoToLocItems(e.Location);
                    loc.placeName = e.PlaceName;

                    latestEvents.Add(new EventItems()
                    {
                        eventId = e.Id,
                        title = e.Title,
                        endDate = e.EndDate,
                        date = e.Date,
                        startTime = e.StartTime,
                        endTime = e.EndTime,
                        location = loc,
                        description = e.Description,
                        cost = (float)e.Cost,
                        maxGuests = e.MaxGuests,
                        photo1 = e.Photo1,
                        photo2 = e.Photo2,
                        photo3 = e.Photo3,
                        photo4 = e.Photo4,
                        photo5 = e.Photo5,
                        eventType = e.EventType,
                        City = e.City,
                        Classification = e.Classification,
                        Country = e.Country,
                        PrivacyFlag = e.PrivacyFlag,
                        posterId = e.PosterId,
                        timeStamp = (DateTime)e.TimeStamp,
                        posterName = e.FullNames,
                        posterPic = e.ProfPic,
                        State = e.State
                    });
                }

                return latestEvents;
            }
            else
                return null;
        }

        public EventItems getEventById(Guid eventId)
        {
            //var e = db.Events.Where(r => r.Id == eventId).FirstOrDefault();
            var e = (from r in db.Events
                     join c in db.Clients on r.PosterId equals c.Id
                     where r.Id == eventId
                     select new
                     {
                         r.Id,
                         r.Title,
                         r.Date,
                         r.EndDate,
                         r.StartTime,
                         r.EndTime,
                         r.Location,
                         r.PlaceName,
                         r.Description,
                         r.Cost,
                         r.Photo1,
                         r.Photo2,
                         r.Photo3,
                         r.Photo4,
                         r.Photo5,
                         r.City,
                         r.Classification,
                         r.Country,
                         r.PrivacyFlag,
                         r.EventType,
                         r.PosterId,
                         r.EventAccess,
                         r.MaxGuests,
                         r.State,
                         r.TimeStamp,
                         c.FullNames,
                         c.ProfPic,
                         c.Email
                     }).FirstOrDefault();

            LocationItems loc = CommonMethods.convertDbGeoToLocItems(e.Location);
            loc.placeName = e.PlaceName;

            if (e != null)
            {
                var ev =  new EventItems()
                {
                    eventId = e.Id,
                    title = e.Title,
                    date = e.Date,
                    endDate = e.EndDate,
                    startTime = e.StartTime,
                    endTime = e.EndTime,
                    location = loc,
                    description = e.Description,
                    cost = (float)e.Cost,
                    maxGuests = e.MaxGuests,
                    photo1 = e.Photo1,
                    photo2 = e.Photo2,
                    photo3 = e.Photo3,
                    photo4 = e.Photo4,
                    photo5 = e.Photo5,
                    City = e.City,
                    Classification = e.Classification,
                    Country = e.Country,
                    PrivacyFlag = e.PrivacyFlag,
                    eventType = e.EventType,
                    posterId = e.PosterId,
                    eventAccess = e.EventAccess,
                    timeStamp = (DateTime)e.TimeStamp,
                    posterName = e.FullNames,
                    posterPic = e.ProfPic,
                    State = e.State
                };

                return ev;
            }
            else
                return null;
        }

        public List<EventItems> getEventsByCity(string city)
        {
            var events = (from e in db.Events
                          join c in db.Clients on e.PosterId equals c.Id
                          where e.City.Contains(city) && e.PrivacyFlag == false && e.EventAccess != "cancelled"//Distance of 10 km
                          //&& e.StartTime >= DateTime.Now && e.EndTime <= DateTime.Now
                          select new
                          {
                              e.Id,
                              e.Title,
                              e.Date,
                              e.EndDate,
                              e.StartTime,
                              e.EndTime,
                              e.Location,
                              e.PlaceName,
                              e.Description,
                              e.Cost,
                              e.MaxGuests,
                              e.Photo1,
                              e.Photo2,
                              e.Photo3,
                              e.Photo4,
                              e.Photo5,
                              e.EventAccess,                                                   
                              e.City,
                              e.Classification,
                              e.State,
                              e.Country,
                              e.PrivacyFlag,
                              e.EventType,
                              e.PosterId,
                              e.TimeStamp,
                              c.FullNames,
                              c.ProfPic,
                              c.Email
                          }).ToList();


            if (events != null)
            {
                events = events.OrderByDescending(e => e.Date).ToList();

                var latestEvents = new List<EventItems>();

                foreach (var e in events)
                {
                    LocationItems loc = CommonMethods.convertDbGeoToLocItems(e.Location);
                    loc.placeName = e.PlaceName;

                    latestEvents.Add(new EventItems()
                    {
                        eventId = e.Id,
                        title = e.Title,
                        date = e.Date,
                        endDate = e.EndDate,
                        startTime = e.StartTime,
                        endTime = e.EndTime,
                        location = loc,
                        description = e.Description,
                        cost = (float)e.Cost,
                        maxGuests = e.MaxGuests,
                        photo1 = e.Photo1,
                        photo2 = e.Photo2,
                        photo3 = e.Photo3,
                        photo4 = e.Photo4,
                        photo5 = e.Photo5,
                        eventType = e.EventType,
                        City = e.City,
                        Classification = e.Classification,
                        Country = e.Country,
                        PrivacyFlag = e.PrivacyFlag,
                        posterId = e.PosterId,
                        timeStamp = (DateTime)e.TimeStamp,
                        posterName = e.FullNames,
                        posterPic = e.ProfPic,
                        State = e.State
                    });
                }

                return latestEvents;
            }
            else
                return null;
        }

        public List<EventItems> getEventsByCityWithoutMine(string city, Guid clientId)
        {
            var events = (from e in db.Events
                          join c in db.Clients on e.PosterId equals c.Id
                          where e.City.Contains(city) && e.PrivacyFlag == false && e.EventAccess != "cancelled"//Distance of 10 km
                          //&& e.StartTime >= DateTime.Now && e.EndTime <= DateTime.Now
                          select new
                          {
                              e.Id,
                              e.Title,
                              e.Date,
                              e.EndDate,
                              e.StartTime,
                              e.EndTime,
                              e.Location,
                              e.PlaceName,
                              e.Description,
                              e.Cost,
                              e.MaxGuests,
                              e.Photo1,
                              e.Photo2,
                              e.Photo3,
                              e.Photo4,
                              e.Photo5,
                              e.EventAccess,
                              e.City,
                              e.Classification,
                              e.State,
                              e.Country,
                              e.PrivacyFlag,
                              e.EventType,
                              e.PosterId,
                              e.TimeStamp,
                              c.FullNames,
                              c.ProfPic,
                              c.Email
                          }).ToList();


            if (events != null)
            {
                events = events.OrderByDescending(e => e.Date).ToList();

                var latestEvents = new List<EventItems>();

                foreach (var e in events)
                {
                    LocationItems loc = CommonMethods.convertDbGeoToLocItems(e.Location);
                    loc.placeName = e.PlaceName;

                    latestEvents.Add(new EventItems()
                    {
                        eventId = e.Id,
                        title = e.Title,
                        date = e.Date,
                        endDate = e.EndDate,
                        startTime = e.StartTime,
                        endTime = e.EndTime,
                        location = loc,
                        description = e.Description,
                        cost = (float)e.Cost,
                        maxGuests = e.MaxGuests,
                        photo1 = e.Photo1,
                        photo2 = e.Photo2,
                        photo3 = e.Photo3,
                        photo4 = e.Photo4,
                        photo5 = e.Photo5,
                        eventType = e.EventType,
                        City = e.City,
                        Classification = e.Classification,
                        Country = e.Country,
                        PrivacyFlag = e.PrivacyFlag,
                        posterId = e.PosterId,
                        timeStamp = (DateTime)e.TimeStamp,
                        posterName = e.FullNames,
                        posterPic = e.ProfPic,
                        State = e.State
                    });
                }


                var myEvents = latestEvents.Where(x => x.posterId == clientId).ToList();

                if (myEvents != null)
                    foreach (var e in myEvents)
                        latestEvents.Remove(e);

                return latestEvents;
            }
            else
                return null;
        }

        public List<EventItems> getEventsIPosted(Guid clientId)
        {
            var events = (from e in db.Events
                          join c in db.Clients on e.PosterId equals c.Id
                          where e.PosterId == clientId
                          select new
                          {
                              e.Id,
                              e.Title,
                              e.Date,
                              e.EndDate,
                              e.StartTime,
                              e.EndTime,
                              e.Location,
                              e.PlaceName,
                              e.Description,
                              e.Cost,
                              e.MaxGuests,
                              e.Photo1,
                              e.Photo2,
                              e.Photo3,
                              e.Photo4,
                              e.City,
                              e.State,
                              e.Country,
                              e.Classification,
                              e.PrivacyFlag,
                              e.EventType,
                              e.PosterId,
                              e.TimeStamp,
                              c.FullNames,
                              c.ProfPic,
                              c.Email
                          }).ToList();


            if (events != null)
            {
                events = events.OrderByDescending(e => e.Date).ToList();

                var latestEvents = new List<EventItems>();

                foreach (var e in events)
                {
                    LocationItems loc = CommonMethods.convertDbGeoToLocItems(e.Location);
                    loc.placeName = e.PlaceName;

                    latestEvents.Add(new EventItems()
                    {
                        eventId = e.Id,
                        title = e.Title,
                        date = e.Date,
                        endDate = e.EndDate,
                        startTime = e.StartTime,
                        endTime = e.EndTime,
                        location = loc,
                        description = e.Description,
                        cost = (float)e.Cost,
                        maxGuests = e.MaxGuests,
                        photo1 = e.Photo1,
                        photo2 = e.Photo2,
                        photo3 = e.Photo3,
                        photo4 = e.Photo4,
                        City = e.City,
                        Classification = e.Classification,
                        Country = e.Country,
                        PrivacyFlag = e.PrivacyFlag,
                        eventType = e.EventType,
                        posterId = e.PosterId,
                        timeStamp = (DateTime)e.TimeStamp,
                        posterName = e.FullNames,
                        posterPic = e.ProfPic,
                        State = e.State
                    });
                }

             

                return latestEvents;
            }
            else
                return null;
        }

        public EventSpaceItems getEventSpaces(Guid eventId)
        {
            var rRepo = new RSVPRepo();

            var rsvps = rRepo.getNumberOfRSVPS(eventId);
            var maxGuests = rRepo.getMaxInvites(eventId);


            return (rsvps != null && maxGuests != null) ?  
                new EventSpaceItems()
                        {
                            maxSpaces = (int)maxGuests,
                            remainingSpaces = ((int)(maxGuests - rsvps))
                        } :
                            null;

            //if (rsvps != null && maxGuests != null)
            //    return new EventSpaceItems()
            //    {
            //        maxSpaces = (int)maxGuests,
            //        remainingSpaces = ((int)(maxGuests - rsvps))
            //    };
            //else
            //    return null;
        }

        public List<EventItems> getLatestEvents(LocationItems lcs)
        {

            var clLoc = CommonMethods.convertLocItemsToDBGeo(lcs);

            //var distanct = db.Events.FirstOrDefault().Location.Distance(clLoc);

            var events = (from e in db.Events
                          join c in db.Clients on e.PosterId equals c.Id
                          where e.Location.Distance(clLoc) <= 160934.4 && e.PrivacyFlag == false && e.EventAccess != "cancelled"//Distance of 10 km
                          //&& e.Date.Date >= DateTime.Now.Date
                          select new
                          {
                              e.Id,
                              e.Title,
                              e.Date,
                              e.EndDate,
                              e.StartTime,
                              e.EndTime,
                              e.Location,
                              e.PlaceName,
                              e.Description,
                              e.Cost,
                              e.MaxGuests,
                              e.Photo1,
                              e.Photo2,
                              e.Photo3,
                              e.Photo4,
                              e.City,
                              e.Classification,
                              e.State,
                              e.Country,
                              e.PrivacyFlag,
                              e.EventType,
                              e.PosterId,
                              e.TimeStamp,
                              c.FullNames,
                              c.ProfPic,
                              c.Email
                          }).ToList();


            if (events != null)
            {
                events = events.OrderByDescending(e => e.Date).ToList();

                var latestEvents = new List<EventItems>();

                foreach (var e in events)
                {
                    LocationItems loc = CommonMethods.convertDbGeoToLocItems(e.Location);
                    loc.placeName = e.PlaceName;

                    latestEvents.Add(new EventItems()
                    {
                        eventId = e.Id,
                        title = e.Title,
                        date = e.Date,
                        endDate = e.EndDate,
                        startTime = e.StartTime,
                        endTime = e.EndTime,
                        location = loc,
                        description = e.Description,
                        cost = (float)e.Cost,
                        maxGuests = e.MaxGuests,
                        photo1 = e.Photo1,
                        photo2 = e.Photo2,
                        photo3 = e.Photo3,
                        photo4 = e.Photo4,
                        eventType = e.EventType,
                        City = e.City,
                        Classification = e.Classification,
                        Country = e.Country,
                        PrivacyFlag = e.PrivacyFlag,
                        posterId = e.PosterId,
                        timeStamp = (DateTime)e.TimeStamp,
                        posterName = e.FullNames,
                        posterPic = e.ProfPic,
                        State = e.State
                    });
                }

                return latestEvents;
            }
            else
                return null;
        }

        public List<EventItems> getLatestEventsWithoutMine(LocationItems lcs, Guid clientId)
        {
            var clLoc = CommonMethods.convertLocItemsToDBGeo(lcs);

            //var distanct = db.Events.FirstOrDefault().Location.Distance(clLoc);

            var events = (from e in db.Events
                          join c in db.Clients on e.PosterId equals c.Id
                          where e.Location.Distance(clLoc) <= 160934.4 && e.PrivacyFlag == false && e.EventAccess != "cancelled"//Distance of 10 km
                          //&& e.Date.Date >= DateTime.Now.Date
                          select new
                          {
                              e.Id,
                              e.Title,
                              e.Date,
                              e.EndDate,
                              e.StartTime,
                              e.EndTime,
                              e.Location,
                              e.PlaceName,
                              e.Description,
                              e.Cost,
                              e.MaxGuests,
                              e.Photo1,
                              e.Photo2,
                              e.Photo3,
                              e.Photo4,
                              e.City,
                              e.Classification,
                              e.State,
                              e.Country,
                              e.PrivacyFlag,
                              e.EventType,
                              e.PosterId,
                              e.TimeStamp,
                              c.FullNames,
                              c.ProfPic,
                              c.Email
                          }).ToList();


            if (events != null)
            {
                events = events.OrderByDescending(e => e.Date).ToList();

                var latestEvents = new List<EventItems>();

                foreach (var e in events)
                {
                    LocationItems loc = CommonMethods.convertDbGeoToLocItems(e.Location);
                    loc.placeName = e.PlaceName;

                    latestEvents.Add(new EventItems()
                    {
                        eventId = e.Id,
                        title = e.Title,
                        date = e.Date,
                        endDate = e.EndDate,
                        startTime = e.StartTime,
                        endTime = e.EndTime,
                        location = loc,
                        description = e.Description,
                        cost = (float)e.Cost,
                        maxGuests = e.MaxGuests,
                        photo1 = e.Photo1,
                        photo2 = e.Photo2,
                        photo3 = e.Photo3,
                        photo4 = e.Photo4,
                        eventType = e.EventType,
                        City = e.City,
                        Classification = e.Classification,
                        Country = e.Country,
                        PrivacyFlag = e.PrivacyFlag,
                        posterId = e.PosterId,
                        timeStamp = (DateTime)e.TimeStamp,
                        posterName = e.FullNames,
                        posterPic = e.ProfPic,
                        State = e.State
                    });
                }

                var myEvents = latestEvents.Where(e => e.posterId == clientId);
                foreach (var e in myEvents)
                    latestEvents.Remove(e);

                return latestEvents;
            }
            else
                return null;
        }

        public int getNumberOfPostsIPost(Guid clientId)
        {
            var number = db.Events.Where(e => e.PosterId == clientId).Count() ;
            return number;
        }

        public List<EventItems> searchForEvent(FilterItems evt)
        {
            var clLoc = CommonMethods.convertLocItemsToDBGeo(evt.location);

            var distance = (evt.distance > 0) ? (evt.distance * 1000) : 160934.4;        

            var events = (from e in db.Events
                          join c in db.Clients on e.PosterId equals c.Id
                          where e.Location.Distance(clLoc) <= distance && e.EventAccess != "cancelled" && e.PrivacyFlag == false
                          select new
                          {
                              e.Id,
                              e.Title,
                              e.Date,
                              e.EndDate,
                              e.StartTime,
                              e.EndTime,
                              e.Location,
                              e.PlaceName,
                              e.Description,
                              e.Cost,
                              e.Photo1,
                              e.Photo2,
                              e.Photo3,
                              e.Photo4,
                              e.Photo5,
                              e.City,
                              e.Classification,
                              e.Country,
                              e.PrivacyFlag,
                              e.EventType,
                              e.PosterId,
                              e.EventAccess,
                              e.MaxGuests,
                              e.State,
                              e.TimeStamp,
                              c.FullNames,
                              c.ProfPic,
                              c.Email,
                              e.active,
                              e.concert,
                              e.travel,
                              e.nightLife,
                              e.sporting,
                              e.food
                          }).ToList();

           

            var filteredEvents = new List<EventItems>();

            foreach(var e in events)
            {
                //if((e.active && evt.active) |
                //     (e.concert && evt.concert) | 
                //     (e.travel && evt.travel) |
                //     (e.sporting && evt.sporting) |
                //     (e.nightLife && evt.nightLife) |
                //     (e.City.ToLower().Contains(evt.City.ToLower())) |
                //     (e.Country.ToLower().Contains(evt.Country.ToLower())) |
                //     (e.State.ToLower().Contains(evt.State.ToLower()))
                //    )
                if ((e.active && evt.active) |
                     (e.concert && evt.concert) |
                     (e.travel && evt.travel) |
                     (e.sporting && evt.sporting) |
                     (e.nightLife && evt.nightLife) |
                     (e.food && evt.food)
                    )
                {
                    LocationItems loc = CommonMethods.convertDbGeoToLocItems(e.Location);
                    loc.placeName = e.PlaceName;

                    filteredEvents.Add(new EventItems()
                    {
                        eventId = e.Id,
                        title = e.Title,
                        date = e.Date,
                        endDate = e.EndDate,
                        startTime = e.StartTime,
                        endTime = e.EndTime,
                        location = loc,
                        description = e.Description,
                        cost = (float)e.Cost,
                        maxGuests = e.MaxGuests,
                        photo1 = e.Photo1,
                        photo2 = e.Photo2,
                        photo3 = e.Photo3,
                        photo4 = e.Photo4,
                        photo5 = e.Photo5,
                        eventType = e.EventType,
                        City = e.City,
                        Classification = e.Classification,
                        Country = e.Country,
                        PrivacyFlag = e.PrivacyFlag,
                        posterId = e.PosterId,
                        timeStamp = (DateTime)e.TimeStamp,
                        posterName = e.FullNames,
                        posterPic = e.ProfPic,
                        State = e.State
                    });
                }

            }

            //if(filteredEvents.Count == 0)
            //{
            //    filteredEvents = getLatestEvents(evt.location);
            //}

            Debug.Write("Filter Number: ");
            Debug.WriteLine(filteredEvents.Count);

            return filteredEvents;
        }

        public async Task<EventItems> updateEvent(EventItems evt)
        {
            var evnte = db.Events.Where(e => e.Id == evt.eventId).FirstOrDefault();

            if (evnte != null)
            {
                evnte.Title = evt.title;
                evnte.Date = evt.date;
                evnte.EndDate = evt.endDate;
                evnte.StartTime = evt.startTime;
                evnte.EndTime = evt.endTime;
                evnte.Location = CommonMethods.convertLocItemsToDBGeo(evt.location);
                evnte.PlaceName = evt.location.placeName;
                evnte.Description = evt.description;
                evnte.Cost = evt.cost;
                evnte.MaxGuests = evt.maxGuests;
                evnte.Photo1 = evt.photo1;
                evnte.Photo2 = evt.photo2;
                evnte.Photo3 = evt.photo3;
                evnte.Photo4 = evt.photo4;
                evnte.Photo5 = evt.photo5;
                evnte.EventType = evt.eventType;
                evnte.City = evt.City;
                evnte.Classification = evt.Classification;
                evnte.State = evt.State;
                evnte.PrivacyFlag = evt.PrivacyFlag;
                evnte.Country = evt.Country;

                db.Entry(evnte).State = System.Data.Entity.EntityState.Modified;

                try
                {
                    await db.SaveChangesAsync();

                    return evt;
                }
                catch (Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);

                    return null;
                }
            }
            else
                return null;
        }

        public List<EventItems> searchForEventsDateDist(FilterItems evt)
        {
            var clLoc = CommonMethods.convertLocItemsToDBGeo(evt.location);

            //var distance = (evt.distance > 0) ? evt.distance : 160934.4;
            var distance = (evt.distance > 0) ? (evt.distance * 1000) : 160934.4;

            var events = (from e in db.Events
                          join c in db.Clients on e.PosterId equals c.Id
                          where (e.Location.Distance(clLoc) <= distance)
                          && ((e.Date) >= (evt.eStartDate))
                          && ((e.EndDate) <= (evt.eEndDate)) && e.EventAccess != "cancelled" && e.PrivacyFlag == false

                          select new
                          {
                              e.Id,
                              e.Title,
                              e.Date,
                              e.EndDate,
                              e.StartTime,
                              e.EndTime,
                              e.Location,
                              e.PlaceName,
                              e.Description,
                              e.Cost,
                              e.Photo1,
                              e.Photo2,
                              e.Photo3,
                              e.Photo4,
                              e.Photo5,
                              e.City,
                              e.Classification,
                              e.Country,
                              e.PrivacyFlag,
                              e.EventType,
                              e.PosterId,
                              e.EventAccess,
                              e.MaxGuests,
                              e.State,
                              e.TimeStamp,
                              c.FullNames,
                              c.ProfPic,
                              c.Email,
                              e.active,
                              e.concert,
                              e.travel,
                              e.nightLife,
                              e.sporting,
                              e.food
                          }).ToList();


            var filteredEvents = new List<EventItems>();

            foreach (var e in events)
            {
                //int startDiff = (int)DbFunctions.DiffDays(e.Date, evt.eStartDate);
                //int endDiff = (int)DbFunctions.DiffDays(e.EndDate, evt.eEndDate);

                //var startDiff = (e.Date - evt.eStartDate).TotalDays;
                //var endDiff = (e.EndDate - evt.eEndDate).TotalDays;

                //if (startDiff > 30 && endDiff < 200)
                {
                    if ((e.active && evt.active) |
                         (e.concert && evt.concert) |
                         (e.travel && evt.travel) |
                         (e.sporting && evt.sporting) |
                         (e.nightLife && evt.nightLife) |
                         (e.food && evt.food)
                        )
                    {
                        LocationItems loc = CommonMethods.convertDbGeoToLocItems(e.Location);
                        loc.placeName = e.PlaceName;

                        filteredEvents.Add(new EventItems()
                        {
                            eventId = e.Id,
                            title = e.Title,
                            date = e.Date,
                            endDate = e.EndDate,
                            startTime = e.StartTime,
                            endTime = e.EndTime,
                            location = loc,
                            description = e.Description,
                            cost = (float)e.Cost,
                            maxGuests = e.MaxGuests,
                            photo1 = e.Photo1,
                            photo2 = e.Photo2,
                            photo3 = e.Photo3,
                            photo4 = e.Photo4,
                            photo5 = e.Photo5,
                            eventType = e.EventType,
                            City = e.City,
                            Classification = e.Classification,
                            Country = e.Country,
                            PrivacyFlag = e.PrivacyFlag,
                            posterId = e.PosterId,
                            timeStamp = (DateTime)e.TimeStamp,
                            posterName = e.FullNames,
                            posterPic = e.ProfPic,
                            State = e.State
                        });
                    }
                }

            }

            //if (filteredEvents.Count == 0)
            //{
            //    filteredEvents = getLatestEvents(evt.location);
            //}

            Debug.Write("Filter Number: ");
            Debug.WriteLine(filteredEvents.Count);

            return filteredEvents;
        }

        public List<EventItems> searchForEventDate(FilterItems evt)
        {
            var clLoc = CommonMethods.convertLocItemsToDBGeo(evt.location);

            //var distance = (evt.distance > 0) ? evt.distance : 160934.4;
            var distance = (evt.distance > 0) ? (evt.distance * 1000) : 160934.4;

            //foreach (var e in db.Events)
            //{
            //    if (e.Location.Distance(clLoc) < distance)
            //    {
            //        Debug.Write("Distance 1: ");
            //        Debug.WriteLine(distance);

            //        Debug.Write("Distance 2: ");
            //        var dx = e.Location.Distance(clLoc);
            //        Debug.WriteLine(dx);
            //    }
            //}

            Debug.Write("Distance is ");
            Debug.WriteLine(distance.ToString());

            var events = (from e in db.Events
                          join c in db.Clients on e.PosterId equals c.Id
                          where (e.Location.Distance(clLoc) <= distance)
                          && ((e.Date) >= (evt.eStartDate))
                          && ((e.EndDate) <= (evt.eEndDate)) && e.EventAccess != "cancelled" && e.PrivacyFlag == false

                          select new
                          {
                              e.Id,
                              e.Title,
                              e.Date,
                              e.EndDate,
                              e.StartTime,
                              e.EndTime,
                              e.Location,
                              e.PlaceName,
                              e.Description,
                              e.Cost,
                              e.Photo1,
                              e.Photo2,
                              e.Photo3,
                              e.Photo4,
                              e.Photo5,
                              e.City,
                              e.Classification,
                              e.Country,
                              e.PrivacyFlag,
                              e.EventType,
                              e.PosterId,
                              e.EventAccess,
                              e.MaxGuests,
                              e.State,
                              e.TimeStamp,
                              c.FullNames,
                              c.ProfPic,
                              c.Email,
                              e.active,
                              e.concert,
                              e.travel,
                              e.nightLife,
                              e.sporting,
                              e.food
                          }).ToList();            


            var filteredEvents = new List<EventItems>();

            foreach (var e in events)
            {
                //int startDiff = (int)DbFunctions.DiffDays(e.Date, evt.eStartDate);
                //int endDiff = (int)DbFunctions.DiffDays(e.EndDate, evt.eEndDate);

                //var startDiff = (e.Date - evt.eStartDate).TotalDays;
                //var endDiff = (e.EndDate - evt.eEndDate).TotalDays;

                //if (startDiff > 30 && endDiff < 200)
                {
                    if ((e.active && evt.active) |
                         (e.concert && evt.concert) |
                         (e.travel && evt.travel) |
                         (e.sporting && evt.sporting) |
                         (e.nightLife && evt.nightLife) |
                         (e.food && evt.food)
                        )
                    {
                        LocationItems loc = CommonMethods.convertDbGeoToLocItems(e.Location);
                        loc.placeName = e.PlaceName;

                        filteredEvents.Add(new EventItems()
                        {
                            eventId = e.Id,
                            title = e.Title,
                            date = e.Date,
                            endDate = e.EndDate,
                            startTime = e.StartTime,
                            endTime = e.EndTime,
                            location = loc,
                            description = e.Description,
                            cost = (float)e.Cost,
                            maxGuests = e.MaxGuests,
                            photo1 = e.Photo1,
                            photo2 = e.Photo2,
                            photo3 = e.Photo3,
                            photo4 = e.Photo4,
                            photo5 = e.Photo5,
                            eventType = e.EventType,
                            City = e.City,
                            Classification = e.Classification,
                            Country = e.Country,
                            PrivacyFlag = e.PrivacyFlag,
                            posterId = e.PosterId,
                            timeStamp = (DateTime)e.TimeStamp,
                            posterName = e.FullNames,
                            posterPic = e.ProfPic,
                            State = e.State
                        });
                    }
                }

            }

            //if (filteredEvents.Count == 0)
            //{
            //    filteredEvents = getLatestEvents(evt.location);
            //}

            Debug.Write("Filter Number: ");
            Debug.WriteLine(filteredEvents.Count);

            return filteredEvents;
        }


        
        public async Task<EventGenItem> addTestEvents(EventGenItem evt)
        {
            evt.timeStamp = DateTime.Now;
            //evt.eventId = Guid.NewGuid();

            if (!evt.eventType.Contains("["))
                evt.eventType = "[" + evt.eventType;

            if (!evt.eventType.Contains("]"))
                evt.eventType = evt.eventType + "]";

            var evnte = new Event()
            {
                Id = Guid.NewGuid(),
                Title = evt.title,
                Date = evt.date,
                EndDate = evt.endDate,
                StartTime = evt.startTime,
                EndTime = evt.endTime,
                Location = CommonMethods.convertLocItemsToDBGeo(evt.location),
                PlaceName = evt.location.placeName,
                Description = evt.description,
                Cost = evt.cost,
                MaxGuests = evt.maxGuests,
                Photo1 = "https://bprstorage.blob.core.windows.net/bprfiles/oh-yeah-jodeci-ooooooh-yeah.jpeg",
                Photo2 = "https://bprstorage.blob.core.windows.net/bprfiles/oh-yeah-jodeci-ooooooh-yeah.jpeg",
                Photo3 = "https://bprstorage.blob.core.windows.net/bprfiles/oh-yeah-jodeci-ooooooh-yeah.jpeg",
                Photo4 = "",
                Photo5 = "",
                City = evt.City,
                Classification = evt.eventType,
                Country = evt.Country,
                PrivacyFlag = evt.PrivacyFlag,
                EventAccess = "OnGoing",
                EventType = evt.eventType,
                PosterId = Guid.Parse("B315FA6B-FE26-499A-8B7F-BF923DCECF14"),
                State = evt.State,
                TimeStamp = DateTime.Now
            };

            db.Events.Add(evnte);

            try
            {

                await db.SaveChangesAsync();

                return evt;

            }
            catch (Exception r)
            {
                Debug.WriteLine(r.Message);
                Debug.WriteLine(r.InnerException.Message);

                return null;
            }
        }

        public async Task updateTypeEvent()
        {
            var events = db.Events.Where(x => x.EventType.Contains("Night Life"));

            foreach (var e in events)
                e.EventType.Replace("\"Night Life\"", "\"NightLife\"");

            foreach(var e in events)
                db.Entry(e).State = System.Data.Entity.EntityState.Modified;
            

           await db.SaveChangesAsync();
        }

        async public Task<bool> editEventImageLinks(Guid eventId, List<string> imageLinks)
        {
            var ev = db.Events.Where(e => e.Id == eventId).FirstOrDefault();

            if (ev != null)
            {

                if (imageLinks.Count > 0 && imageLinks.Count <= 5)
                {
                    if(imageLinks.Count == 1)
                    {
                        ev.Photo1 = imageLinks[0];
                    }else if(imageLinks.Count == 2)
                    {
                        ev.Photo1 = imageLinks[0];
                        ev.Photo2 = imageLinks[1];
                    }
                    else if (imageLinks.Count == 3)
                    {
                        ev.Photo1 = imageLinks[0];
                        ev.Photo2 = imageLinks[1];
                        ev.Photo3 = imageLinks[2];
                    }
                    else if (imageLinks.Count == 4)
                    {
                        ev.Photo1 = imageLinks[0];
                        ev.Photo2 = imageLinks[1];
                        ev.Photo3 = imageLinks[2];
                        ev.Photo4 = imageLinks[3];
                    }
                    else if (imageLinks.Count == 5)
                    {
                        ev.Photo1 = imageLinks[0];
                        ev.Photo2 = imageLinks[1];
                        ev.Photo3 = imageLinks[2];
                        ev.Photo4 = imageLinks[3];
                        ev.Photo5 = imageLinks[4];
                    }

                    try
                    {
                        db.Entry(ev).State = EntityState.Modified;

                        await db.SaveChangesAsync();

                        return true;
                    }catch(Exception r)
                    {
                        return false;
                    }
                }
                else
                    return false;
            }
            else
                return false;
        }
    }
}