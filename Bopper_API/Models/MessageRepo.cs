﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bopper.ViewModels;
using System.Threading.Tasks;
using Bopper.Data;

namespace Bopper.Models
{
    #region Interfaces
    interface ICreateMessage
    { Task<ChatItems> createMessage(ChatItems msg); }

    interface IReplyMessage
    { Task<ChatItems> replyMessage(ChatItems msg); }
    interface IEditMessage
    { Task<ChatItems> editMessage(ChatItems msg); }
    interface IDeleteMessage
    { Task<bool> deleteMessage(Guid messageId); }

    interface IGetTrotMessages
    { List<ChatReturnItems> getTrotMessages(Guid trotId); }

    interface IGetMyTrotMessages
    { List<ChatReturnItems> getMyTrotMessages(Guid trotId, Guid senderId); }

    interface IGetNumberofChatsMessages
    { int getNumberOfMessages(Guid uniqueId); }

    #endregion


    public class ChatRepo : ICreateMessage, IReplyMessage,
        IEditMessage,
        IDeleteMessage, IGetTrotMessages, IGetMyTrotMessages,
        IGetNumberofChatsMessages
    {

        BopperContext db = new BopperContext();

        public ChatRepo()
        {
            db.Configuration.LazyLoadingEnabled = false;
        }

        public async Task<ChatItems> createMessage(ChatItems msg)
        {
            msg.messageId = Guid.NewGuid();
            msg.dateSent = DateTime.Now.Date;
            msg.timeStamp = DateTime.Now;

            Chat message = new Chat()
            {
                Id = msg.messageId,
                UniqueMessageId = msg.uniqueMessageId,
                SenderId = msg.senderId,
                Message = msg.message,
                ImageLink = msg.imageLink,
                Status = "MESSAGE",
                DateSent = msg.dateSent,
                TimeStamp = msg.timeStamp
            };

            try
            {
                db.Chats.Add(message);
                await db.SaveChangesAsync();

                //Notify the merchant here

                return msg;
            }
            catch (Exception r)
            {
                Console.WriteLine(r.Message);
                return null;
            }
        }

        public async Task<bool> deleteMessage(Guid messageId)
        {
            var message = db.Chats.Where(x => x.Id == messageId).FirstOrDefault();
            if (message != null)
            {
                try
                {
                    db.Chats.Remove(message);
                    await db.SaveChangesAsync();
                    return true;
                }
                catch (Exception r)
                {
                    Console.WriteLine(r.Message);
                    return false;
                }
            }
            else
                return false;
        }

        public async Task<ChatItems> editMessage(ChatItems msg)
        {
            var message = db.Chats.Where(m => m.Id == msg.messageId).FirstOrDefault();
            if (message != null)
            {
                message.Message = msg.message;
                message.ImageLink = msg.imageLink;

                try
                {
                    db.Entry(message).State = System.Data.Entity.EntityState.Modified;
                    await db.SaveChangesAsync();
                    return msg;
                }
                catch (Exception r)
                {
                    Console.WriteLine(r.Message);
                    return null;
                }
            }
            else
                return null;
        }

        public List<ChatReturnItems> getMyTrotMessages(Guid trotId, Guid senderId)
        {
            List<ChatReturnItems> returnMessages = new List<ChatReturnItems>();
            var messages = (from m in db.Chats
                            join u in db.Clients on m.SenderId equals u.Id
                            where m.UniqueMessageId == trotId && m.SenderId == senderId
                            select new
                            {
                                m.Id,
                                m.UniqueMessageId,
                                m.SenderId,
                                m.Message,
                                m.ImageLink,
                                m.Status,
                                m.DateSent,
                                m.TimeStamp,
                                u.FullNames,
                                u.Email,
                                u.PhoneNumber,
                                u.ProfPic
                            }).OrderBy(m => m.TimeStamp);


            foreach (var mes in messages)
            {
                ChatReturnItems msg = new ChatReturnItems()
                {
                    messageId = mes.Id,
                    uniqueMessageId = mes.UniqueMessageId,
                    senderId = mes.SenderId,
                    message = mes.Message,
                    imageLink = mes.ImageLink,
                    status = mes.Status,
                    dateSent = mes.DateSent,
                    timeStamp = mes.TimeStamp,
                    name = mes.FullNames,
                    email = mes.Email,
                    phoneNumber = mes.PhoneNumber,
                    profPic = mes.ProfPic
                };

                returnMessages.Add(msg);
            }

            return returnMessages;
        }

        public int getNumberOfMessages(Guid trotId)
        {
            var messages = db.Chats.Where(m => m.UniqueMessageId == trotId).Count();
            return messages;
        }

        public List<ChatReturnItems> getTrotMessages(Guid trotId)
        {
            List<ChatReturnItems> returnMessages = new List<ChatReturnItems>();
            var messages = (from m in db.Chats
                            join u in db.Clients on m.SenderId equals u.Id
                            where m.UniqueMessageId == trotId
                            select new
                            {
                                m.Id,
                                m.UniqueMessageId,
                                m.SenderId,
                                m.Message,
                                m.ImageLink,
                                m.Status,
                                m.DateSent,
                                m.TimeStamp,
                                u.FullNames,
                                u.Email,
                                u.PhoneNumber,
                                u.ProfPic
                            }).OrderBy(m => m.TimeStamp);


            foreach (var mes in messages)
            {
                ChatReturnItems msg = new ChatReturnItems()
                {
                    messageId = mes.Id,
                    uniqueMessageId = mes.UniqueMessageId,
                    senderId = mes.SenderId,
                    message = mes.Message,
                    imageLink = mes.ImageLink,
                    status = mes.Status,
                    dateSent = mes.DateSent,
                    timeStamp = mes.TimeStamp,
                    name = mes.FullNames,
                    email = mes.Email,
                    phoneNumber = mes.PhoneNumber,
                    profPic = mes.ProfPic
                };

                returnMessages.Add(msg);
            }

            return returnMessages;
        }

        public async Task<ChatItems> replyMessage(ChatItems msg)
        {
            msg.messageId = Guid.NewGuid();
            msg.dateSent = DateTime.Now.Date;
            msg.timeStamp = DateTime.Now;

            Chat message = new Chat()
            {
                Id = msg.messageId,
                UniqueMessageId = msg.uniqueMessageId,
                SenderId = msg.senderId,
                Message = msg.message,
                ImageLink = msg.imageLink,
                Status = "REPLY",
                DateSent = msg.dateSent,
                TimeStamp = msg.timeStamp
            };

            try
            {
                db.Chats.Add(message);
                await db.SaveChangesAsync();

                //Notify the merchant here

                return msg;
            }
            catch (Exception r)
            {
                Console.WriteLine(r.Message);
                return null;
            }
        }

    }
}