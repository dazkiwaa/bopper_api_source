﻿using Bopper.Data;
using Bopper.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bopper.Models
{
    #region Interfaces
    interface ICreateDM
    { Task<DirectMessageItems> createDm(DirectMessageItems dm); }
    interface IDeleteDm
    { Task<bool> deleteDm(Guid dmId); }
    interface IGetMyDms
    { List<DirectMessageReturnItems> getMyDms(Guid senderId); }
    interface IGetMyDmId
    { DirectMessageReturnItems getMyDmId(Guid senderId, Guid receiverId); }

    interface IGetNumberOfMessages { int getNumberOfMessages(Guid clientId); }
    #endregion
    public class DirectMessageRepo : ICreateDM, IDeleteDm, IGetMyDms, IGetMyDmId, IGetNumberOfMessages
    {
        BopperContext db = new BopperContext();

        public DirectMessageRepo() { this.db.Configuration.LazyLoadingEnabled = false; }


        async public Task<DirectMessageItems> createDm(DirectMessageItems dm)
        {
            dm.directMessageId = Guid.NewGuid();
            dm.timeStamp = CommonMethods.getKenyaTime();

            var dmm = new DirectMessages()
            {
                Id = dm.directMessageId,
                RecipientId = dm.receiptId,
                SenderId = dm.senderId,
                TimeStamp = dm.timeStamp
            };

            try
            {
                db.DirectMessages.Add(dmm);

                var sender = db.Clients.Where(c => c.Id == dm.senderId).FirstOrDefault();
                var receiver = db.Clients.Where(c => c.Id == dm.receiptId).FirstOrDefault();

                //await new NotificationRepo().createNotificationHub(new NotificationItems()
                //{
                //    clientId = receiver.Id,
                //    notificationId = Guid.NewGuid(),
                //    notificationMessage = sender.FullNames + " has added you to their DM",
                //    notificationType = NotificationType.DM.ToString(),
                //    otherId = dm.directMessageId,
                //    timeStamp = dm.timeStamp
                //});

                await db.SaveChangesAsync();

                return dm;
            }
            catch (Exception r)
            {
                Debug.WriteLine(r.Message);
                Debug.WriteLine(r.InnerException.Message);

                return null;
            }
        }

        async public Task<bool> deleteDm(Guid dmId)
        {
            var dm = db.DirectMessages.Where(x => x.Id == dmId).FirstOrDefault();

            if (dm != null)
            {
                var dms = db.DM_Messages.Where(x => x.DirectMessageId == dm.Id);

                foreach (var d in dms)
                    db.DM_Messages.Remove(d);

                db.DirectMessages.Remove(dm);

                try
                {

                    await db.SaveChangesAsync();

                    return true;
                }
                catch (Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);

                    return false;
                }
            }
            else
                return false;
        }

        public DirectMessageReturnItems getMyDmId(Guid senderId, Guid receiverId)
        {
            var dms = (from dm in db.DirectMessages
                       join c in db.Clients on dm.RecipientId equals c.Id
                       where dm.RecipientId == receiverId && dm.SenderId == senderId
                       select new
                       {
                           dm.Id,
                           dm.SenderId,
                           dm.RecipientId,
                           dm.TimeStamp,
                           c.FullNames,
                           c.ProfPic,
                       }).FirstOrDefault();

            var dmy = (from dm in db.DirectMessages
                       join c in db.Clients on dm.RecipientId equals c.Id
                       where dm.RecipientId == senderId && dm.SenderId == receiverId
                       select new
                       {
                           dm.Id,
                           dm.SenderId,
                           dm.RecipientId,
                           dm.TimeStamp,
                           c.FullNames,
                           c.ProfPic,
                       }).FirstOrDefault();

            if (dms != null)
            {
                var dmReturn = new DirectMessageReturnItems();

                var dmMessages = db.DM_Messages.Where(x => x.DirectMessageId == dms.Id).OrderByDescending(c => c.TimeStamp).FirstOrDefault();

                if (dmMessages != null)
                {
                    dmReturn.lastMessage = dmMessages.Message;
                    dmReturn.lastMessageTime = dmMessages.TimeStamp;
                }
                else
                {
                    dmReturn.lastMessage = "None";
                    dmReturn.lastMessageTime = CommonMethods.getKenyaTime();
                }

                dmReturn.directMessageId = dms.Id;
                dmReturn.senderId = dms.SenderId;
                dmReturn.receiptId = dms.RecipientId;
                dmReturn.timeStamp = dms.TimeStamp;
                dmReturn.clientName = dms.FullNames;
                dmReturn.clientPic = dms.ProfPic;

                return dmReturn;
            } else if(dmy != null)
            {
                var dmReturn = new DirectMessageReturnItems();

                var dmMessages = db.DM_Messages.Where(x => x.DirectMessageId == dmy.Id).OrderByDescending(c => c.TimeStamp).FirstOrDefault();

                if (dmMessages != null)
                {
                    dmReturn.lastMessage = dmMessages.Message;
                    dmReturn.lastMessageTime = dmMessages.TimeStamp;
                }
                else
                {
                    dmReturn.lastMessage = "None";
                    dmReturn.lastMessageTime = CommonMethods.getKenyaTime();
                }

                dmReturn.directMessageId = dmy.Id;
                dmReturn.senderId = dmy.SenderId;
                dmReturn.receiptId = dmy.RecipientId;
                dmReturn.timeStamp = dmy.TimeStamp;
                dmReturn.clientName = dmy.FullNames;
                dmReturn.clientPic = dmy.ProfPic;

                return dmReturn;
            }
            else
                return null;
        }

        public DirectMessageReturnItems getMyDmId(Guid dmsId)
        {
            var dms = (from dm in db.DirectMessages
                       join c in db.Clients on dm.RecipientId equals c.Id
                       where dm.Id == dmsId
                       select new
                       {
                           dm.Id,
                           dm.SenderId,
                           dm.RecipientId,
                           dm.TimeStamp,
                           c.FullNames,
                           c.ProfPic,
                       }).FirstOrDefault();

            if (dms != null)
            {
                var dmReturn = new DirectMessageReturnItems();

                var dmMessages = db.DM_Messages.Where(x => x.DirectMessageId == dms.Id).OrderByDescending(c => c.TimeStamp).FirstOrDefault();

                if (dmMessages != null)
                {
                    dmReturn.lastMessage = dmMessages.Message;
                    dmReturn.lastMessageTime = dmMessages.TimeStamp;
                }
                else
                {
                    dmReturn.lastMessage = "None";
                    dmReturn.lastMessageTime = CommonMethods.getKenyaTime();
                }

                dmReturn.directMessageId = dms.Id;
                dmReturn.senderId = dms.SenderId;
                dmReturn.receiptId = dms.RecipientId;
                dmReturn.timeStamp = dms.TimeStamp;
                dmReturn.clientName = dms.FullNames;
                dmReturn.clientPic = dms.ProfPic;

                return dmReturn;
            }
            else
                return null;
        }

        public List<DirectMessageReturnItems> getMyDms(Guid senderId)
        {
            var myDms = new List<DirectMessageReturnItems>();

            var dms = (from dm in db.DirectMessages
                       join c in db.Clients on dm.RecipientId equals c.Id
                       where dm.SenderId == senderId
                       select new
                       {
                           dm.Id,
                           dm.SenderId,
                           dm.RecipientId,
                           dm.TimeStamp,
                           c.FullNames,
                           c.ProfPic,
                       });

            var dms2 = (from dm in db.DirectMessages
                        join c in db.Clients on dm.SenderId equals c.Id
                        where dm.RecipientId == senderId
                        select new
                        {
                            dm.Id,
                            dm.SenderId,
                            dm.RecipientId,
                            dm.TimeStamp,
                            c.FullNames,
                            c.ProfPic,
                        });

            Debug.WriteLine(dms.Count() + " Count 1");
            Debug.WriteLine(dms2.Count() + " Count 2");

            try
            {
                foreach (var d in dms)
                {
                    var dmReturn = new DirectMessageReturnItems();

                    var dmMessages = db.DM_Messages.Where(x => x.DirectMessageId == d.Id).OrderByDescending(c => c.TimeStamp).FirstOrDefault();

                    if (dmMessages != null)
                    {
                        dmReturn.lastMessage = dmMessages.Message;
                        dmReturn.lastMessageTime = dmMessages.TimeStamp;

                        dmReturn.directMessageId = d.Id;
                        dmReturn.senderId = d.SenderId;
                        dmReturn.receiptId = d.RecipientId;
                        dmReturn.timeStamp = d.TimeStamp;
                        dmReturn.clientName = d.FullNames;
                        dmReturn.clientPic = d.ProfPic;

                        try
                        {
                            dmReturn.lastMessageTime = CommonMethods.getTimeZonedTime(dmMessages.TimeStamp, dmMessages.timeZone);
                        }
                        catch (Exception r)
                        {
                            Debug.WriteLine(r.Message);
                            Debug.WriteLine(r.InnerException.Message);
                        }


                        myDms.Add(dmReturn);
                    }
                    else
                    {
                        dmReturn.lastMessage = "None";
                        dmReturn.lastMessageTime = CommonMethods.getKenyaTime();
                    }

                  
                }

                foreach (var d in dms2)
                {
                    var dmReturn = new DirectMessageReturnItems();

                    var dmMessages = db.DM_Messages.Where(x => x.DirectMessageId == d.Id).OrderByDescending(c => c.TimeStamp).FirstOrDefault();

                    if (dmMessages != null)
                    {
                        dmReturn.lastMessage = dmMessages.Message;
                        dmReturn.lastMessageTime = dmMessages.TimeStamp;

                        dmReturn.directMessageId = d.Id;
                        dmReturn.senderId = d.SenderId;
                        dmReturn.receiptId = d.RecipientId;
                        dmReturn.timeStamp = d.TimeStamp;
                        dmReturn.clientName = d.FullNames;
                        dmReturn.clientPic = d.ProfPic;

                        try
                        {
                            dmReturn.lastMessageTime = CommonMethods.getTimeZonedTime(dmMessages.TimeStamp, dmMessages.timeZone);
                        }
                        catch (Exception r)
                        {
                            Debug.WriteLine(r.Message);
                            Debug.WriteLine(r.InnerException.Message);
                        }

                        myDms.Add(dmReturn);
                    }
                    else
                    {
                        dmReturn.lastMessage = "None";
                        dmReturn.lastMessageTime = CommonMethods.getKenyaTime();
                    }

                 
                }

                return myDms.OrderByDescending(x => x.lastMessageTime).ToList();
            } catch (Exception r)
            {
                Debug.WriteLine(r.Message);
                Debug.WriteLine(r.InnerException.Message);
                return null;
            }
        }

        public int getNumberOfMessages(Guid clientId)
        {
            var dmNumber = db.DM_Messages.Where(c => c.ClientId == clientId);
            return dmNumber != null ? dmNumber.Count() : 0;
        }
    }
}