﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bopper.Models;
using Bopper.ViewModels;
using System.Threading.Tasks;
using System.Diagnostics;
using Bopper.Data;

namespace Bopper.Models
{

    #region Interfaces
    interface ISendInvite
    { Task<InviteItems> sendInvite(InviteItems inv); }

    interface IAcceptInvite
    { Task<bool> acceptInvite(Guid inid); }

    interface IDeclineInvite
    { Task<bool> declineInvite(Guid inid); }

    interface IGetInvitesISent
    { List<InviteClientItems> getInvitesISent(Guid clientId); }

    interface IGetInvitesIReceived
    { List<InviteClientItems> getInvitesIReceived(Guid clientId); }

    interface IDeleteInvite
    { Task<bool> deleteInvite(Guid inId); }

    interface IRequestInvite
    { Task<InviteRequestItems> requestInvite(InviteRequestItems rqI); }

    interface IGetWhosComing
    { List<InviteClientItems> getWhosComing(Guid eventId); }

    interface IGetWhosInterested
    { List<InviteClientItems> getWhosInterest(Guid eventId); }

    interface ICheckIfHasRequestedInvite
    { bool checkIfHasRequestedInvite(Guid eventId, Guid clientId); }

    interface ICheckIfHasInvite
    { bool checkIfHasInvite(Guid eventId, Guid clientId); }

    interface IGetInvite
    { InviteItems getInvite(Guid eventId, Guid clientId); }

    interface ISendMultiInvites
    { Task<List<InviteItems>> sendMultipleInvites(List<InviteItems> multiInvites); }

    interface IGetInviteByID { InviteItems getInviteById(Guid inviteId); }

    interface IGetEventInvites { List<ContactItems> getEventInvites(Guid eventId); }

    public class InvitesComparer : IEqualityComparer<InviteClientItems>
    {
        public bool Equals(InviteClientItems x, InviteClientItems y)
        {
            return x.eventId == y.eventId && x.userId == y.userId;
        }

        public int GetHashCode(InviteClientItems obj)
        {
            return obj.eventId.GetHashCode();
        }
    }
    #endregion

    public class InviteRepo : ISendInvite, IAcceptInvite, IDeclineInvite, IGetInvitesISent, IGetInvitesIReceived, IDeleteInvite
        , IGetWhosComing, IGetWhosInterested, IRequestInvite, ICheckIfHasInvite, ICheckIfHasRequestedInvite, IGetInviteByID
        ,ISendMultiInvites, IGetInvite, IGetEventInvites
    {
        BopperContext db = new BopperContext();

        NotificationRepo nRepo = new NotificationRepo();

        public InviteRepo()
        { db.Configuration.LazyLoadingEnabled = false; }

        public async Task<bool> acceptInvite(Guid inid)
        {
            var invite = db.Invites.Where(i => i.Id == inid).FirstOrDefault();

            if (invite != null)
            {
                invite.InviteAc = "ACCEPTED";

                try
                {
                    await db.SaveChangesAsync();

                    //Send notification here  
                    var rRepo = new RSVPRepo();

                    //Create an RSVP item here 
                    var result = await rRepo.createRSVPfromInvite(new RSVPItems() {
                        eventId = invite.EventId,
                        userId = invite.UserId,
                        inviteId = invite.Id
                    });

                    //accept the invite
                    await rRepo.acceptRSVP(result.rsvpId);
                    
                    return true;
                }
                catch (Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);

                    return false;
                }
            }
            else
                return false;
        }

        public bool checkIfHasInvite(Guid eventId, Guid clientId)
        {
            var invite = db.Invites.Where(i => i.EventId == eventId && i.UserId == clientId).FirstOrDefault();

            return invite == null ? true : false;
        }

        public bool checkIfHasRequestedInvite(Guid eventId, Guid clientId)
        {
            var inviteRequest = db.InviteRequests.Where(i => i.EventId == eventId && i.UserId == clientId).FirstOrDefault();

            return inviteRequest == null ? true : false;
        }

        public async Task<bool> declineInvite(Guid inid)
        {
            var invite = db.Invites.Where(i => i.Id == inid).FirstOrDefault();

            if (invite != null)
            {
                invite.InviteAc = "REJECTED";

                try
                {
                    await db.SaveChangesAsync();

                    //Send notification here

                    return true;
                }
                catch (Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);

                    return false;
                }
            }
            else
                return false;
        }

        public async Task<bool> deleteInvite(Guid inId)
        {
            var invite = db.Invites.Where(i => i.Id == inId).FirstOrDefault();

            if (invite != null)
            {

                db.Invites.Remove(invite);

                try
                {
                    await db.SaveChangesAsync();

                    return true;
                }
                catch (Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);

                    return false;
                }
            }
            else
                return false;
        }

        public List<ContactItems> getEventInvites(Guid eventId)
        {
            List<ContactItems> invitees = new List<ContactItems>();
            var invites = (from i in db.Invites
                           join u in db.Clients on i.UserId equals u.Id
                           where i.EventId == eventId
                           select new {
                               i.Id,
                               i.EventId,
                               i.InviteAc,
                               i.Status,
                               i.TimeStamp,
                               i.UserId,
                               u.ProfPic,
                               u.UserName,
                               u.Email,
                               u.FullNames
                           });

            foreach (var x in invites)
            {
                invitees.Add(new ContactItems() {
                    cId = x.Id,
                    contactId = x.EventId,
                    email = x.Email,
                    profPic = x.ProfPic,
                    fullNames = x.FullNames,
                    timeStamp = x.TimeStamp,
                    userId = x.UserId
                });
            }


            return invitees.OrderBy(c => c.fullNames).ToList();
        }

        public InviteItems getInvite(Guid eventId, Guid clientId)
        {
            var invites = db.Invites.Where(i => i.EventId == eventId && i.UserId == clientId).FirstOrDefault();

            if (invites != null)
            {
                var invRe = new InviteItems()
                {
                    inviteId = invites.Id,
                    eventId = invites.EventId,
                    inviteAc = invites.InviteAc,
                    status = invites.Status,
                    timeStamp = (DateTime) invites.TimeStamp,
                    userId = invites.UserId
                };

                return invRe;
            }
            else
                return null;
        }

        public InviteItems getInviteById(Guid inviteId)
        {
            var invite = db.Invites.Where(i => i.Id == inviteId).FirstOrDefault();

            return (invite != null) ? new InviteItems()
            {
                eventId = invite.EventId,
                inviteId = invite.Id,
                inviteAc = invite.InviteAc,
                status = invite.Status,
                timeStamp = invite.TimeStamp,
                userId = invite.UserId
            } : null;
        }

        public List<InviteClientItems> getInvitesIReceived(Guid clientId)
        {
            var invitesIReceived = new List<InviteClientItems>();

            var invites = (from i in db.Invites
                           join c in db.Clients on i.UserId equals c.Id
                           join e in db.Events on i.EventId equals e.Id
                           where i.UserId == clientId
                           select new {
                               i.Id,
                               i.InviteAc,
                               i.EventId,
                               i.UserId,
                               i.Status,
                               i.TimeStamp,
                               c.UserName,
                               c.FullNames,
                               c.ProfPic,
                               c.Email,
                               e.PlaceName,
                               e.Title
                           }).ToList();

            invites = invites.OrderByDescending(i => i.TimeStamp).ToList();

            foreach(var i in invites)
            {
                invitesIReceived.Add(new InviteClientItems()
                {
                    email = i.Email,
                    eventId = i.EventId,
                    fullNames = i.FullNames,
                    inviteAc = i.InviteAc,
                    inviteId = i.Id,
                    profPic = i.ProfPic,
                    status = i.Status,
                    timeStamp = (DateTime)i.TimeStamp,
                    userId = i.UserId,
                    userName = i.UserName,
                    placeName = i.PlaceName,
                    title = i.Title
                });
            }

            return invitesIReceived;
        }

        public List<InviteClientItems> getInvitesISent(Guid clientId)
        {
            var invitesIReceived = new List<InviteClientItems>();

            var invites = (from i in db.Invites
                           join c in db.Clients on i.UserId equals c.Id
                           join e in db.Events on i.EventId equals e.Id
                           where e.PosterId == clientId
                           select new
                           {
                               i.Id,
                               i.InviteAc,
                               i.EventId,
                               i.UserId,
                               i.Status,
                               i.TimeStamp,
                               c.UserName,
                               c.FullNames,
                               c.ProfPic,
                               c.Email,
                               e.PlaceName,
                               e.Title
                           }).ToList();

            invites = invites.OrderByDescending(i => i.TimeStamp).ToList();

            foreach (var i in invites)
            {
                invitesIReceived.Add(new InviteClientItems()
                {
                    email = i.Email,
                    eventId = i.EventId,
                    fullNames = i.FullNames,
                    inviteAc = i.InviteAc,
                    inviteId = i.Id,
                    profPic = i.ProfPic,
                    status = i.Status,
                    timeStamp = (DateTime)i.TimeStamp,
                    userId = i.UserId,
                    userName = i.UserName,
                    placeName = i.PlaceName,
                    title = i.Title
                });
            }

            return invitesIReceived;
        }

        public List<InviteClientItems> getWhosComing(Guid eventId)
        {
            var invitesIReceived = new List<InviteClientItems>();

            var invites = (from i in db.Invites
                           join c in db.Clients on i.UserId equals c.Id
                           join e in db.Events on i.EventId equals e.Id
                           where e.Id == eventId //&& i.InviteAc.ToLower() == "ACCEPTED"
                           select new
                           {
                               i.Id,
                               i.InviteAc,
                               i.EventId,
                               i.UserId,
                               i.Status,
                               i.TimeStamp,
                               c.UserName,
                               c.FullNames,
                               c.ProfPic,
                               c.Email,
                               e.PlaceName,
                               e.Title
                           }).ToList();

            invites = invites.OrderByDescending(i => i.TimeStamp).ToList();

            foreach (var i in invites)
            {
                invitesIReceived.Add(new InviteClientItems()
                {
                    email = i.Email,
                    eventId = i.EventId,
                    fullNames = i.FullNames,
                    inviteAc = i.InviteAc,
                    inviteId = i.Id,
                    profPic = i.ProfPic,
                    status = i.Status,
                    timeStamp = (DateTime)i.TimeStamp,
                    userId = i.UserId,
                    userName = i.UserName,
                    placeName = i.PlaceName,
                    title = i.Title
                });
            }

            return invitesIReceived.Distinct(new InvitesComparer()).ToList();
        }

        public List<InviteClientItems> getWhosInterest(Guid eventId)
        {
            var invitesIReceived = new List<InviteClientItems>();

            var invites = (from i in db.InviteRequests
                           join c in db.Clients on i.UserId equals c.Id
                           join e in db.Events on i.EventId equals e.Id
                           where e.Id == eventId
                           select new
                           {
                               i.Id,
                               i.EventId,
                               i.UserId,
                               i.TimeStamp,
                               c.UserName,
                               c.FullNames,
                               c.ProfPic,
                               c.Email,
                               e.PlaceName,
                               e.Title
                           }).ToList();

            invites = invites.OrderByDescending(i => i.TimeStamp).ToList();

            foreach (var i in invites)
            {
                invitesIReceived.Add(new InviteClientItems()
                {
                    email = i.Email,
                    eventId = i.EventId,
                    fullNames = i.FullNames,                
                    profPic = i.ProfPic,
                    timeStamp = (DateTime)i.TimeStamp,
                    userId = i.UserId,
                    userName = i.UserName,
                    placeName = i.PlaceName,
                    title = i.Title
                });
            }

            return invitesIReceived.Distinct(new InvitesComparer()).ToList(); 
        }

        public async Task<InviteRequestItems> requestInvite(InviteRequestItems rqI)
        {
            rqI.requestId = Guid.NewGuid();

            var inviteRequest = new InviteRequest()
            {
                Id = rqI.requestId,
                EventId = rqI.eventId,
                UserId = rqI.userId,
                TimeStamp = DateTime.Now
            };

            db.InviteRequests.Add(inviteRequest);

            try
            {
                db.SaveChanges();

                ////Send notification here
                //var nR = await nRepo.sendNotificion(new NotificationItems()
                //{
                //    notificationMessage = " Has requested to be invited to your event.",
                //    notificationTitle = "Event Invitation Request",
                //    receiverId = db.Events.Where(i => i.Id == rqI.eventId).FirstOrDefault().PosterId,
                //    typeid = rqI.eventId,
                //    type = NotificationTypes.Request.ToString()
                //});

                return rqI;
            }
            catch (Exception r)
            {
                Debug.WriteLine(r.Message);
                Debug.WriteLine(r.InnerException.Message);

                return null;
            }
        }

        public async Task<InviteItems> sendInvite(InviteItems inv)
        {
           

            var invite = new Invite()
            {
                EventId = inv.eventId,
                InviteAc = "SENT",
                Status = false,
                Id = inv.inviteId,
                TimeStamp = DateTime.Now,
                UserId = inv.userId
            };

            db.Invites.Add(invite);

            try
            {
                db.SaveChanges();

                //Send notification here           
                //var nR = await new NotificationRepo().sendNotificion(new NotificationItems()
                //{
                //    notificationMessage = " Has invited you to an event.",
                //    notificationTitle = "Event Invitation",
                //    receiverId = invite.UserId,
                //    typeid = invite.EventId,
                //    type = NotificationTypes.Invite.ToString()                    
                //});

                return inv;
            }catch(Exception r)
            {
                Debug.WriteLine(r.Message);
                Debug.WriteLine(r.InnerException.Message);

                return null;
            }
        }

        public async Task<List<InviteItems>> sendMultipleInvites(List<InviteItems> multiInvites)
        {
            try
            {
                foreach (var inv in multiInvites)
                {
                    var innx = db.Invites.Where(i => i.EventId == inv.eventId && i.UserId == inv.userId).FirstOrDefault();

                    if (innx == null)
                    {
                        inv.inviteId = Guid.NewGuid();
                        inv.timeStamp = DateTime.Now;

                        var invite = new Invite()
                        {
                            EventId = inv.eventId,
                            InviteAc = "SENT",
                            Status = false,
                            Id = inv.inviteId,
                            TimeStamp = DateTime.Now,
                            UserId = inv.userId
                        };

                        db.Invites.Add(invite);

                        await db.SaveChangesAsync();

                        //Send notification here           
                        var eventPoster = (from e in db.Events
                                           join c in db.Clients on e.PosterId equals c.Id
                                           where e.Id == inv.eventId
                                           select new { c.FullNames, e.PosterId, e.Title }).FirstOrDefault();

                        var nR = new NotificationItems()
                        {
                            notificationMessage = eventPoster.FullNames + " Has invited you to " + eventPoster.Title + " event.",
                            notificationId = Guid.NewGuid(),
                            clientId = inv.userId,
                            notificationType = NotificationType.INVITE.ToString(),
                            otherId = inv.inviteId,
                            timeStamp = inv.timeStamp
                        };

                        await new NotificationRepo().createNotificationHub(nR);
                    }
                }

                return multiInvites;
            }catch(Exception r)
            {
                Debug.WriteLine(r.Message);
                Debug.WriteLine(r.InnerException.Message);

                return null;
            }
        }
    }
}