﻿using Bopper.Data;
using Bopper.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bopper.Models
{
    #region Interfaces
    interface ISendDmMessage
    { Task<DM_MessagesItems> createMessage(DM_MessagesItems msg); }

    interface IReplyDmMessage
    { Task<DM_MessagesItems> replyMessage(DM_MessagesItems msg); }
    interface IEditDmMessage
    { Task<DM_MessagesItems> editMessage(DM_MessagesItems msg); }
    interface IDeleteDmMessage
    { Task<bool> deleteMessage(Guid dmMsgId); }

    interface IGetDmMessages
    { List<DM_MessagesItems> getChatRoomMessages(Guid directMessageId); }

    #endregion

    public class DM_MessagesRepo : ISendDmMessage, IReplyDmMessage, IEditDmMessage,
        IDeleteDmMessage, IGetDmMessages
    {
        BopperContext db = new BopperContext();
        public DM_MessagesRepo()
        {
            db.Configuration.LazyLoadingEnabled = false;
        }

        async public Task<DM_MessagesItems> createMessage(DM_MessagesItems msg)
        {
            msg.dm_MsgId = Guid.NewGuid();
            //msg.dateSent = DateTime.Now.Date;
            msg.timeStamp = DateTime.UtcNow;//CommonMethods.getKenyaTime();

            var message = new DM_Messages()
            {
                Id = msg.dm_MsgId,
                DirectMessageId = msg.directMessageId,
                ClientId = msg.clientId,
                Message = msg.message,
                Image = msg.image,
                Status = "MESSAGE",
                TimeStamp = msg.timeStamp,
                timeZone = msg.timeZone
            };

            try
            {
                db.DM_Messages.Add(message);
                await db.SaveChangesAsync();

                var dmGroup = db.DirectMessages.Where(d => d.Id == msg.directMessageId).FirstOrDefault();

                Guid clientX = msg.clientId;

                if (dmGroup.SenderId == msg.clientId)
                    clientX = dmGroup.RecipientId;
                else if (dmGroup.RecipientId == msg.clientId)
                    clientX = dmGroup.SenderId;

                var notification = new NotificationItems()
                {
                    clientId = clientX,
                    notificationType = NotificationType.DM.ToString(),
                    otherId = msg.dm_MsgId,
                    timeStamp = msg.timeStamp,
                    notificationMessage = db.Clients.Where(c => c.Id == msg.clientId).FirstOrDefault().FullNames + " sent you a direct message."
                };

              //  await new NotificationRepo().createNotificationHub(notification);

                return msg;
            }
            catch (Exception r)
            {
                Console.WriteLine(r.Message);
                return null;
            }
        }

        async public Task<bool> deleteMessage(Guid dmMsgId)
        {
            var message = db.DM_Messages.Where(x => x.Id == dmMsgId).FirstOrDefault();
            if (message != null)
            {
                try
                {
                    db.DM_Messages.Remove(message);
                    await db.SaveChangesAsync();
                    return true;
                }
                catch (Exception r)
                {
                    Console.WriteLine(r.Message);
                    return false;
                }
            }
            else
                return false;
        }

        async public Task<DM_MessagesItems> editMessage(DM_MessagesItems msg)
        {
            var message = db.DM_Messages.Where(m => m.Id == msg.dm_MsgId).FirstOrDefault();
            if (message != null)
            {
                message.Message = msg.message;
                message.Image = msg.image;

                try
                {
                    db.Entry(message).State = System.Data.Entity.EntityState.Modified;
                    await db.SaveChangesAsync();
                    return msg;
                }
                catch (Exception r)
                {
                    Console.WriteLine(r.Message);
                    return null;
                }
            }
            else
                return null;
        }

        public List<DM_MessagesItems> getChatRoomMessages(Guid directMessageId)
        {
            var returnMessages = new List<DM_MessagesItems>();

            var messages = db.DM_Messages.Where(c => c.DirectMessageId == directMessageId).OrderByDescending(x => x.TimeStamp).ToList();

            foreach (var m in messages)
            {
                var dmMsg = new DM_MessagesItems()
                {
                    clientId = m.ClientId,
                    directMessageId = m.DirectMessageId,
                    dm_MsgId = m.Id,
                    image = m.Image,
                    message = m.Message,
                    status = m.Status,
                    timeStamp = m.TimeStamp,
                    timeZone = m.timeZone
                };

                

                try {
                    dmMsg.timeStamp = CommonMethods.getTimeZonedTime(dmMsg.timeStamp, dmMsg.timeZone);
                } catch(Exception r)
                {
                    //Debug.WriteLine(r.Message);
                    //Debug.WriteLine(r.InnerException.Message);
                }

                returnMessages.Add(dmMsg);
            }

            return returnMessages;
        }

        public Task<DM_MessagesItems> replyMessage(DM_MessagesItems msg)
        {
            throw new NotImplementedException();
        }
    }
}