﻿using Bopper.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bopper.Data;
using System.Threading.Tasks;
using System.Diagnostics;
using Bopper.Data.Model;

namespace Bopper.Models
{
    #region Interfaces
    interface IAddPic { Task<FbClientPicItems> addPic(FbClientPicItems pic); }
    interface IAddPics { Task<List<FbClientPicItems>> addPics(List<FbClientPicItems> pics); }
    interface IGetClientPics { List<FbClientPicItems> getClientPics(Guid clientId); }
    interface IUpdateClientPic { Task<FbClientPicItems> updatePic(FbClientPicItems pic); }
    interface IDeleteClientPic { Task<bool> deletePic(Guid photoId); }

    interface IGetNumberOfPics { int getNumberOfPics(Guid clientId); }
    #endregion

    public class FbClientPicRepo : IAddPic, IAddPics, IGetClientPics, IUpdateClientPic, IDeleteClientPic, IGetNumberOfPics
    {
        BopperContext db = new BopperContext();

        public FbClientPicRepo() { db.Configuration.LazyLoadingEnabled = false; }

        async public Task<FbClientPicItems> addPic(FbClientPicItems pic)
        {
            pic.photoId = Guid.NewGuid();
            pic.timeStamp = DateTime.Now;

            try
            {
                db.FbClientPics.Add(new FbClientPics()
                {
                    ClientId = pic.clientId,
                    Id = pic.photoId,
                    PhotoType = pic.photoType,
                    PhotoUrl = pic.photoUrl,
                    TimeStamp = pic.timeStamp
                });

                await db.SaveChangesAsync();
                return pic;
            }catch(Exception r)
            {
                Debug.WriteLine(r.Message);
                Debug.WriteLine(r.InnerException.Message);
                return null;
            }
        }

        async public Task<List<FbClientPicItems>> addPics(List<FbClientPicItems> pics)
        {
            foreach (var pic in pics)
            {
                pic.photoId = Guid.NewGuid();
                pic.timeStamp = DateTime.Now;
            }

            try
            {
                foreach (var pic in pics)
                {
                    db.FbClientPics.Add(new FbClientPics()
                    {
                        ClientId = pic.clientId,
                        Id = pic.photoId,
                        PhotoType = pic.photoType,
                        PhotoUrl = pic.photoUrl,
                        TimeStamp = pic.timeStamp
                    });
                }

                await db.SaveChangesAsync();
                return pics;
            }
            catch (Exception r)
            {
                Debug.WriteLine(r.Message);
                Debug.WriteLine(r.InnerException.Message);
                return null;
            }
        }

        async public Task<bool> deletePic(Guid photoId)
        {
            var pic = db.FbClientPics.Where(p => p.Id == photoId).FirstOrDefault();
            if(pic != null)
            {
                try
                {
                    db.FbClientPics.Remove(pic);
                    await db.SaveChangesAsync();
                    return true;
                }catch(Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);
                    return false;
                }
            }else
            {
                return false;
            }
        }

        public List<FbClientPicItems> getClientPics(Guid clientId)
        {
            var lstPics = new List<FbClientPicItems>();
            var clientPics = (from f in db.FbClientPics
                              join c in db.Clients on f.ClientId equals c.Id
                              where f.ClientId == clientId
                              select new {
                                  f.Id,
                                  f.ClientId,
                                  f.PhotoUrl,
                                  f.PhotoType,
                                  f.TimeStamp,
                                  c.FullNames,
                                  c.ProfPic
                              });

            foreach(var pic in clientPics)           
                lstPics.Add(new FbClientPicItems()
                {
                    clientId = pic.ClientId,
                    clientName = pic.FullNames,
                    clientPic = pic.ProfPic,
                    photoId = pic.Id,
                    photoType = pic.PhotoType,
                    photoUrl = pic.PhotoUrl,
                    timeStamp = pic.TimeStamp
                });
            

            return lstPics;
        }

        public int getNumberOfPics(Guid clientId)
        {
            return db.FbClientPics.Where(c => c.ClientId == clientId).Count();
        }

        async public Task<FbClientPicItems> updatePic(FbClientPicItems pic)
        {
            var px = db.FbClientPics.Where(x => x.Id == pic.photoId).FirstOrDefault();

            if (px != null)
            {
                try
                {
                    px.PhotoType = pic.photoType;
                    px.PhotoUrl = pic.photoUrl;
                    db.Entry(px).State = System.Data.Entity.EntityState.Modified;

                    await db.SaveChangesAsync();
                    return pic;
                }
                catch (Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);
                    return null;
                }
            }
            else
                return null;
        }
    }
}