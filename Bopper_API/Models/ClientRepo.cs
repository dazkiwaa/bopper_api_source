﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bopper.ViewModels;
using System.Diagnostics;
using System.Threading.Tasks;
using Bopper.Data;

namespace Bopper.Models
{
    #region Interfaces
    interface ILoginRegistration
    { Task<ClientItems> loginRegistration(ClientItems cls); }

    interface IUpdateInfo
    { Task<ClientItems> updateInfo(ClientItems cls); }

    interface IChangeProfPic
    { Task<bool> changeProfPic(Guid clientId, string profPic); }

    interface IDeleteInfo
    { Task<bool> deleteInfo(Guid clientId); }

    interface ILoginClient
    { Task<ClientItems> loginClient(LoginItems lg); }

    interface ICreateClient
    { Task<ClientItems> createClient(ClientItems cls); }

    interface ICheckUserName
    { bool checkUserName(string userName); }

    interface IGetAllClients
    {
        List<ClientItems> getAllClients();
    }

    interface IForgotPassword
    { Task<bool> forgotPassword(string userName); }

    interface IGetClientAccountById
    {
        ClientItems getClientAccountById(Guid clientId);
    }

    interface IChangePassword
    { Task<NewPasswordItems> changePassword(NewPasswordItems np); }
    #endregion
    public class ClientRepo : ILoginRegistration, IUpdateInfo, 
        IChangeProfPic, IDeleteInfo, IGetAllClients
        ,ILoginClient, ICreateClient, ICheckUserName
        ,IForgotPassword, IChangePassword, IGetClientAccountById
    {
        BopperContext db = new BopperContext();

        public ClientRepo()
        { db.Configuration.LazyLoadingEnabled = false; }

        public async Task<NewPasswordItems> changePassword(NewPasswordItems np)
        {
            string pass = CommonMethods.encryptPassword(np.password);

            var client = db.Clients.Where(c => c.UserName == np.userName && c.Password == pass).FirstOrDefault();
            if (client != null)
            {
                client.Password = CommonMethods.encryptPassword(np.newPassword);

                try
                {

                    np.newPassword = "";
                    np.password = "";

                    db.Entry(client).State = System.Data.Entity.EntityState.Modified;

                    await db.SaveChangesAsync();

                    return np;

                }catch(Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);

                    return null;
                }
            }
            else
                return null;
            throw new NotImplementedException();
        }

        public async Task<bool> forgotPassword(string userName)
        {
            var client = db.Clients.Where(c => c.UserName == userName).FirstOrDefault();
            if (client != null)
            {
                string pass = CommonMethods.createPassword(10);

                client.Password = CommonMethods.encryptPassword(pass);

                try {

                    db.Entry(client).State = System.Data.Entity.EntityState.Modified;
                    await db.SaveChangesAsync();

                    CommonMethods.sendEmailViaWebApi("Bopper Password Change", "Your password has been changed to: " + pass + ". Login and change from the app.", client.Email);

                    return true;
                } catch(Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);

                    return false;
                }
            }
            else
                return false;
            throw new NotImplementedException();
        }

        public async Task<bool> changeProfPic(Guid clientId, string profPic)
        {
            var client = db.Clients.Where(c => c.Id == clientId).FirstOrDefault();
            if (client != null)
            {
                //Update email, phone number, fullnames, profpic              
                client.ProfPic = profPic;

                try
                {
                    db.Entry(client).State = System.Data.Entity.EntityState.Modified;
                    await db.SaveChangesAsync();
                    return true;
                }
                catch (Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);

                    return false;
                }
            }
            else
                return false;
        }

        public bool checkUserName(string userName)
        {
            var client = db.Clients.Where(c => c.UserName == userName).FirstOrDefault();// != null ? true : false;
            //if (client != null)
            //    return true;
            //else
            //    return false;

            return (client != null) ? true : false;
        }

        public async Task<ClientItems> createClient(ClientItems cls)
        {
            cls.userId = Guid.NewGuid();
            cls.password = CommonMethods.encryptPassword(cls.password);
            cls.timeStamp = DateTime.Now;

            try
            {

                var client = new Client()
                {
                    Email = cls.email,
                    FullNames = cls.fullNames,
                    Password = cls.password,
                    PhoneNumber = cls.phoneNumber,
                    ProfPic = cls.profPic,
                    TimeStamp = cls.timeStamp,
                    Id = cls.userId,
                    UserName = cls.userName,
                    City = cls.City,
                    State = cls.State,
                    Country = cls.Country,
                    CoverPhoto = cls.coverPhoto
                };

                db.Clients.Add(client);

                await db.SaveChangesAsync();

                return cls;

            }catch(Exception r)
            {
                Debug.WriteLine(r.Message);
                Debug.WriteLine(r.InnerException.Message);
                return null;
            }
        }

        //T b dfnd ltr
        public async Task<bool> deleteInfo(Guid clientId)
        {            
            var client = db.Clients.Where(c => c.Id == clientId).FirstOrDefault();
            if (client != null)
            {
                //Other methods will be added here

                db.Clients.Remove(client);

                try
                {
                    await db.SaveChangesAsync();
                    return true;
                }
                catch (Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);

                    return false;
                }
            }
            else
                return false;
        } 

        public List<ClientItems> getAllClients()
        {
            var allClients = new List<ClientItems>();

            var clients = db.Clients.OrderByDescending(c => c.TimeStamp);

            foreach (var c in clients)
            {
                var cl = (new ClientItems()
                {
                    userName = c.UserName,
                    email = c.Email,
                    fullNames = c.FullNames,
                    password = "",
                    phoneNumber = c.PhoneNumber,
                    profPic = c.ProfPic,
                    timeStamp = c.TimeStamp,
                    userId = c.Id,
                    bio = "Write something about yourself",
                    dob = "",
                    City = c.City,
                    State = c.State,
                    Country = c.Country,
                    gender = c.Gender,
                    coverPhoto = c.CoverPhoto
                });

                if (c.DOB != null)
                    cl.dob = c.DOB;

                if (c.Bio != null)
                    cl.bio = c.Bio;

                allClients.Add(cl);
            }

            return allClients;
        }

        public async Task<ClientItems> loginClient(LoginItems lg)
        {
            lg.password = CommonMethods.encryptPassword(lg.password);

            var client = db.Clients.Where(c => c.UserName == lg.userName).FirstOrDefault();

            if (client != null)
            {
                var cl = new ClientItems()
                {
                    email = client.Email,
                    fullNames = client.FullNames,
                    password = "",
                    phoneNumber = client.PhoneNumber,
                    profPic = client.ProfPic,
                    timeStamp = client.TimeStamp,
                    userId = client.Id,
                    userName = client.UserName,
                    bio = "",
                    dob = "",
                    City = client.City,
                    State = client.State,
                    Country = client.Country,
                    gender = client.Gender
                };

                if (client.DOB != null)
                    cl.dob = client.DOB;

                if (client.Bio != null)
                    cl.bio = client.Bio;

                return cl;
            }
            else
                return null;
        }

        public async Task<ClientItems> loginRegistration(ClientItems cls)
        {
            //Check if user has account with us before
            var client = db.Clients.Where(c => c.Email == cls.email).FirstOrDefault();
            if(client == null) //Register the user here
            {
                cls.userId = Guid.NewGuid();
                cls.timeStamp = DateTime.Now;
                cls.password = CommonMethods.encryptPassword(cls.email); //Set the password as email

                var clientSvr = new Client()
                {
                    Email = cls.email,
                    UserName = cls.userName,
                    FullNames = cls.fullNames,
                    Password = cls.password,
                    PhoneNumber = cls.phoneNumber,
                    ProfPic = cls.profPic,
                    TimeStamp = cls.timeStamp,
                    Id = cls.userId,
                    City = cls.City,
                    State = cls.State,
                    Country = cls.Country,
                    DOB = cls.dob,
                    Bio = cls.bio,
                    Gender = cls.gender,
                    CoverPhoto = cls.coverPhoto
                };

                try {
                    db.Clients.Add(clientSvr);
                    await db.SaveChangesAsync();
                    return cls;

                } catch(Exception r) {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);

                    return null;
                }
            }
            else
            {
                //Update the prof pics here for FB
                if(String.IsNullOrEmpty(client.CoverPhoto) || client.CoverPhoto != cls.coverPhoto)
                {
                    client.ProfPic = cls.profPic;
                    client.CoverPhoto = cls.coverPhoto;

                    db.SaveChanges();
                }

                var clientItems = new ClientItems()
                {
                    userName = client.UserName,
                    email = client.Email,
                    fullNames = client.FullNames,
                    password = "",
                    phoneNumber = client.PhoneNumber,
                    profPic = client.ProfPic,
                    timeStamp = client.TimeStamp,
                    userId = client.Id,
                    City = client.City,
                    State = client.State,
                    Country = client.Country,
                    dob = client.DOB,
                    bio = client.Bio,
                    gender = client.Gender,
                    coverPhoto = cls.coverPhoto
                };

                return clientItems;
            }            
        }

        async public Task<ClientItems> updateInfo(ClientItems cls)
        {
            var client = db.Clients.Where(c => c.Id == cls.userId).FirstOrDefault();
            if (client != null)
            {
                //Update email, phone number, fullnames, profpic
                client.Email = cls.email;
                client.PhoneNumber = cls.phoneNumber;
                client.FullNames = cls.fullNames;
                client.ProfPic = cls.profPic;
                client.City = cls.City;
                client.State = cls.State;
                client.Country = cls.Country;
                client.DOB = cls.dob;
                client.Bio = cls.bio;
                client.Gender = cls.gender;
                client.CoverPhoto = cls.coverPhoto;
                //client.UserName = cls.userName;

                try {
                    db.Entry(client).State = System.Data.Entity.EntityState.Modified;
                    await db.SaveChangesAsync();
                    return cls;
                } catch(Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);

                    return null;
                }
            }
            else
                return null;            
        }

        public ClientItems getClientAccountById(Guid clientId)
        {
            var client = db.Clients.Where(c => c.Id == clientId).FirstOrDefault();


            return (client != null) ? (new ClientItems()
            {
                userId = client.Id,
                userName = client.UserName,
                bio = client.Bio,
                City = client.City,
                Country = client.Country,
                dob = client.DOB,
                email = client.Email,
                fullNames = client.FullNames,
                gender = client.Gender,
                profPic = client.ProfPic,
                State = client.State,
                coverPhoto = client.CoverPhoto,
                timeStamp = client.TimeStamp
            }) : null;
        }
    }
}