﻿using Bopper.Data;
using Bopper.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bopper.Models
{
    #region Interfaces
    interface IAddContact
    { Task<ContactItems> addContact(ContactItems contact); }
    interface IAddContactByEmail
    { Task<bool?> addContactByMail(ContactItems contact, string email); }
    interface IDeleteContact
    { Task<bool> deleteContact(Guid cId); }
    interface IGetMyContacts
    { List<ContactItems> getMyContacts(Guid userId); }

    interface ICheckIfAlreadyAdded
    { bool checkIfAddedContact(Guid userId, Guid contactId); }

    interface IGetNumberOfContacts
    { int getNumberOfContacts(Guid clientId); }
    #endregion
    public class ContactsRepo : IAddContact, IDeleteContact, IGetMyContacts, ICheckIfAlreadyAdded, IGetNumberOfContacts,
        IAddContactByEmail
    {
        BopperContext db = new BopperContext();

        public ContactsRepo() { db.Configuration.LazyLoadingEnabled = false; }

        async public Task<ContactItems> addContact(ContactItems contact)
        {
            

            try
            {
                contact.cId = Guid.NewGuid();
                contact.timeStamp = DateTime.Now;

                db.Contacts.Add(new Contact() {
                    Id = contact.cId,
                    ContactId = contact.contactId,
                    TimeStamp = contact.timeStamp,
                    UserId = contact.userId
                });

                await db.SaveChangesAsync();

                return contact;
            }catch(Exception r)
            {
                Debug.WriteLine(r.Message);
                Debug.WriteLine(r.InnerException.Message);

                return null;
            }
        }

        public async Task<bool?> addContactByMail(ContactItems contact, string email)
        {
            var client = db.Clients.Where(c => c.Email == email).FirstOrDefault();

            if (client != null)
            {
                if(checkIfAddedContactX(client.Id, contact.userId))
                {
                    contact.contactId = client.Id;
                    var data = await addContact(contact);
                    return (data != null) ? true : false;
                }
                return false;
            }
            else
                return null;
        }

        public bool checkIfAddedContact(Guid userId, Guid contactId) //Return true if not added
        {
            var contact = db.Contacts.Where(c => c.UserId == userId && c.ContactId == contactId).FirstOrDefault();


            return (contact == null) ? true : false;
        }

        public bool checkIfAddedContactX(Guid userId, Guid contactId) //Return true if not added
        {
             var myContacts = (from c in db.Contacts
                              join cl in db.Clients on c.ContactId equals cl.Id
                              where c.UserId == userId && c.ContactId == contactId
                              select new {
                                  c.Id,
                                  c.ContactId,
                                  c.UserId,
                                  c.TimeStamp,
                                  cl.FullNames,
                                  cl.ProfPic,
                                  cl.Email,
                                  cl.DOB
                              }).FirstOrDefault();

            var otherContacts = (from c in db.Contacts
                              join cl in db.Clients on c.UserId equals cl.Id
                              //where c.ContactId == userId
                                 where c.ContactId == userId && c.UserId == contactId
                                 select new {
                                  c.Id,
                                  c.ContactId,
                                  c.UserId,
                                  c.TimeStamp,
                                  cl.FullNames,
                                  cl.ProfPic,
                                  cl.Email,
                                  cl.DOB
                              }).FirstOrDefault();

            return (myContacts == null && otherContacts == null) ? true : false;
        }

        async public Task<bool> deleteContact(Guid cId)
        {
            var contact = db.Contacts.Where(c => c.Id == cId).FirstOrDefault();

            if (contact != null)
            {
                try
                {
                    db.Contacts.Remove(contact);
                    await db.SaveChangesAsync();                
                    return true;
                }
                catch (Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);

                    return false;
                }
            }
            else
                return false;
        }

        public List<ContactItems> getMyContacts(Guid userId)
        {
            var lstMyContacts = new List<ContactItems>();

            var myContacts = (from c in db.Contacts
                              join cl in db.Clients on c.ContactId equals cl.Id
                              where c.UserId == userId
                              select new {
                                  c.Id,
                                  c.ContactId,
                                  c.UserId,
                                  c.TimeStamp,
                                  cl.FullNames,
                                  cl.ProfPic,
                                  cl.Email,
                                  cl.DOB
                              });

            var otherContacts = (from c in db.Contacts
                              join cl in db.Clients on c.UserId equals cl.Id
                              //where c.ContactId == userId
                                 where c.ContactId == userId
                                 select new {
                                  c.Id,
                                  c.ContactId,
                                  c.UserId,
                                  c.TimeStamp,
                                  cl.FullNames,
                                  cl.ProfPic,
                                  cl.Email,
                                  cl.DOB
                              });

            foreach (var c in myContacts)
                lstMyContacts.Add(new ContactItems() {
                    cId = c.Id,
                    contactId = c.ContactId,
                    email = c.Email,
                    fullNames = c.FullNames,
                    profPic = c.ProfPic,
                    timeStamp = c.TimeStamp,
                    userId = c.UserId,
                    age = c.DOB
                });

            foreach (var c in otherContacts)
                lstMyContacts.Add(new ContactItems()
                {
                    cId = c.Id,
                    contactId = c.ContactId,
                    email = c.Email,
                    fullNames = c.FullNames,
                    profPic = c.ProfPic,
                    timeStamp = c.TimeStamp,
                    userId = c.UserId,
                    age = c.DOB
                });

            return lstMyContacts.OrderBy(c => c.fullNames).ToList();
        }

        public int getNumberOfContacts(Guid clientId)
        {
            return db.Contacts.Where(c => c.UserId == clientId).Count();
        }
    }
}