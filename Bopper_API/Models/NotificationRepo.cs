﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bopper.ViewModels;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.Azure.NotificationHubs;
using Bopper.Data;

namespace Bopper.Models
{

    #region Interfaces
    interface ICreateNotification
    { Task<NotificationItems> createNotification(NotificationItems notif); }

    interface IDeleteNotification
    { Task<bool> deleteNotification(Guid notifId); }

    interface IGetMyNotifications
    { List<NotificationReturnItems> getMyNotifications(Guid clientId); }

    interface ICreateNotificationMentor
    { Task<NotificationItems> createNotificationMentor(NotificationItems notif); }

    interface IGetMentorNotifications
    { List<NotificationReturnItems> getMentorNotifications(Guid mentorId); }

    interface ICreateNotificationHub
    { Task<NotificationReturnItems> createNotificationHub(NotificationItems notif); }

    interface IGetMyNotificationNumbers
    { int getMyNotificationNumbers(Guid clientId); }
    
    interface IGetAllNotifications
    { List<NotificationItems> getAllNotifications(); }
     #endregion


    public class NotificationRepo : ICreateNotification, IDeleteNotification, IGetMyNotifications,
        ICreateNotificationHub, IGetMyNotificationNumbers, IGetAllNotifications
    {
        BopperContext db = new BopperContext();

        static public string NHConnString =
            "Endpoint=sb://bopperv2.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=vdjw2Ti9Ce17wWahfrLF2X8PpArEi5rOV+25HjpKRZ0=";

        public NotificationRepo() { db.Configuration.LazyLoadingEnabled = false; }

        async public Task<NotificationItems> createNotification(NotificationItems notif)
        {
            notif.notificationId = Guid.NewGuid();
            notif.timeStamp = DateTime.UtcNow;//CommonMethods.getKenyaTime();

            var notification = new Data.Notification()
            {
                ClientId = notif.clientId,
                Id = notif.notificationId,
                NotificationMessage = notif.notificationMessage,
                NotificationType = notif.notificationType,
                OtherId = notif.otherId,
                TimeStamp = notif.timeStamp
            };

            try
            {
                db.Notifications.Add(notification);

                await db.SaveChangesAsync();

                return notif;
            }
            catch (Exception r)
            {
                Debug.Print(r.Message);
                Debug.Print(r.InnerException.Message);

                return null;
            }
        }

       

        public async Task<bool> deleteNotification(Guid notifId)
        {
            var notification = db.Notifications.Where(n => n.Id == notifId).FirstOrDefault();

            if (notification != null)
            {
                try
                {
                    db.Notifications.Remove(notification);
                    await db.SaveChangesAsync();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            else
                return false;
        }


        public List<NotificationReturnItems> getMyNotifications(Guid clientId)
        {
            var myNotifications = new List<NotificationReturnItems>();

            var notifications = db.Notifications.Where(n => n.ClientId == clientId);

            if (notifications != null)
            {
                foreach (var n in notifications)
                {
                    var notif = new NotificationReturnItems()
                    {
                        clientId = n.ClientId,
                        notificationId = n.Id,
                        notificationMessage = n.NotificationMessage,
                        notificationType = n.NotificationType,
                        otherId = (Guid)n.OtherId,
                        timeStamp = n.TimeStamp
                    };

                    if (n.NotificationType.ToLower().Equals(NotificationType.DM.ToString().ToLower()))
                    {
                        var dms = (from dm in db.DM_Messages
                                   join c in db.Clients on dm.ClientId equals c.Id
                                   where dm.Id == n.OtherId
                                   select new { c.FullNames, c.ProfPic, dm.DirectMessageId, c.Id }).FirstOrDefault();

                        notif.clientId = dms.Id;
                        notif.clientName = dms.FullNames;
                        notif.clientProfpic = dms.ProfPic;
                        notif.directMessageId = dms.DirectMessageId;
                        notif.otherId = dms.DirectMessageId;
                    }
                    else if (n.NotificationType.ToLower().Equals(NotificationType.INVITE.ToString().ToLower()))
                    {
                        var invite = (from i in db.Invites
                                      join e in db.Events on i.EventId equals e.Id
                                      join c in db.Clients on e.PosterId equals c.Id
                                      where i.Id == n.OtherId
                                      select new { c.FullNames, c.ProfPic, i.Id, i.UserId, i.EventId }).FirstOrDefault();

                        notif.clientId = invite.UserId;
                        notif.clientName = invite.FullNames;
                        notif.clientProfpic = invite.ProfPic;
                        notif.directMessageId = invite.EventId;
                        notif.otherId = invite.Id;
                    }
                    else if (n.NotificationType.ToLower().Equals(NotificationType.REQUEST.ToString().ToLower()))
                    {
                        var rsvp = (from r in db.RSVPs
                                    join e in db.Events on r.EventId equals e.Id
                                    join c in db.Clients on r.ClientId equals c.Id
                                    where r.Id == n.OtherId
                                    select new { c.FullNames, c.ProfPic, r.Id, r.ClientId, r.EventId }).FirstOrDefault();

                        notif.clientId = rsvp.ClientId;
                        notif.clientName = rsvp.FullNames;
                        notif.clientProfpic = rsvp.ProfPic;
                        notif.directMessageId = rsvp.ClientId;
                        notif.otherId = rsvp.Id;
                    }
                    else if (n.NotificationType.ToLower().Equals(NotificationType.MENTION.ToString().ToLower()))
                    {
                        var eventX = (from e in db.Events
                                      join ec in db.EventChatMessages on e.Id equals ec.EventId
                                      join c in db.Clients on ec.ClientId equals c.Id
                                      where e.Id == n.OtherId
                                      select new { c.FullNames, c.ProfPic, e.Id, e.Title, e.PosterId }).FirstOrDefault();


                        notif.clientId = n.ClientId;
                        notif.clientName = eventX.FullNames;
                        notif.clientProfpic = eventX.ProfPic;
                        notif.directMessageId = eventX.PosterId;
                        notif.otherId = eventX.Id;
                    }
                    else if (n.NotificationType.ToLower().Equals(NotificationType.CANCEL.ToString().ToLower()))
                    {
                        var rsvp = (from r in db.RSVPs
                                    join e in db.Events on r.EventId equals e.Id
                                    join c in db.Clients on r.ClientId equals c.Id
                                    where r.Id == n.OtherId
                                    select new { c.FullNames, c.ProfPic, r.Id, r.ClientId, r.EventId, e.Photo1, }).FirstOrDefault();

                        notif.clientId = rsvp.ClientId;
                        notif.clientName = rsvp.FullNames;
                        notif.clientProfpic = rsvp.Photo1;
                        notif.directMessageId = rsvp.ClientId;
                        notif.otherId = rsvp.Id;
                    }


                    myNotifications.Add(notif);
                }

                return myNotifications.OrderByDescending(n => n.timeStamp).ToList();
            }
            else
                return null;
        }

        public async Task<bool> sendGCMNotification(NotificationReturnItems msg, Guid userId)
        {
            NotificationHubClient hub = NotificationHubClient.CreateClientFromConnectionString(NHConnString, "bopperv2-notifications");
            try
            {
                string message = Newtonsoft.Json.JsonConvert.SerializeObject(msg);
                var notif = "{ \"data\" : {\"message\":" + message + "}}";

                string tag = userId.ToString();

                await hub.SendGcmNativeNotificationAsync(notif, tag);

                return true;
            }
            catch (Exception r) { Debug.WriteLine(r.Message); return false; }
        }

        public async Task<bool> sendGCMStringNotification(string message)
        {
            NotificationHubClient hub = NotificationHubClient.CreateClientFromConnectionString(NHConnString, "bopperv2-notifications");
            try
            {
                var notif = "{ \"data\" : {\"message\":" + "\"" + message + "\"" + "}}";


                var x = await hub.SendGcmNativeNotificationAsync(notif);//, tag);

                return true;
            }
            catch (Exception r)
            {
                Debug.WriteLine(r.Message);
                return false;
            }
        }

        async public Task<NotificationReturnItems> createNotificationHub(NotificationItems x)
        {
            var n = await createNotification(x);
            if (n != null)
            {
                try
                {
                    var notif = new NotificationReturnItems()
                    {
                        clientId = n.clientId,
                        notificationId = x.notificationId,
                        notificationMessage = n.notificationMessage,
                        notificationType = n.notificationType,
                        otherId = (Guid)n.otherId,
                        timeStamp = n.timeStamp
                    };

                    if (n.notificationType.ToLower().Equals(NotificationType.DM.ToString().ToLower()))
                    {
                        var dms = (from dm in db.DM_Messages
                                   join c in db.Clients on dm.ClientId equals c.Id
                                   where dm.Id == n.otherId
                                   select new { c.FullNames, c.ProfPic, dm.DirectMessageId, c.Id }).FirstOrDefault();

                        if (dms != null)
                        {
                            notif.clientId = dms.Id;
                            notif.clientName = dms.FullNames;
                            notif.clientProfpic = dms.ProfPic;
                            notif.directMessageId = dms.DirectMessageId;
                            notif.otherId = dms.DirectMessageId;
                        }
                    }
                    else if (n.notificationType.ToLower().Equals(NotificationType.INVITE.ToString().ToLower()))
                    {
                        var invite = (from i in db.Invites
                                      join e in db.Events on i.EventId equals e.Id
                                      join c in db.Clients on e.PosterId equals c.Id
                                      where i.Id == n.otherId
                                      select new { c.FullNames, c.ProfPic, i.Id, i.UserId, i.EventId }).FirstOrDefault();

                        notif.clientId = invite.UserId;
                        notif.clientName = invite.FullNames;
                        notif.clientProfpic = invite.ProfPic;
                        notif.directMessageId = invite.EventId;
                        notif.otherId = invite.Id;
                    }
                    else if (n.notificationType.ToLower().Equals(NotificationType.REQUEST.ToString().ToLower()))
                    {
                        //var rsvp = (from r in db.RSVPs
                        //            join e in db.Events on r.EventId equals e.Id
                        //            join c in db.Clients on r.ClientId equals c.Id
                        //            where r.Id == n.otherId
                        //            select new { c.FullNames, c.ProfPic, r.Id, r.ClientId, r.EventId }).FirstOrDefault();
                        var rsvp = (from r in db.RSVPs
                                    join e in db.Events on r.EventId equals e.Id
                                    join c in db.Clients on r.ClientId equals c.Id
                                    where r.Id == n.otherId
                                    select new { c.FullNames, c.ProfPic, r.Id, r.ClientId, r.EventId }).FirstOrDefault();

                        notif.clientId = rsvp.ClientId;
                        notif.clientName = rsvp.FullNames;
                        notif.clientProfpic = rsvp.ProfPic;
                        notif.directMessageId = rsvp.EventId;
                        notif.otherId = rsvp.Id;
                    }
                    else if (n.notificationType.ToLower().Equals(NotificationType.MENTION.ToString().ToLower()))
                    {
                        var eventX = (from e in db.Events
                                      join ec in db.EventChatMessages on e.Id equals ec.EventId
                                      join c in db.Clients on ec.ClientId equals c.Id
                                      where e.Id == n.otherId
                                      select new { c.FullNames, c.ProfPic, e.Id, e.Title, e.PosterId }).FirstOrDefault();


                        notif.clientId = n.clientId;
                        notif.clientName = eventX.FullNames;
                        notif.clientProfpic = eventX.ProfPic;
                        notif.directMessageId = eventX.PosterId;
                        notif.otherId = eventX.Id;
                    }
                    else if (n.notificationType.ToLower().Equals(NotificationType.CANCEL.ToString().ToLower()))
                    {
                        //var rsvp = (from r in db.RSVPs
                        //            join e in db.Events on r.EventId equals e.Id
                        //            join c in db.Clients on r.ClientId equals c.Id
                        //            where r.Id == n.otherId
                        //            select new { c.FullNames, c.ProfPic, r.Id, r.ClientId, r.EventId }).FirstOrDefault();
                        var rsvp = (from r in db.RSVPs
                                    join e in db.Events on r.EventId equals e.Id
                                    join c in db.Clients on r.ClientId equals c.Id
                                    where r.Id == n.otherId
                                    select new { c.FullNames, c.ProfPic, r.Id, r.ClientId, r.EventId, e.Photo1 }).FirstOrDefault();

                        notif.clientId = rsvp.ClientId;
                        notif.clientName = rsvp.FullNames;
                        notif.clientProfpic = rsvp.Photo1;
                        notif.directMessageId = rsvp.EventId;
                        notif.otherId = rsvp.Id;
                    }

                    await sendGCMNotification(notif, n.clientId);
                    return notif;
                }catch(Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);

                    return null;
                }
            }
            else
                return null;
            //throw new NotImplementedException();
        }

        public int getMyNotificationNumbers(Guid clientId)
        {
            var myNotifications = db.Notifications.Where(n => n.ClientId == clientId);

            return myNotifications != null ? myNotifications.Count() : 0;
        }

        public List<NotificationItems> getAllNotifications()
        {
            var allNotifications = new List<NotificationItems>();

            foreach(var n in db.Notifications)
                allNotifications.Add(new NotificationItems() {
                    clientId = n.ClientId,
                    notificationId = n.Id,
                    notificationMessage = n.NotificationMessage,
                    notificationType = n.NotificationType,
                    otherId = (Guid)n.OtherId,
                    timeStamp = n.TimeStamp
                });

            return allNotifications;
        }

        public async Task<NotificationReturnItems> sendTestNotification(NotificationItems nt)
        {
            var n = nt;
            if (n != null)
            {
                try
                {
                    var notif = new NotificationReturnItems()
                    {
                        clientId = n.clientId,
                        notificationId = nt.notificationId,
                        notificationMessage = n.notificationMessage,
                        notificationType = n.notificationType,
                        otherId = (Guid)n.otherId,
                        timeStamp = n.timeStamp
                    };

                    if (n.notificationType.ToLower().Equals(NotificationType.DM.ToString().ToLower()))
                    {
                        var dms = (from dm in db.DM_Messages
                                   join c in db.Clients on dm.ClientId equals c.Id
                                   where dm.Id == n.otherId
                                   select new { c.FullNames, c.ProfPic, dm.DirectMessageId, c.Id }).FirstOrDefault();

                        if (dms != null)
                        {
                            notif.clientId = dms.Id;
                            notif.clientName = dms.FullNames;
                            notif.clientProfpic = dms.ProfPic;
                            notif.directMessageId = dms.DirectMessageId;
                            notif.otherId = dms.DirectMessageId;
                        }
                    }
                    else if (n.notificationType.ToLower().Equals(NotificationType.INVITE.ToString().ToLower()))
                    {
                        var invite = (from i in db.Invites
                                      join e in db.Events on i.EventId equals e.Id
                                      join c in db.Clients on e.PosterId equals c.Id
                                      where i.Id == n.otherId
                                      select new { c.FullNames, c.ProfPic, i.Id, i.UserId, i.EventId }).FirstOrDefault();

                        notif.clientId = invite.UserId;
                        notif.clientName = invite.FullNames;
                        notif.clientProfpic = invite.ProfPic;
                        notif.directMessageId = invite.EventId;
                        notif.otherId = invite.Id;
                    }
                    else if (n.notificationType.ToLower().Equals(NotificationType.REQUEST.ToString().ToLower()))
                    {
                        var rsvp = (from r in db.RSVPs
                                    join e in db.Events on r.EventId equals e.Id
                                    join c in db.Clients on r.ClientId equals c.Id
                                    where r.Id == n.otherId
                                    select new { c.FullNames, c.ProfPic, r.Id, r.ClientId, r.EventId }).FirstOrDefault();

                        notif.clientId = rsvp.ClientId;
                        notif.clientName = rsvp.FullNames;
                        notif.clientProfpic = rsvp.ProfPic;
                        notif.directMessageId = rsvp.EventId;
                        notif.otherId = rsvp.Id;
                    }
                    else if (n.notificationType.ToLower().Equals(NotificationType.MENTION.ToString().ToLower()))
                    {
                        var eventX = (from e in db.Events
                                      join ec in db.EventChatMessages on e.Id equals ec.EventId
                                      join c in db.Clients on ec.ClientId equals c.Id
                                      where e.Id == n.otherId
                                      select new { c.FullNames, c.ProfPic, e.Id, e.Title, e.PosterId }).FirstOrDefault();


                        notif.clientId = n.clientId;
                        notif.clientName = eventX.FullNames;
                        notif.clientProfpic = eventX.ProfPic;
                        notif.directMessageId = eventX.PosterId;
                        notif.otherId = eventX.Id;
                    }

                    await sendGCMNotification(notif, n.clientId);
                    return notif;
                }
                catch (Exception r)
                {
                    Debug.WriteLine(r.Message);
                    Debug.WriteLine(r.InnerException.Message);

                    return null;
                }
            }
            else
                return null;
            //throw new NotImplementedException();
        }

           
    }

    public enum NotificationType { DM, INVITE, REQUEST, MENTION, CANCEL }
}