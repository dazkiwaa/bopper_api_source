﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Bopper.ViewModels;
using Bopper.Models;
using Newtonsoft.Json;

namespace Bopper.Hubs
{



    public class MessagesHub : Hub
    {
        readonly DM_MessagesRepo cRepo = new DM_MessagesRepo();
        readonly BopperContext db = new BopperContext();
        readonly EventChatRepo chRepo = new EventChatRepo();

        //Send message on new chat message
        public void SendMessages(string mx)
        {
            DM_MessagesItems cht = JsonConvert.DeserializeObject<DM_MessagesItems>(mx);

            var dmGroup = db.DirectMessages.Where(d => d.Id == cht.directMessageId).FirstOrDefault();

            Guid clientX = cht.clientId;

            if (dmGroup.SenderId == cht.clientId)
                clientX = dmGroup.RecipientId;
            else if (dmGroup.RecipientId == cht.clientId)
                clientX = dmGroup.SenderId;

            var data = cRepo.getChatRoomMessages(cht.directMessageId);


            Clients.Client(users.Where(u => u.userName == clientX.ToString()).FirstOrDefault().connectionId).
                sendMessages(JsonConvert.SerializeObject(data));
        }

        public void SendMessagesAll(string mx)
        {
            DM_MessagesItems cht = JsonConvert.DeserializeObject<DM_MessagesItems>(mx);

            var dmGroup = db.DirectMessages.Where(d => d.Id == cht.directMessageId).FirstOrDefault();

            Guid clientX = cht.clientId;

            if (dmGroup.SenderId == cht.clientId)
                clientX = dmGroup.RecipientId;
            else if (dmGroup.RecipientId == cht.clientId)
                clientX = dmGroup.SenderId;

            var data = cRepo.getChatRoomMessages(cht.directMessageId);

            Clients.All.
                sendMessagesAll(JsonConvert.SerializeObject(data));
        }


        public async Task SendChat(string mx)
        {
            DM_MessagesItems cht = JsonConvert.DeserializeObject<DM_MessagesItems>(mx);
            var data = await cRepo.createMessage(cht);

            var myMessages = cRepo.getChatRoomMessages(cht.directMessageId);
            SendMessagesAll(mx);
            SendMessages(mx);
            if (data != null)
                Clients.Caller.SendChat(JsonConvert.SerializeObject(myMessages));
            else
                Clients.Caller.SendChat("ERROR");
        }

        public void GetDMChatMessages(string dmId)
        {
            var data = cRepo.getChatRoomMessages(Guid.Parse(dmId));

            Clients.Caller.getDMChatMessages(JsonConvert.SerializeObject(data));
        }


        public void GetMyDirectMessages(string sId)
        {
            var data = new DirectMessageRepo().getMyDms(Guid.Parse(sId));
            Clients.Caller.getMyDirectMessages(JsonConvert.SerializeObject(data));
        }

        

        public async Task SendEventChat(string mx)
        {
            EventChatItems cht = JsonConvert.DeserializeObject<EventChatItems>(mx);

            var data = await chRepo.commentOnArticle(cht);

            Clients.Caller.SendEventChat(JsonConvert.SerializeObject(data));
        }

        public void GetChatMessages(string chId)
        {
            var data = new EventChatRepo().getArticleComments(Guid.Parse(chId));
            Clients.Caller.GetChatMessages(JsonConvert.SerializeObject(data));
        }

        static List<HubUsers> users = new List<HubUsers>();
        #region Preamble
        public override Task OnConnected()
        {
            var us = new HubUsers();
            us.userName = Context.QueryString["userName"];
            us.connectionId = Context.ConnectionId;
            users.Add(us);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            var u = users.Where(x => x.connectionId == Context.ConnectionId).FirstOrDefault();
            users.Remove(u);
            return base.OnDisconnected(stopCalled);
        }
        #endregion
    }

    public class HubUsers
    {
        public string userName { get; set; }
        public string connectionId { get; set; }
    }
}