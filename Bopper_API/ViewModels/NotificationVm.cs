﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bopper.ViewModels
{
    public class NotificationVm
    {
        public System.Guid ReceiverId { get; set; }
        public string NotificationTitle { get; set; }
        public string NotificationMessage { get; set; }
        public string Type { get; set; }
        public Guid? TypeId { get; set; }
    }
}