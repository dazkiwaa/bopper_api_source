﻿using Bopper.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bopper.ViewModels
{
    public class InviteRequestVm
    {
        public System.Guid EventId { get; set; }
        public System.Guid UserId { get; set; }
        public virtual Client Client { get; set; }
        public virtual Event Event { get; set; }
    }
}