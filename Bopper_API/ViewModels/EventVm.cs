﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bopper.ViewModels
{
    public class EventVm
    {
        public string Title { get; set; }
        public System.DateTime Date { get; set; }
        public System.DateTime StartTime { get; set; }
        public System.DateTime EndTime { get; set; }
        public System.Data.Entity.Spatial.DbGeography Location { get; set; }
        public string PlaceName { get; set; }
        public string Description { get; set; }
        public double Cost { get; set; }
        public int MaxGuests { get; set; }
        public string Photo1 { get; set; }
        public string Photo2 { get; set; }
        public string Photo3 { get; set; }
        public string Photo4 { get; set; }
        public string EventType { get; set; }
        public System.Guid PosterId { get; set; }
        public string EventAccess { get; set; }
        public string Photo5 { get; set; }
    }
}