﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bopper.ViewModels
{
    public class RSVPVm
    {
        public System.Guid ClientId { get; set; }
        public System.Guid EventId { get; set; }
        public System.Guid InviteId { get; set; }
        public string Status { get; set; }
    }
}