﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bopper.Data.Model
{
    public class Dashboard
    {
        public float TotalAmountPaid { get; set; }
        public int TotalRSVP { get; set; }
        public int TotalUsers { get; set; }
        public int TotalEventsCreated { get; set; }
    }

    public class EventStat
    {
        public string Month { get; set; }
        public int NightLife { get; set; }
        public int Travel { get; set; }
        public int Concert { get; set; }
        public int Sporting { get; set; }
        public int Food { get; set; }

    }
}
