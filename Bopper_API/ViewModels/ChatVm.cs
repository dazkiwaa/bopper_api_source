﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bopper.ViewModels
{
    public class ChatVm
    {
        public System.Guid UniqueMessageId { get; set; }
        public System.Guid SenderId { get; set; }
        public string Message { get; set; }
        public string ImageLink { get; set; }
        public string Status { get; set; }
        public System.DateTime DateSent { get; set; }
    }
}