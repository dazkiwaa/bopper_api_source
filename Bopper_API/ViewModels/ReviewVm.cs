﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bopper.ViewModels
{
    public class ReviewVm
    {
        public Guid EventId { get; set; }
        public string Comment { get; set; }
    }
}