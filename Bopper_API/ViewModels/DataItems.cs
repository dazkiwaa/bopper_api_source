﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Bopper.ViewModels
{
    public class CommonMethods
    {
        //All methods here are static
        public static DbGeography convertLocItemsToDBGeo(LocationItems vLoc)
        {
            var point = string.Format("POINT({1} {0})", vLoc.lats, vLoc.longs);
            return DbGeography.FromText(point);
        }
        public static LocationItems convertDbGeoToLocItems(DbGeography vGeo)
        {
            return new LocationItems() { lats = (float)vGeo.Latitude, longs = (float)vGeo.Longitude };
        }
        public static string encryptPassword(string clearText)
        {
            string EncryptionKey = "BPR";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        public static string decryptPassword(string cipherText)
        {
            string EncryptionKey = "BPR";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
    

        public static void sendEmailViaWebApi(string sbj, string msg, string recipient)
        {
            string str1 = sbj;
            string str2 = CommonMethods.PopulateBody(recipient, sbj, msg);
            string address = "controlmkononi@gmail.com";
            string addresses = recipient;
            MailMessage mailMessage = new MailMessage();
            SmtpClient smtpClient = new SmtpClient("smtp.gmail.com");
            mailMessage.From = new MailAddress(address);
            mailMessage.To.Add(addresses);
            mailMessage.Subject = str1;
            mailMessage.Body = str2;
            mailMessage.IsBodyHtml = true;
            int num1 = 587;
            smtpClient.Port = num1;
            NetworkCredential networkCredential = new NetworkCredential("controlmkononi@gmail.com", "rianKiwaa258025");
            smtpClient.Credentials = (ICredentialsByHost)networkCredential;
            int num2 = 1;
            smtpClient.EnableSsl = num2 != 0;
            MailMessage message = mailMessage;
            smtpClient.Send(message);
        }

        public static string PopulateBody(string userName, string title, string description)
        {
            string str = string.Empty;
            using (StreamReader streamReader = new StreamReader(HttpContext.Current.Server.MapPath("~/emailbody.html")))
                str = streamReader.ReadToEnd();
            return str.Replace("{UserName}", userName).Replace("{Title}", title).Replace("{Description}", description);
        }

        public static string createPassword(int length)
        {
            StringBuilder stringBuilder = new StringBuilder();
            Random random = new Random();
            while (0 < length--)
                stringBuilder.Append("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"[random.Next("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".Length)]);
            return stringBuilder.ToString();
        }

        static public DateTime getKenyaTime()
        {
            //TimeZone time2 = TimeZone.CurrentTimeZone;
            //DateTime test = time2.ToUniversalTime(DateTime.Now);
            //var kenya = TimeZoneInfo.FindSystemTimeZoneById("E. Africa Standard Time");
            //var kenyaStandardTime = TimeZoneInfo.ConvertTimeFromUtc(test, kenya);

            //return kenyaStandardTime;
            return DateTime.Now;
        }

        static public DateTime getTimeZonedTime(DateTime dtX, string timeZone)
        {
            //var dateUtc = dtX;//DateTime.UtcNow;

            //string offsetType = timeZone.Substring(0, 1);

            //if (offsetType.Equals("+"))
            //{
            //    string offsetTime = timeZone.Substring(1);

            //    var offsetHour = TimeSpan.Parse(offsetTime);

            //    dateUtc += offsetHour;

            //    return (dateUtc);
            //}
            //else if (offsetType.Equals("-"))
            //{
            //    string offsetTime = timeZone.Substring(1);

            //    var offsetHour = TimeSpan.Parse(offsetTime);

            //    dateUtc -= offsetHour;

            //    return (dateUtc);
            //}
            //else
            //    return (dateUtc);

            return dtX;
        }

      
    }

    public class LoginItems
    {
        public string userName { get; set; }
        public string password { get; set; }
    }

    public class NewPasswordItems : LoginItems
    {
        public string newPassword { get; set; }
    }

    public class LocationItems
    {
        public float lats { get; set; }
        public float longs { get; set; }
        public string placeName { get; set; }
    }

    public class ClientItems
    {
        public Guid userId { get; set; }
        public string userName { get; set; }
        public string fullNames { get; set; }
        public string email { get; set; }
        public string profPic { get; set; }
        public string phoneNumber { get; set; }
        public string password { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string dob { get; set; }
        public string bio { get; set; }
        public string gender { get; set; }
        public string coverPhoto { get; set; }

        public DateTime timeStamp { get; set; }
    }

    public class EventItems
    {
        public Guid eventId { get; set; }
        public string title { get; set; }
        public DateTime date { get; set; }
        public DateTime endDate { get; set; }
        public DateTime startTime { get; set; }
        public DateTime endTime { get; set; }
        public LocationItems location { get; set; }
        //public string placeName { get; set; }
        public string description { get; set; }
        public float cost { get; set; }
        public int maxGuests { get; set; }
        public string photo1 { get; set; }
        public string photo2 { get; set; }
        public string photo3 { get; set; }
        public string photo4 { get; set; }
        public string eventType { get; set; }
        public Guid posterId { get; set; }
        public DateTime timeStamp { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        //public string Description { get; set; }
        public string Classification { get; set; }
        public bool PrivacyFlag { get; set; }
        public string photo5 { get; set; }
        public string eventAccess { get; set; }

        //Not really needed
        public string posterName { get; set; }
        public string posterPic { get; set; }        
    }

    public class InviteItems
    {
        public Guid inviteId { get; set; }
        public Guid eventId { get; set; }
        public Guid userId { get; set; }
        public bool status { get; set; }
        public string inviteAc { get; set; }
        public DateTime timeStamp { get; set; }
    }

    public class RSVPItems
    {
        public Guid rsvpId { get; set; }
        public Guid eventId { get; set; }
        public Guid userId { get; set; }
        public Guid inviteId { get; set; }
        public string status { get; set; }
        public DateTime timeStamp { get; set; }

        //Not really needed
        public string clientName { get; set; }
        public string clientPic { get; set; }
        public string clientEmail { get; set; }
    }

    public class InviteClientItems : InviteItems
    {
        public string userName { get; set; }
        public string fullNames { get; set; }
        public string email { get; set; }
        public string profPic { get; set; }
        public string title { get; set; }
        public string placeName { get; set; }
    }

    public class InviteRequestItems
    {
        public Guid requestId { get; set; }
        public Guid eventId { get; set; }
        public Guid userId { get; set; }
        public DateTime timeStamp { get; set; }
    }

    public class ChatItems
    {
        public Guid messageId { get; set; }
        public Guid uniqueMessageId { get; set; }
        public Guid senderId { get; set; }
        public string message { get; set; }
        public string imageLink { get; set; }
        public string status { get; set; }
        public DateTime dateSent { get; set; }
        public DateTime timeStamp { get; set; }
    }

    public class ChatReturnItems : ChatItems
    {
        public string name { get; set; }
        public string email { get; set; }
        public string phoneNumber { get; set; }
        public string profPic { get; set; }
    }

    //enum NotificationTypes
    //{
    //    Mention, Invite, DM, Request
    //}

    public class NotificationItems
    {
        public Guid notificationId { get; set; }
        public Guid clientId { get; set; }
        public string notificationType { get; set; }
        public string notificationMessage { get; set; }
        public DateTime timeStamp { get; set; }
        public Guid otherId { get; set; }
    }

    public class NotificationReturnItems : NotificationItems
    {
        public string clientProfpic { get; set; }
        public string clientName { get; set; }

        //Not really necessary
        public Guid directMessageId { get; set; }
    }

    public class CheckInItems
    {
        public Guid checkInId { get; set; }
        public Guid clientId { get; set; }
        public LocationItems location { get; set; }
        public DateTime timeStamp { get; set; }
    }

    public class FbClientPicItems
    {
        public Guid photoId { get; set; }
        public Guid clientId { get; set; }
        public string photoUrl { get; set; }
        public string photoType { get; set; }
        public DateTime timeStamp { get; set; }

        //Not really needed
        public string clientName { get; set; }
        public string clientPic { get; set; }
    }

    public class EventChatItems
    {
        public Guid commentId { get; set; }
        public string comment { get; set; }
        public string image1 { get; set; }
        public string image2 { get; set; }
        public string image3 { get; set; }
        public Guid eventId { get; set; }
        public Guid clientId { get; set; }
        public DateTime timeStamp { get; set; }
        public string timeZone { get; set; }
    }

    public class EventChatReturnItems : EventChatItems
    {
        public string clientName { get; set; }
        public string clientPic { get; set; }
    }

    public class DirectMessageItems
    {
        public Guid directMessageId { get; set; }
        public Guid senderId { get; set; }
        public Guid receiptId { get; set; }
        public string dm_Type { get; set; }
        public DateTime timeStamp { get; set; }
    }

    public class DirectMessageReturnItems : DirectMessageItems
    {
        public string clientName { get; set; }
        public string clientPic { get; set; }
        public string lastMessage { get; set; }
        public DateTime lastMessageTime { get; set; }
    }

    public class DM_MessagesItems
    {
        public Guid dm_MsgId { get; set; }
        public Guid directMessageId { get; set; }
        public Guid clientId { get; set; }
        public string message { get; set; }
        public string image { get; set; }
        public string status { get; set; }
        public DateTime timeStamp { get; set; }
        public string timeZone { get; set; }
    }    

    public class EventSpaceItems
    {
        public int maxSpaces { get; set; }
        public int remainingSpaces { get; set; }

    }

    public class ContactItems
    {
        public Guid cId { get; set; }
        public Guid contactId { get; set; }
        public Guid userId { get; set; }
        public DateTime timeStamp { get; set; }

        public string fullNames { get; set; }
        public string profPic { get; set; }
        public string email { get; set; }
        public string age { get; set; }
    }

    public class FilterItems
    {
        public bool active { get; set; }
        public bool nightLife { get; set; }
        public bool sporting { get; set; }
        public bool concert { get; set; }
        public bool travel { get; set; }
        public bool food { get; set; }
        public LocationItems location { get; set; }
        //public string placeName { get; set; }
        public float cost { get; set; }     
        public string eventType { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string City { get; set; }     
        public DateTime eStartDate { get; set; }
        public DateTime eEndDate { get; set; }
        public float distance { get; set; }
    }

    public class EventGenItem
    {
        //public Guid eventId { get; set; }
        public string title { get; set; }
        public DateTime date { get; set; }
        public DateTime endDate { get; set; }
        public DateTime startTime { get; set; }
        public DateTime endTime { get; set; }
        public LocationItems location { get; set; }
        //public string placeName { get; set; }
        public string description { get; set; }
        public float cost { get; set; }
        public int maxGuests { get; set; }  
        public string eventType { get; set; }
        public DateTime timeStamp { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        //public string Description { get; set; }
        //public string Classification { get; set; }
        public bool PrivacyFlag { get; set; }
        //public string photo5 { get; set; }
        //public string eventAccess { get; set; }

        //Not really needed
        //public string posterName { get; set; }
        //public string posterPic { get; set; }
    }


    public class SubEventItems
    {
        public Guid id { get; set; }
        public Guid eventId { get; set; }
        public Guid clientId { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public LocationItems location { get; set; }
        public bool privacyFlag { get; set; }
        public double cost { get; set; }
        public string photo1 { get; set; }
        public string photo2 { get; set; }
        public string photo3 { get; set; }
        public string photo4 { get; set; }
        public string photo5 { get; set; }
        public System.DateTime timeStamp { get; set; }


        //NotRequired
        public string eventTitle { get; set; }
        public string fullNames { get; set; }
        public string phoneNumber { get; set; }
        public string email { get; set; }
    }
}
