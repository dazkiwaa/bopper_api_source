﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bopper.ViewModels
{
    public class ClientVm
    {
        public string UserName { get; set; }
        public string FullNames { get; set; }
        public string Email { get; set; }
        public string ProfPic { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
        public string DOB { get; set; }
        public string Bio { get; set; }
    }
}