﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bopper.ViewModels
{
    public class InviteVm
    {
        public System.Guid EventId { get; set; }
        public System.Guid UserId { get; set; }
        public bool Status { get; set; }
        public string InviteAc { get; set; }
    }
}