﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bopper.ViewModels
{
    public class CheckInVm
    {
        public System.Guid ClientId { get; set; }
        public System.Data.Entity.Spatial.DbGeography Location { get; set; }
    }
}