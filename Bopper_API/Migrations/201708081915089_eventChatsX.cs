namespace Bopper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class eventChatsX : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.EventChatMessages", "TimeZone", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.EventChatMessages", "TimeZone");
        }
    }
}
