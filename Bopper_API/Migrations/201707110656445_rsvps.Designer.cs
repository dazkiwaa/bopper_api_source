// <auto-generated />
namespace Bopper.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class rsvps : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(rsvps));
        
        string IMigrationMetadata.Id
        {
            get { return "201707110656445_rsvps"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
