namespace Bopper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class subevent1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SubEvents",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        EventId = c.Guid(nullable: false),
                        ClientId = c.Guid(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        Location = c.Geography(),
                        PrivacyFlag = c.Boolean(nullable: false),
                        Cost = c.Double(nullable: false),
                        Photo1 = c.String(),
                        Photo2 = c.String(),
                        Photo3 = c.String(),
                        Photo4 = c.String(),
                        Photo5 = c.String(),
                        TimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SubEvents");
        }
    }
}
