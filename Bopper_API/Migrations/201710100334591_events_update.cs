namespace Bopper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class events_update : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "nightLife", c => c.Boolean(nullable: false));
            AddColumn("dbo.Events", "travel", c => c.Boolean(nullable: false));
            AddColumn("dbo.Events", "concert", c => c.Boolean(nullable: false));
            AddColumn("dbo.Events", "sporting", c => c.Boolean(nullable: false));
            AddColumn("dbo.Events", "active", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Events", "active");
            DropColumn("dbo.Events", "sporting");
            DropColumn("dbo.Events", "concert");
            DropColumn("dbo.Events", "travel");
            DropColumn("dbo.Events", "nightLife");
        }
    }
}
