namespace Bopper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dm_Messages2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DM_Messages", "timeZone", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DM_Messages", "timeZone");
        }
    }
}
