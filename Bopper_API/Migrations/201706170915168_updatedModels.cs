namespace Bopper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedModels : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RSVPs", "Invite_Invite_Id", "dbo.Invites");
            DropForeignKey("dbo.Events", "Client_UID", "dbo.Clients");
            DropForeignKey("dbo.InviteRequests", "Client_UID", "dbo.Clients");
            DropForeignKey("dbo.InviteRequests", "EventId", "dbo.Events");
            DropForeignKey("dbo.Invites", "EventId", "dbo.Events");
            DropIndex("dbo.RSVPs", new[] { "Invite_Invite_Id" });
            DropColumn("dbo.RSVPs", "InviteId");
            RenameColumn(table: "dbo.Events", name: "Client_UID", newName: "Client_Id");
            RenameColumn(table: "dbo.InviteRequests", name: "Client_UID", newName: "Client_Id");
            RenameColumn(table: "dbo.RSVPs", name: "Invite_Invite_Id", newName: "InviteId");
            RenameIndex(table: "dbo.Events", name: "IX_Client_UID", newName: "IX_Client_Id");
            RenameIndex(table: "dbo.InviteRequests", name: "IX_Client_UID", newName: "IX_Client_Id");
            DropPrimaryKey("dbo.Chats");
            DropPrimaryKey("dbo.CheckIns");
            DropPrimaryKey("dbo.Clients");
            DropPrimaryKey("dbo.Events");
            DropPrimaryKey("dbo.InviteRequests");
            DropPrimaryKey("dbo.Invites");
            DropPrimaryKey("dbo.RSVPs");
            DropPrimaryKey("dbo.Notifications");
            AddColumn("dbo.Chats", "Id", c => c.Guid(nullable: false));
            AddColumn("dbo.CheckIns", "Id", c => c.Guid(nullable: false));
            AddColumn("dbo.Clients", "Id", c => c.Guid(nullable: false));
            AddColumn("dbo.Events", "Id", c => c.Guid(nullable: false));
            AddColumn("dbo.InviteRequests", "Id", c => c.Guid(nullable: false));
            AddColumn("dbo.Invites", "Id", c => c.Guid(nullable: false));
            AddColumn("dbo.RSVPs", "Id", c => c.Guid(nullable: false));
            AddColumn("dbo.Notifications", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.Events", "TimeStamp", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Invites", "TimeStamp", c => c.DateTime(nullable: false));
            AlterColumn("dbo.RSVPs", "ClientId", c => c.Guid(nullable: false));
            AlterColumn("dbo.RSVPs", "EventId", c => c.Guid(nullable: false));
            AlterColumn("dbo.RSVPs", "InviteId", c => c.Guid(nullable: false));
            AlterColumn("dbo.RSVPs", "InviteId", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Chats", "Id");
            AddPrimaryKey("dbo.CheckIns", "Id");
            AddPrimaryKey("dbo.Clients", "Id");
            AddPrimaryKey("dbo.Events", "Id");
            AddPrimaryKey("dbo.InviteRequests", "Id");
            AddPrimaryKey("dbo.Invites", "Id");
            AddPrimaryKey("dbo.RSVPs", "Id");
            AddPrimaryKey("dbo.Notifications", "Id");
            CreateIndex("dbo.RSVPs", "InviteId");
            AddForeignKey("dbo.RSVPs", "InviteId", "dbo.Invites", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Events", "Client_Id", "dbo.Clients", "Id");
            AddForeignKey("dbo.InviteRequests", "Client_Id", "dbo.Clients", "Id");
            AddForeignKey("dbo.InviteRequests", "EventId", "dbo.Events", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Invites", "EventId", "dbo.Events", "Id", cascadeDelete: true);
            DropColumn("dbo.Chats", "MessageId");
            DropColumn("dbo.CheckIns", "CheckinId");
            DropColumn("dbo.Clients", "UID");
            DropColumn("dbo.Events", "EventId");
            DropColumn("dbo.InviteRequests", "RequestId");
            DropColumn("dbo.Invites", "Invite_Id");
            DropColumn("dbo.RSVPs", "RSVP_Id");
            DropColumn("dbo.Notifications", "NotificationId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Notifications", "NotificationId", c => c.Guid(nullable: false));
            AddColumn("dbo.RSVPs", "RSVP_Id", c => c.Guid(nullable: false));
            AddColumn("dbo.Invites", "Invite_Id", c => c.Guid(nullable: false));
            AddColumn("dbo.InviteRequests", "RequestId", c => c.Guid(nullable: false));
            AddColumn("dbo.Events", "EventId", c => c.Guid(nullable: false));
            AddColumn("dbo.Clients", "UID", c => c.Guid(nullable: false));
            AddColumn("dbo.CheckIns", "CheckinId", c => c.Guid(nullable: false));
            AddColumn("dbo.Chats", "MessageId", c => c.Guid(nullable: false));
            DropForeignKey("dbo.Invites", "EventId", "dbo.Events");
            DropForeignKey("dbo.InviteRequests", "EventId", "dbo.Events");
            DropForeignKey("dbo.InviteRequests", "Client_Id", "dbo.Clients");
            DropForeignKey("dbo.Events", "Client_Id", "dbo.Clients");
            DropForeignKey("dbo.RSVPs", "InviteId", "dbo.Invites");
            DropIndex("dbo.RSVPs", new[] { "InviteId" });
            DropPrimaryKey("dbo.Notifications");
            DropPrimaryKey("dbo.RSVPs");
            DropPrimaryKey("dbo.Invites");
            DropPrimaryKey("dbo.InviteRequests");
            DropPrimaryKey("dbo.Events");
            DropPrimaryKey("dbo.Clients");
            DropPrimaryKey("dbo.CheckIns");
            DropPrimaryKey("dbo.Chats");
            AlterColumn("dbo.RSVPs", "InviteId", c => c.Guid());
            AlterColumn("dbo.RSVPs", "InviteId", c => c.Guid());
            AlterColumn("dbo.RSVPs", "EventId", c => c.Guid());
            AlterColumn("dbo.RSVPs", "ClientId", c => c.Guid());
            AlterColumn("dbo.Invites", "TimeStamp", c => c.DateTime());
            AlterColumn("dbo.Events", "TimeStamp", c => c.DateTime());
            DropColumn("dbo.Notifications", "Id");
            DropColumn("dbo.RSVPs", "Id");
            DropColumn("dbo.Invites", "Id");
            DropColumn("dbo.InviteRequests", "Id");
            DropColumn("dbo.Events", "Id");
            DropColumn("dbo.Clients", "Id");
            DropColumn("dbo.CheckIns", "Id");
            DropColumn("dbo.Chats", "Id");
            AddPrimaryKey("dbo.Notifications", "NotificationId");
            AddPrimaryKey("dbo.RSVPs", "RSVP_Id");
            AddPrimaryKey("dbo.Invites", "Invite_Id");
            AddPrimaryKey("dbo.InviteRequests", "RequestId");
            AddPrimaryKey("dbo.Events", "EventId");
            AddPrimaryKey("dbo.Clients", "UID");
            AddPrimaryKey("dbo.CheckIns", "CheckinId");
            AddPrimaryKey("dbo.Chats", "MessageId");
            RenameIndex(table: "dbo.InviteRequests", name: "IX_Client_Id", newName: "IX_Client_UID");
            RenameIndex(table: "dbo.Events", name: "IX_Client_Id", newName: "IX_Client_UID");
            RenameColumn(table: "dbo.RSVPs", name: "InviteId", newName: "Invite_Invite_Id");
            RenameColumn(table: "dbo.InviteRequests", name: "Client_Id", newName: "Client_UID");
            RenameColumn(table: "dbo.Events", name: "Client_Id", newName: "Client_UID");
            AddColumn("dbo.RSVPs", "InviteId", c => c.Guid());
            CreateIndex("dbo.RSVPs", "Invite_Invite_Id");
            AddForeignKey("dbo.Invites", "EventId", "dbo.Events", "EventId", cascadeDelete: true);
            AddForeignKey("dbo.InviteRequests", "EventId", "dbo.Events", "EventId", cascadeDelete: true);
            AddForeignKey("dbo.InviteRequests", "Client_UID", "dbo.Clients", "UID");
            AddForeignKey("dbo.Events", "Client_UID", "dbo.Clients", "UID");
            AddForeignKey("dbo.RSVPs", "Invite_Invite_Id", "dbo.Invites", "Invite_Id");
        }
    }
}
