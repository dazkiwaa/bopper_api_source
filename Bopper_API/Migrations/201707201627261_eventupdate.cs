namespace Bopper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class eventupdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "EndDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Events", "EndDate");
        }
    }
}
