namespace Bopper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EventChatMessages : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DirectMessages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RecipientId = c.Guid(nullable: false),
                        TimeStamp = c.DateTime(nullable: false),
                        SenderId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DM_Messages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DirectMessageId = c.Guid(nullable: false),
                        ClientId = c.Guid(nullable: false),
                        Message = c.String(),
                        Image = c.String(),
                        Status = c.String(),
                        TimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EventChatMessages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        EventId = c.Guid(nullable: false),
                        ClientId = c.Guid(nullable: false),
                        Message = c.String(),
                        Image1 = c.String(),
                        Image2 = c.String(),
                        Image3 = c.String(),
                        TimeStamp = c.DateTime(nullable: false),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EventChatMessages");
            DropTable("dbo.DM_Messages");
            DropTable("dbo.DirectMessages");
        }
    }
}
