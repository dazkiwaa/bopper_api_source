namespace Bopper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FbClientPics : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FbClientPics",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ClientId = c.Guid(nullable: false),
                        PhotoUrl = c.String(),
                        PhotoType = c.String(),
                        TimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.FbClientPics");
        }
    }
}
