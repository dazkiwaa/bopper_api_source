namespace Bopper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class chatsx : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Chats", "TimeZone", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Chats", "TimeZone");
        }
    }
}
