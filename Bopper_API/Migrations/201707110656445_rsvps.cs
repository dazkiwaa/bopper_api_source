namespace Bopper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rsvps : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RSVPs", "InviteId", "dbo.Invites");
            DropIndex("dbo.RSVPs", new[] { "InviteId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.RSVPs", "InviteId");
            AddForeignKey("dbo.RSVPs", "InviteId", "dbo.Invites", "Id", cascadeDelete: true);
        }
    }
}
