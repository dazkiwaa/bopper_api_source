namespace Bopper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newReviewModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "Classification", c => c.String());
            AddColumn("dbo.Events", "PrivacyFlag", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Events", "PrivacyFlag");
            DropColumn("dbo.Events", "Classification");
        }
    }
}
