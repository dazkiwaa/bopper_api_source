namespace Bopper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _new : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Chats",
                c => new
                    {
                        MessageId = c.Guid(nullable: false),
                        UniqueMessageId = c.Guid(nullable: false),
                        SenderId = c.Guid(nullable: false),
                        Message = c.String(),
                        ImageLink = c.String(),
                        Status = c.String(),
                        DateSent = c.DateTime(nullable: false),
                        TimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.MessageId);
            
            CreateTable(
                "dbo.CheckIns",
                c => new
                    {
                        CheckinId = c.Guid(nullable: false),
                        ClientId = c.Guid(nullable: false),
                        Location = c.Geography(),
                        TimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.CheckinId);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        UID = c.Guid(nullable: false),
                        UserName = c.String(),
                        FullNames = c.String(),
                        Email = c.String(),
                        ProfPic = c.String(),
                        PhoneNumber = c.String(),
                        Password = c.String(),
                        TimeStamp = c.DateTime(nullable: false),
                        DOB = c.String(),
                        Bio = c.String(),
                    })
                .PrimaryKey(t => t.UID);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        EventId = c.Guid(nullable: false),
                        Title = c.String(),
                        Date = c.DateTime(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        Location = c.Geography(),
                        PlaceName = c.String(),
                        Description = c.String(),
                        Cost = c.Double(nullable: false),
                        MaxGuests = c.Int(nullable: false),
                        Photo1 = c.String(),
                        Photo2 = c.String(),
                        Photo3 = c.String(),
                        Photo4 = c.String(),
                        EventType = c.String(),
                        PosterId = c.Guid(nullable: false),
                        TimeStamp = c.DateTime(),
                        EventAccess = c.String(),
                        Photo5 = c.String(),
                        Client_UID = c.Guid(),
                    })
                .PrimaryKey(t => t.EventId)
                .ForeignKey("dbo.Clients", t => t.Client_UID)
                .Index(t => t.Client_UID);
            
            CreateTable(
                "dbo.InviteRequests",
                c => new
                    {
                        RequestId = c.Guid(nullable: false),
                        EventId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        TimeStamp = c.DateTime(nullable: false),
                        Client_UID = c.Guid(),
                    })
                .PrimaryKey(t => t.RequestId)
                .ForeignKey("dbo.Clients", t => t.Client_UID)
                .ForeignKey("dbo.Events", t => t.EventId, cascadeDelete: true)
                .Index(t => t.EventId)
                .Index(t => t.Client_UID);
            
            CreateTable(
                "dbo.Invites",
                c => new
                    {
                        Invite_Id = c.Guid(nullable: false),
                        EventId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        Status = c.Boolean(nullable: false),
                        TimeStamp = c.DateTime(),
                        InviteAc = c.String(),
                    })
                .PrimaryKey(t => t.Invite_Id)
                .ForeignKey("dbo.Events", t => t.EventId, cascadeDelete: true)
                .Index(t => t.EventId);
            
            CreateTable(
                "dbo.RSVPs",
                c => new
                    {
                        RSVP_Id = c.Guid(nullable: false),
                        ClientId = c.Guid(),
                        EventId = c.Guid(),
                        InviteId = c.Guid(),
                        Status = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                        Invite_Invite_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.RSVP_Id)
                .ForeignKey("dbo.Invites", t => t.Invite_Invite_Id)
                .Index(t => t.Invite_Invite_Id);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        NotificationId = c.Guid(nullable: false),
                        ReceiverId = c.Guid(nullable: false),
                        NotificationTitle = c.String(),
                        NotificationMessage = c.String(),
                        Type = c.String(),
                        TypeId = c.Guid(),
                        TimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.NotificationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RSVPs", "Invite_Invite_Id", "dbo.Invites");
            DropForeignKey("dbo.Invites", "EventId", "dbo.Events");
            DropForeignKey("dbo.InviteRequests", "EventId", "dbo.Events");
            DropForeignKey("dbo.InviteRequests", "Client_UID", "dbo.Clients");
            DropForeignKey("dbo.Events", "Client_UID", "dbo.Clients");
            DropIndex("dbo.RSVPs", new[] { "Invite_Invite_Id" });
            DropIndex("dbo.Invites", new[] { "EventId" });
            DropIndex("dbo.InviteRequests", new[] { "Client_UID" });
            DropIndex("dbo.InviteRequests", new[] { "EventId" });
            DropIndex("dbo.Events", new[] { "Client_UID" });
            DropTable("dbo.Notifications");
            DropTable("dbo.RSVPs");
            DropTable("dbo.Invites");
            DropTable("dbo.InviteRequests");
            DropTable("dbo.Events");
            DropTable("dbo.Clients");
            DropTable("dbo.CheckIns");
            DropTable("dbo.Chats");
        }
    }
}
