namespace Bopper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newUserAndEventModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clients", "State", c => c.String());
            AddColumn("dbo.Clients", "Country", c => c.String());
            AddColumn("dbo.Clients", "City", c => c.String());
            AddColumn("dbo.Clients", "Description", c => c.String());
            AddColumn("dbo.Events", "State", c => c.String());
            AddColumn("dbo.Events", "Country", c => c.String());
            AddColumn("dbo.Events", "City", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Events", "City");
            DropColumn("dbo.Events", "Country");
            DropColumn("dbo.Events", "State");
            DropColumn("dbo.Clients", "Description");
            DropColumn("dbo.Clients", "City");
            DropColumn("dbo.Clients", "Country");
            DropColumn("dbo.Clients", "State");
        }
    }
}
