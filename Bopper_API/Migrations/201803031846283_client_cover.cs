namespace Bopper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class client_cover : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Clients", "CoverPhoto", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clients", "CoverPhoto");
        }
    }
}
