namespace Bopper.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class notifications : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Notifications", "ClientId", c => c.Guid(nullable: false));
            AddColumn("dbo.Notifications", "NotificationType", c => c.String());
            AddColumn("dbo.Notifications", "OtherId", c => c.Guid());
            DropColumn("dbo.Notifications", "ReceiverId");
            DropColumn("dbo.Notifications", "NotificationTitle");
            DropColumn("dbo.Notifications", "Type");
            DropColumn("dbo.Notifications", "TypeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Notifications", "TypeId", c => c.Guid());
            AddColumn("dbo.Notifications", "Type", c => c.String());
            AddColumn("dbo.Notifications", "NotificationTitle", c => c.String());
            AddColumn("dbo.Notifications", "ReceiverId", c => c.Guid(nullable: false));
            DropColumn("dbo.Notifications", "OtherId");
            DropColumn("dbo.Notifications", "NotificationType");
            DropColumn("dbo.Notifications", "ClientId");
        }
    }
}
